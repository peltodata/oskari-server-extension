# Installation

### prerequirements
1. nvm
    - for node version management
2. java
3. maven
4. git


---
### Installation steps
1. load sample bundle
2. unzip sample bundle somewhere
3. pull repositories
    1. this
    2. peltodata-frontend
4. compile server extension
    1. mvn -DskipTests=true clean package
        1. skip tests because some flyway scripts fail and not sure how to fix that issue
    2. copy webapp-map/target/oskari-map.war to <UNZIPPED_BUNDLE>/oskari-server/webapps/oskari-map.war
        1. this should overwrite a file
5. compile frontend
    1. npm install
    2. export NODE_OPTIONS=--max_old_space_size=2048
    3. set parallel=false in UglifyJsPlugin settings
        - Node_modules/oskari_frontend/webpack.config.js
    4. npm run build
        - this creates a dist folder
        - dist folder should be copied to <UNZIPPED_BUNDLE>/oskari-server/peltodata/dist
6. configure jetty to use correct peltodata frontend
    1. <UNZIPPED_BUNDLE>/oskari-server/webapps/oskari-front.xml
    2. modify /sample_application to /peltodata
7. configure <UNZIPPED_BUNDLE>/oskari-server/resources/oskari-ext.properties
8. run in <UNZIPPED_BUNDLE>/jetty-distribution
    1. java -jar <UNZIPPED_BUNDLE>/jetty-distribution-<<VERSION>>/start.jar

# Configuration
### 1. oskari-server/resources/oskari-ext.properties

- Configure the "version" path for peltodata frontend files
    - this has to correspond with /peltodata/dist/<version>/ which is also specified in package.json (now devapp)
    - Modify row
        - `oskari.client.version=dist/1.0.0`
- Activate the bundle
    - Done so that `my fields` is activated and shown in the Ui for logged in user with role `User`
    - Add peltodata to row below
        - `actionhandler.GetAppSetup.dynamic.bundles = admin-layerselector, admin-layerrights, admin-users, admin, peltodata`
    - Add row
        - `actionhandler.GetAppSetup.dynamic.bundle.peltodata.roles = User`

- configure correct geoserver url 
    - This url has to be accessible from outside because clients will access the layers from there
        - `geoserver.url=https://xxx/geoserver`

- Activate peltodata backend module
    - If not modified, the database migrations wont be run and there will be errors
    - also remove `myapp` from this list
    - Modify row
        - `db.additional.modules=myplaces, userlayer, peltodata`

- Configure path for uploaded files
    - Configures where NEW files are stored, existing files do not care about this setting (path saved in db)
    - Optional
        - if not configured, it is under `oskari-server/geoserver_data`
    - `peltodata.upload.root.dir`
    - Under this folder, a folder structure will be made, for example:
        - ```
            /farms
            /farms/{farmid}/
            /farms/{farmid}/{year}/
            /farms/{farmid}/{year}/<filetimestamp>_CROP_ESTIMATION_ORIGINAL.tiff              
          ```
- Python script configuration
    - path to python script
    - if value is `dev`, for dev purposes and better developer experience, only copy will be made (input -> output).
        - `peltodata.scripts.crop_estimation=/home/oskari/copy-file.py`
        - `peltodata.scripts.yield=dev`
    - scripts will be called as `python <SCRIPT> 123123123123_CROP_ESTIMATION.json`
        - json file format can be found in `example_json.json` 

- Configure registration and role creation based on user
    - modify
        - `oskari.user.service=fi.peltodata.service.PeltodataUserService`
    - add
        ```
        allow.registration=true
        oskari.email.sender=<sender@domain.com>
        oskari.email.host=<smtp.domain.com>
      ```

### 2. nginx.conf
- if nginx is used, big file uploads have to be enabled
- can be put in `http`, `server` or `location` block. Best probably server 
  - `client_max_body_size 2G;`

### 3. jetty/start.d/console-capture.ini
- to enable logging for jetty. after this, logfiles are in logs folder 
```
# ---------------------------------------
# Module: console-capture
# Redirects JVMs console stderr and stdout to a log file,
# including output from Jetty's default StdErrLog logging.
# ---------------------------------------
--module=console-capture

## Logging directory (relative to $jetty.base)
# jetty.console-capture.dir=logs

## Whether to append to existing file
# jetty.console-capture.append=true

## How many days to retain old log files
jetty.console-capture.retainDays=10

## Timezone of the log timestamps
# jetty.console-capture.timezone=GMT
```

