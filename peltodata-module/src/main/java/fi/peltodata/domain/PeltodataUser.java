package fi.peltodata.domain;

public class PeltodataUser {
    private Long userId;
    private String farmId;

    public PeltodataUser(Long userId, String farmId) {
        this.userId = userId;
        this.farmId = farmId;
    }

    public PeltodataUser() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }
}
