package fi.peltodata.domain;

public class Peltolohko {
    private int gid;
    private String ely;
    private String knro;
    private String tiltu;
    private String lohko;
    private double pintaAla;
    private double ymparys;
    private String geoJson;

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public String getEly() {
        return ely;
    }

    public void setEly(String ely) {
        this.ely = ely;
    }

    public String getKnro() {
        return knro;
    }

    public void setKnro(String knro) {
        this.knro = knro;
    }

    public String getTiltu() {
        return tiltu;
    }

    public void setTiltu(String tiltu) {
        this.tiltu = tiltu;
    }

    public String getLohko() {
        return lohko;
    }

    public void setLohko(String lohko) {
        this.lohko = lohko;
    }

    public double getPintaAla() {
        return pintaAla;
    }

    public void setPintaAla(double pintaAla) {
        this.pintaAla = pintaAla;
    }

    public double getYmparys() {
        return ymparys;
    }

    public void setYmparys(double ymparys) {
        this.ymparys = ymparys;
    }

    public String getGeoJson() {
        return geoJson;
    }

    public void setGeoJson(String geoJson) {
        this.geoJson = geoJson;
    }
}
