package fi.peltodata.domain;

import org.apache.ibatis.annotations.*;

import java.util.Map;

public interface PeltodataUserMapper {


    @Select("select user_id, farm_id from peltodata_user where user_id = #{userId}")
    @Results({
        @Result(property = "userId", column = "user_id"),
        @Result(property = "farmId", column = "farm_id"),
    })
    PeltodataUser findPeltodataUser(@Param("userId") Long userId);

    @Update("UPDATE peltodata_user SET " +
        "farm_id=#{farmId}, " +
        "WHERE user_id=#{userId}")
    void updatePeltodataUser(final PeltodataUser farmfield);

    @Insert("INSERT INTO peltodata_user (user_id, farm_id) " +
        "VALUES (#{userId}, #{farmId})")
    void insertPeltodataUser(final PeltodataUser peltodataUser);
}
