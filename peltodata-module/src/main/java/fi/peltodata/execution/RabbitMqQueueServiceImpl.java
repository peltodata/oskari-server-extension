package fi.peltodata.execution;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RabbitMqQueueServiceImpl implements QueueService {
    @Override
    public void addTaskToQueue(TaskMessage task) {
        Channel c = RabbitMqClient.getChannel();
        ObjectMapper om = new ObjectMapper();
        try {
            String asJsonString = om.writeValueAsString(task);
            c.basicPublish("", task.getChannelName(), null, asJsonString.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
