package fi.peltodata.execution;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class TaskMessage {
    @JsonIgnore
    private String channelName;

    public TaskMessage() {
    }

    public TaskMessage(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
