package fi.peltodata.service;

import fi.nls.oskari.domain.User;
import fi.peltodata.domain.PeltodataUser;

public interface PeltodataService {
    boolean farmIdExists(final String farmId);

    void registerFarmId(String farmId, User user);

    PeltodataUser getPeltodataUser(long id);
}
