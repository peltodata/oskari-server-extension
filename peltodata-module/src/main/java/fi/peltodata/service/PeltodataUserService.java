package fi.peltodata.service;

import fi.nls.oskari.domain.Role;
import fi.nls.oskari.domain.User;
import fi.nls.oskari.log.LogFactory;
import fi.nls.oskari.log.Logger;
import fi.nls.oskari.service.ServiceException;
import fi.nls.oskari.user.DatabaseUserService;
import fi.nls.oskari.user.MybatisRoleService;
import fi.nls.oskari.user.MybatisUserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PeltodataUserService extends DatabaseUserService {
    private static final Logger log = LogFactory.getLogger(PeltodataUserService.class);
    private static final String ERR_USER_MISSING = "User was null";
    private final PeltodataService peltodataService;

    private MybatisRoleService roleService = new MybatisRoleService();
    private MybatisUserService userService = new MybatisUserService();

    public PeltodataUserService() {
        peltodataService = new PeltodataServiceMybatisImpl();
    }

    @Override
    public User createUser(User user) throws ServiceException {
        List<String> r = user.getRoles().stream().map(Role::getId).map(String::valueOf).collect(Collectors.toList());
        String[] roleIds = new String[0];
        roleIds = r.toArray(roleIds);
        return createUser(user, roleIds);
    }

    @Override
    public User createUser(User user, String[] roleIds) throws ServiceException {
        log.debug("createUser #######################");
        if (user == null) {
            throw new ServiceException(ERR_USER_MISSING);
        }

        String farmId = (String) user.getAttribute("farm_id");
        if (farmId == null || !peltodataService.farmIdExists(farmId)) {
            throw new ServiceException("invalid farm id");
        }

        Role role = roleService.findRoleByName(user.getScreenname());

        List<String> roleIdList = new ArrayList<>(Arrays.asList(roleIds));
        if (role == null) {
            role = new Role();
            role.setName(user.getScreenname());
            roleService.insert(role);
            role = roleService.findRoleByName(user.getScreenname());
            roleIdList.add(String.valueOf(role.getId()));
        }

        if (user.getUuid() == null || user.getUuid().isEmpty()) {
            user.setUuid(generateUuid());
        }
        Long id = userService.addUser(user);

        for (String roleId : roleIdList) {
            log.debug("roleId: " + roleId + " userId: " + id);
            roleService.linkRoleToUser(Long.parseLong(roleId), id);
        }

        peltodataService.registerFarmId(farmId, user);
        return userService.find(id);
    }
}
