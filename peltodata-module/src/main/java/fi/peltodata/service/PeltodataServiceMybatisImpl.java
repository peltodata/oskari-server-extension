package fi.peltodata.service;

import fi.nls.oskari.annotation.Oskari;
import fi.nls.oskari.domain.User;
import fi.nls.oskari.domain.map.OskariLayer;
import fi.nls.oskari.log.LogFactory;
import fi.nls.oskari.log.Logger;
import fi.nls.oskari.map.layer.OskariLayerService;
import fi.nls.oskari.map.layer.OskariLayerServiceMybatisImpl;
import fi.nls.oskari.service.OskariComponent;
import fi.nls.oskari.service.ServiceException;
import fi.nls.oskari.user.MybatisUserService;
import fi.nls.oskari.util.PropertyUtil;
import fi.peltodata.domain.*;
import fi.peltodata.execution.FarmTaskMessage;
import fi.peltodata.execution.RabbitMqQueueServiceImpl;
import fi.peltodata.execution.TaskMessage;
import fi.peltodata.repository.PeltodataRepository;
import fi.peltodata.repository.PeltodataRepositoryImpl;
import org.oskari.service.maplayer.OskariMapLayerGroupService;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Oskari
public class PeltodataServiceMybatisImpl extends OskariComponent implements PeltodataService {
    private static final Logger LOG = LogFactory.getLogger(PeltodataRepositoryImpl.class);
    private final RabbitMqQueueServiceImpl queueService;
    private PeltodataRepository peltodataRepository;

    public PeltodataServiceMybatisImpl() {
        this.peltodataRepository = new PeltodataRepositoryImpl();
        this.queueService = new RabbitMqQueueServiceImpl();
    }

    @Override
    public boolean farmIdExists(String farmId) {
        return peltodataRepository.farmIdExists(farmId);
    }

    @Override
    public void registerFarmId(String farmId, User user) {
        try {
            PeltodataUser peltodataUser = peltodataRepository.findPeltodataUser(user.getId());
            if (peltodataUser == null) {
                peltodataUser = new PeltodataUser(user.getId(), farmId);
                peltodataRepository.insertPeltodataUser(peltodataUser);
            }

            TaskMessage taskMessage = new FarmTaskMessage(farmId);
            queueService.addTaskToQueue(taskMessage);
        } catch (Exception e) {
            LOG.error("error registering farm id", e);
        }
    }

    @Override
    public PeltodataUser getPeltodataUser(long id) {
        return peltodataRepository.findPeltodataUser(id);
    }
}
