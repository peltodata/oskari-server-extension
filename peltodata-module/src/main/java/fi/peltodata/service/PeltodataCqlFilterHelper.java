package fi.peltodata.service;

import fi.nls.oskari.domain.User;
import fi.nls.oskari.domain.map.OskariLayer;
import fi.nls.oskari.service.ServiceException;
import fi.peltodata.domain.PeltodataUser;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class PeltodataCqlFilterHelper {
    public static final String PELTODATA_CQL_FILTER = "peltodata_cql_filter";
    public static final String CQL_FILTER_KEY = "cql_filter";
    private final PeltodataService peltodataService;
    private final String defaultWrongQuery;

    public PeltodataCqlFilterHelper() throws ServiceException {
        peltodataService = new PeltodataServiceMybatisImpl();
        defaultWrongQuery = "1=2";
        //defaultWrongQuery = "1%3D2";
    }

    private String getFilterAsString(OskariLayer layer, User user) {
        if (peltodataCqlFilterDisabled(layer)) {
            return "";
        }

        PeltodataUser peltodataUser = peltodataService.getPeltodataUser(user.getId());
        if (peltodataUser == null && user.isAdmin()) {
            return "";
        } else if (peltodataUser == null) {
            return defaultWrongQuery;
        }

        String farmId = peltodataUser.getFarmId();

        String filterTemplate;
        try {
            filterTemplate = getCqlFilterTemplate(layer);
        } catch (JSONException e) {
            return defaultWrongQuery;
        }
        String filter = filterTemplate.replace("<farm_id>", farmId);
        filter = filter.replace("%", "%25");
        return filter;
    }

    public String getFilterParam(OskariLayer layer, User user) {
        String filter = getFilterAsString(layer, user);
        if (filter.length() > 0) {
            return "&" + CQL_FILTER_KEY + "=" + filter;
        }
        return "";
    }

    public Map<String, String> getFilterParamAsMap(OskariLayer layer, User user) {
        String filter = getFilterAsString(layer, user);
        if (filter.length() > 0) {
            Map<String, String> ret = new HashMap<>();
            ret.put(CQL_FILTER_KEY, filter);
            return ret;
        }

        return new HashMap<>();
    }

    private String getCqlFilterTemplate(OskariLayer layer) throws JSONException {
        JSONObject attributes = layer.getAttributes();
        String text = attributes.getString(PELTODATA_CQL_FILTER);
        return text;

    }

    private boolean peltodataCqlFilterDisabled(OskariLayer layer) {
        JSONObject attributes = layer.getAttributes();
        if (attributes == null) {
            return true;
        }

        return !attributes.has(PELTODATA_CQL_FILTER);
    }

}
