package fi.peltodata.controller;

import fi.nls.oskari.control.ActionParameters;
import fi.nls.oskari.domain.User;
import fi.nls.oskari.log.LogFactory;
import fi.nls.oskari.log.Logger;
import fi.nls.oskari.spring.extension.OskariParam;
import fi.nls.oskari.util.PropertyUtil;
import fi.peltodata.PeltodataConfig;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Map;

@Controller
@RequestMapping("peltodata/support")
public class PeltodataSupportController {
    private static final Logger LOG = LogFactory.getLogger(PeltodataSupportController.class);
    private static final String SUPPORT_EXTENSION_URL = PropertyUtil.get(PeltodataConfig.PROP_SUPPORT_EXTENSION_URL, "http://localhost:5000");

    @RequestMapping(value="/**")
    public ResponseEntity<String> proxyGet(RequestEntity<String> requestEntity, @RequestParam Map<String, String> params, @OskariParam ActionParameters param) throws Exception {
        URI remoteService = URI.create(SUPPORT_EXTENSION_URL);
        URIBuilder builder = new URIBuilder(remoteService);

        URI fullRmeoteUri = builder.setPath(requestEntity.getUrl().getPath())
            .setQuery(requestEntity.getUrl().getQuery())
            .setFragment(requestEntity.getUrl().getFragment()).build();


        User user = param.getUser();
        boolean isGuest = user.isGuest();
        if (isGuest) {
            throw new Exception("not logged in");
        }
        HttpHeaders headers = requestEntity.getHeaders();
        Long userId = user.getId();
        /// headers = HttpHeaders.writableHttpHeaders(headers);
        headers = HttpHeaders.writableHttpHeaders(new HttpHeaders());
        headers.add("x-oskari-user", String.valueOf(userId));
        if (requestEntity.getHeaders().containsKey("Content-Type")) {
            MediaType contentType = requestEntity.getHeaders().getContentType();
            headers.setContentType(contentType);
        }


        HttpEntity<String> httpEntity = new HttpEntity<>(requestEntity.getBody(), headers);
        ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
        RestTemplate restTemplate = new RestTemplate(factory);
        System.out.println(fullRmeoteUri.toString());
        try {

            ResponseEntity<String> serverResponse = restTemplate.exchange(fullRmeoteUri, requestEntity.getMethod(), httpEntity, String.class);
            return serverResponse;


        } catch (HttpStatusCodeException e) {
            return ResponseEntity.status(e.getRawStatusCode())
                .headers(e.getResponseHeaders())
                .body(e.getResponseBodyAsString());
        }

    }
}
