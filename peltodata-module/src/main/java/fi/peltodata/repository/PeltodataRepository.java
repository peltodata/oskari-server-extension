package fi.peltodata.repository;

import fi.peltodata.domain.*;

import java.util.List;

public interface PeltodataRepository {
    boolean farmIdExists(String farmId);

    void insertPeltodataUser(PeltodataUser peltodataUser);

    PeltodataUser findPeltodataUser(long userId);

    void updatePeltodataUser(PeltodataUser peltodataUser);
}
