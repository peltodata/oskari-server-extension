package fi.peltodata.repository;

import fi.nls.oskari.db.DatasourceHelper;
import fi.nls.oskari.log.LogFactory;
import fi.nls.oskari.log.Logger;
import fi.nls.oskari.mybatis.JSONObjectMybatisTypeHandler;
import fi.peltodata.domain.*;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.sql.DataSource;
import java.util.List;

public class PeltodataRepositoryImpl implements PeltodataRepository {
    private SqlSessionFactory factory;
    private static final Logger LOG = LogFactory.getLogger(PeltodataRepositoryImpl.class);

    public PeltodataRepositoryImpl() {
        final DatasourceHelper helper = DatasourceHelper.getInstance();
        DataSource dataSource = helper.getDataSource();
        if (dataSource == null) {
            dataSource = helper.createDataSource();
        }
        if (dataSource == null) {
            LOG.error("Couldn't get datasource for oskari layer service");
        }
        factory = initializeMyBatis(dataSource);
    }

    @Override
    public boolean farmIdExists(String farmId) {
        return getPeltolohkot(farmId).size() > 0;
    }

    public List<Peltolohko> getPeltolohkot(String farmId) {
        LOG.debug("getPeltolohkot " + farmId);
        try (SqlSession session = factory.openSession()) {
            PeltolohkoMapper peltolohkoMapper = session.getMapper(PeltolohkoMapper.class);
            List<Peltolohko> lohkot = peltolohkoMapper.findAllByFarmId(farmId);
            return lohkot;
        }
    }

    private SqlSessionFactory initializeMyBatis(final DataSource dataSource) {
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);

        final Configuration configuration = new Configuration(environment);
        configuration.setLazyLoadingEnabled(true);
        configuration.getTypeHandlerRegistry().register(JSONObjectMybatisTypeHandler.class);
        configuration.addMapper(PeltolohkoMapper.class);
        configuration.addMapper(PeltodataUserMapper.class);

        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @Override
    public void insertPeltodataUser(PeltodataUser peltodataUser) {
        try (final SqlSession session = factory.openSession()) {
            final PeltodataUserMapper mapper = session.getMapper(PeltodataUserMapper.class);
            mapper.insertPeltodataUser(peltodataUser);
            session.commit();
        }
    }

    @Override
    public PeltodataUser findPeltodataUser(long userId) {
        try (final SqlSession session = factory.openSession()) {
            final PeltodataUserMapper mapper = session.getMapper(PeltodataUserMapper.class);
            return mapper.findPeltodataUser(userId);
        }
    }

    @Override
    public void updatePeltodataUser(PeltodataUser peltodataUser) {
        try (final SqlSession session = factory.openSession()) {
            final PeltodataUserMapper mapper = session.getMapper(PeltodataUserMapper.class);
            mapper.updatePeltodataUser(peltodataUser);
            session.commit();
        }
    }
}
