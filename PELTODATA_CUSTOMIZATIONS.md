# List of modified Oskari files and reasons for modifications

### GetGeoPointDataHandler.java
Is overridden so we can pass some parameters to geoserver as custom attributes

### GetLayerTileHandler.java
Is overridden so we can pass some parameters to geoserver as custom attributes

### SpringInitializer.java
Allow custom envinment variables


