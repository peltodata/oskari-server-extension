--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE oskaridb;
--
-- Name: oskaridb; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE oskaridb WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_Finland.1252';


ALTER DATABASE oskaridb OWNER TO postgres;

\connect oskaridb

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.categories (
    id bigint NOT NULL,
    category_name character varying(256) NOT NULL,
    "default" boolean,
    uuid character varying(64),
    publisher_name character varying(256),
    options json
);


ALTER TABLE public.categories OWNER TO oskari;

--
-- Name: TABLE categories; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.categories IS 'Layers including styles for user generated myplaces data';


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO oskari;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: gt_pk_metadata_table; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.gt_pk_metadata_table (
    table_schema character varying(32) NOT NULL,
    table_name character varying(32) NOT NULL,
    pk_column character varying(32) NOT NULL,
    pk_column_idx integer,
    pk_policy character varying(32),
    pk_sequence character varying(64)
);


ALTER TABLE public.gt_pk_metadata_table OWNER TO oskari;

--
-- Name: TABLE gt_pk_metadata_table; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.gt_pk_metadata_table IS 'Defines user layer data tables';


--
-- Name: my_places_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.my_places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.my_places_id_seq OWNER TO oskari;

--
-- Name: oskari_backendstatus; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_backendstatus (
    ts timestamp without time zone DEFAULT now(),
    maplayer_id integer NOT NULL,
    status character varying(500),
    statusmessage character varying(2000),
    infourl character varying(2000)
);


ALTER TABLE public.oskari_backendstatus OWNER TO oskari;

--
-- Name: TABLE oskari_backendstatus; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_backendstatus IS 'Results of layer data source availability probes';


--
-- Name: oskari_capabilities_cache; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_capabilities_cache (
    id bigint NOT NULL,
    layertype character varying(64) NOT NULL,
    url character varying(2048) NOT NULL,
    data text NOT NULL,
    created timestamp with time zone DEFAULT now(),
    updated timestamp with time zone,
    version text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.oskari_capabilities_cache OWNER TO oskari;

--
-- Name: TABLE oskari_capabilities_cache; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_capabilities_cache IS 'Cache of GetCapabilities results for WMS/WMTS layers';


--
-- Name: oskari_capabilities_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_capabilities_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_capabilities_cache_id_seq OWNER TO oskari;

--
-- Name: oskari_capabilities_cache_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_capabilities_cache_id_seq OWNED BY public.oskari_capabilities_cache.id;


--
-- Name: oskari_dataprovider; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_dataprovider (
    id integer NOT NULL,
    locale text DEFAULT '{}'::text
);


ALTER TABLE public.oskari_dataprovider OWNER TO oskari;

--
-- Name: TABLE oskari_dataprovider; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_dataprovider IS 'Layer data provider name localizations';


--
-- Name: oskari_jaas_users; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_jaas_users (
    id integer NOT NULL,
    login text NOT NULL,
    password text NOT NULL
);


ALTER TABLE public.oskari_jaas_users OWNER TO oskari;

--
-- Name: TABLE oskari_jaas_users; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_jaas_users IS 'Credentials for users when using built-in login';


--
-- Name: oskari_jaas_users_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_jaas_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_jaas_users_id_seq OWNER TO oskari;

--
-- Name: oskari_jaas_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_jaas_users_id_seq OWNED BY public.oskari_jaas_users.id;


--
-- Name: oskari_layergroup_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_layergroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_layergroup_id_seq OWNER TO oskari;

--
-- Name: oskari_layergroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_layergroup_id_seq OWNED BY public.oskari_dataprovider.id;


--
-- Name: oskari_maplayer; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_maplayer (
    id integer NOT NULL,
    parentid integer DEFAULT '-1'::integer NOT NULL,
    type character varying(50) NOT NULL,
    base_map boolean DEFAULT false NOT NULL,
    dataprovider_id integer,
    name character varying(2000),
    url character varying(2000),
    locale text,
    opacity integer DEFAULT 100,
    style character varying(100),
    minscale double precision DEFAULT '-1'::integer,
    maxscale double precision DEFAULT '-1'::integer,
    legend_image character varying(2000),
    metadataid character varying(200),
    params text DEFAULT '{}'::text,
    options text DEFAULT '{}'::text,
    gfi_type character varying(200),
    gfi_xslt text,
    gfi_content text,
    realtime boolean DEFAULT false,
    refresh_rate integer DEFAULT 0,
    created timestamp with time zone DEFAULT now(),
    updated timestamp with time zone,
    username character varying(256),
    password character varying(256),
    srs_name character varying,
    version character varying(64) DEFAULT ''::character varying NOT NULL,
    attributes text DEFAULT '{}'::text,
    capabilities text DEFAULT '{}'::text,
    capabilities_last_updated timestamp with time zone,
    capabilities_update_rate_sec integer DEFAULT 0,
    internal boolean DEFAULT false NOT NULL
);


ALTER TABLE public.oskari_maplayer OWNER TO oskari;

--
-- Name: TABLE oskari_maplayer; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_maplayer IS 'Map layers configuration';


--
-- Name: oskari_maplayer_externalid; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_maplayer_externalid (
    maplayerid integer NOT NULL,
    externalid character varying(50) NOT NULL
);


ALTER TABLE public.oskari_maplayer_externalid OWNER TO oskari;

--
-- Name: TABLE oskari_maplayer_externalid; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_maplayer_externalid IS 'Legacy "external id" associated with map layer';


--
-- Name: oskari_maplayer_group; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_maplayer_group (
    id integer NOT NULL,
    locale character varying(20000),
    parentid integer DEFAULT '-1'::integer NOT NULL,
    selectable boolean DEFAULT true NOT NULL,
    order_number integer DEFAULT 1000000
);


ALTER TABLE public.oskari_maplayer_group OWNER TO oskari;

--
-- Name: TABLE oskari_maplayer_group; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_maplayer_group IS 'Logical group for layers';


--
-- Name: oskari_maplayer_group_link; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_maplayer_group_link (
    maplayerid integer NOT NULL,
    groupid integer NOT NULL,
    order_number integer DEFAULT 1000000
);


ALTER TABLE public.oskari_maplayer_group_link OWNER TO oskari;

--
-- Name: TABLE oskari_maplayer_group_link; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_maplayer_group_link IS 'Bridge table between map layer and its group';


--
-- Name: oskari_maplayer_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_maplayer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_maplayer_id_seq OWNER TO oskari;

--
-- Name: oskari_maplayer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_maplayer_id_seq OWNED BY public.oskari_maplayer.id;


--
-- Name: oskari_maplayer_metadata; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_maplayer_metadata (
    id integer NOT NULL,
    metadataid character varying(256),
    wkt character varying(512) DEFAULT ''::character varying,
    json text DEFAULT ''::text,
    ts timestamp without time zone DEFAULT now()
);


ALTER TABLE public.oskari_maplayer_metadata OWNER TO oskari;

--
-- Name: TABLE oskari_maplayer_metadata; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_maplayer_metadata IS 'Metadata about map layers';


--
-- Name: oskari_maplayer_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_maplayer_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_maplayer_metadata_id_seq OWNER TO oskari;

--
-- Name: oskari_maplayer_metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_maplayer_metadata_id_seq OWNED BY public.oskari_maplayer_metadata.id;


--
-- Name: oskari_maplayer_projections; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_maplayer_projections (
    id bigint NOT NULL,
    name character varying(64) NOT NULL,
    maplayerid integer NOT NULL
);


ALTER TABLE public.oskari_maplayer_projections OWNER TO oskari;

--
-- Name: TABLE oskari_maplayer_projections; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_maplayer_projections IS 'Supported projections (EPSG-codes) for map layers';


--
-- Name: oskari_maplayer_projections_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_maplayer_projections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_maplayer_projections_id_seq OWNER TO oskari;

--
-- Name: oskari_maplayer_projections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_maplayer_projections_id_seq OWNED BY public.oskari_maplayer_projections.id;


--
-- Name: oskari_permission; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_permission (
    id integer NOT NULL,
    oskari_resource_id bigint NOT NULL,
    external_type character varying(100),
    permission character varying(100),
    external_id character varying(1000)
);


ALTER TABLE public.oskari_permission OWNER TO oskari;

--
-- Name: TABLE oskari_permission; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_permission IS 'Permissions for resources';


--
-- Name: oskari_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_permission_id_seq OWNER TO oskari;

--
-- Name: oskari_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_permission_id_seq OWNED BY public.oskari_permission.id;


--
-- Name: oskari_resource; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_resource (
    id integer NOT NULL,
    resource_type character varying(100) NOT NULL,
    resource_mapping character varying(1000) NOT NULL
);


ALTER TABLE public.oskari_resource OWNER TO oskari;

--
-- Name: TABLE oskari_resource; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_resource IS 'Declares resources representing a map layer or functionality that permissions are linked to';


--
-- Name: oskari_resource_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_resource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_resource_id_seq OWNER TO oskari;

--
-- Name: oskari_resource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_resource_id_seq OWNED BY public.oskari_resource.id;


--
-- Name: oskari_role_external_mapping; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_role_external_mapping (
    roleid bigint NOT NULL,
    name character varying(50) NOT NULL,
    external_type character varying(50) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.oskari_role_external_mapping OWNER TO oskari;

--
-- Name: TABLE oskari_role_external_mapping; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_role_external_mapping IS 'For mapping roles from external system to Oskari roles (requires custom code to use)';


--
-- Name: oskari_role_oskari_user; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_role_oskari_user (
    id integer NOT NULL,
    role_id integer,
    user_id bigint
);


ALTER TABLE public.oskari_role_oskari_user OWNER TO oskari;

--
-- Name: TABLE oskari_role_oskari_user; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_role_oskari_user IS 'Bridge table connecting role and user';


--
-- Name: oskari_role_oskari_user_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_role_oskari_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_role_oskari_user_id_seq OWNER TO oskari;

--
-- Name: oskari_role_oskari_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_role_oskari_user_id_seq OWNED BY public.oskari_role_oskari_user.id;


--
-- Name: oskari_roles; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_roles (
    id integer NOT NULL,
    name text NOT NULL,
    is_guest boolean DEFAULT false
);


ALTER TABLE public.oskari_roles OWNER TO oskari;

--
-- Name: TABLE oskari_roles; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_roles IS 'Roles that have associated permissions';


--
-- Name: oskari_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_roles_id_seq OWNER TO oskari;

--
-- Name: oskari_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_roles_id_seq OWNED BY public.oskari_roles.id;


--
-- Name: oskari_statistical_datasource; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_statistical_datasource (
    id bigint NOT NULL,
    locale text DEFAULT '{}'::text NOT NULL,
    config text DEFAULT '{}'::text,
    plugin text NOT NULL
);


ALTER TABLE public.oskari_statistical_datasource OWNER TO oskari;

--
-- Name: TABLE oskari_statistical_datasource; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_statistical_datasource IS 'Data source for statistical data (thematic maps)';


--
-- Name: oskari_statistical_datasource_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_statistical_datasource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_statistical_datasource_id_seq OWNER TO oskari;

--
-- Name: oskari_statistical_datasource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_statistical_datasource_id_seq OWNED BY public.oskari_statistical_datasource.id;


--
-- Name: oskari_statistical_layer; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_statistical_layer (
    datasource_id integer NOT NULL,
    layer_id integer NOT NULL,
    config text DEFAULT '{}'::text
);


ALTER TABLE public.oskari_statistical_layer OWNER TO oskari;

--
-- Name: TABLE oskari_statistical_layer; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_statistical_layer IS 'Link map layers with region geometry to statistical data sources (based on data available on the data source)';


--
-- Name: oskari_status; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_status (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.oskari_status OWNER TO oskari;

--
-- Name: oskari_status_myapp; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_status_myapp (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.oskari_status_myapp OWNER TO oskari;

--
-- Name: oskari_status_myplaces; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_status_myplaces (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.oskari_status_myplaces OWNER TO oskari;

--
-- Name: oskari_status_peltodata; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_status_peltodata (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.oskari_status_peltodata OWNER TO oskari;

--
-- Name: oskari_status_userlayer; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_status_userlayer (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.oskari_status_userlayer OWNER TO oskari;

--
-- Name: oskari_terms_of_use_for_publishing; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_terms_of_use_for_publishing (
    userid bigint NOT NULL,
    agreed boolean DEFAULT false NOT NULL,
    "time" timestamp with time zone
);


ALTER TABLE public.oskari_terms_of_use_for_publishing OWNER TO oskari;

--
-- Name: TABLE oskari_terms_of_use_for_publishing; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_terms_of_use_for_publishing IS 'Approval of terms for publishing by user';


--
-- Name: oskari_user_indicator; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_user_indicator (
    id integer NOT NULL,
    user_id bigint,
    title character varying(1000),
    source character varying(1000),
    description character varying(1000),
    published boolean
);


ALTER TABLE public.oskari_user_indicator OWNER TO oskari;

--
-- Name: TABLE oskari_user_indicator; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_user_indicator IS 'Metadata for statistical indicators created by users';


--
-- Name: oskari_user_indicator_data; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_user_indicator_data (
    id integer NOT NULL,
    indicator_id integer NOT NULL,
    regionset_id integer NOT NULL,
    year integer NOT NULL,
    data text NOT NULL
);


ALTER TABLE public.oskari_user_indicator_data OWNER TO oskari;

--
-- Name: TABLE oskari_user_indicator_data; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_user_indicator_data IS 'Data for statistical indicators created by users';


--
-- Name: oskari_user_indicator_data_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_user_indicator_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_user_indicator_data_id_seq OWNER TO oskari;

--
-- Name: oskari_user_indicator_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_user_indicator_data_id_seq OWNED BY public.oskari_user_indicator_data.id;


--
-- Name: oskari_user_indicator_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_user_indicator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_user_indicator_id_seq OWNER TO oskari;

--
-- Name: oskari_user_indicator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_user_indicator_id_seq OWNED BY public.oskari_user_indicator.id;


--
-- Name: oskari_users; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_users (
    id integer NOT NULL,
    user_name character varying(128) NOT NULL,
    first_name character varying(128),
    last_name character varying(128),
    email character varying(256),
    uuid character varying(64),
    attributes text DEFAULT '{}'::text
);


ALTER TABLE public.oskari_users OWNER TO oskari;

--
-- Name: TABLE oskari_users; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_users IS 'Oskari instance user accounts';


--
-- Name: oskari_users_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_users_id_seq OWNER TO oskari;

--
-- Name: oskari_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_users_id_seq OWNED BY public.oskari_users.id;


--
-- Name: oskari_users_pending; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_users_pending (
    id bigint NOT NULL,
    user_name text,
    email text,
    uuid text,
    expiry_timestamp timestamp with time zone
);


ALTER TABLE public.oskari_users_pending OWNER TO oskari;

--
-- Name: TABLE oskari_users_pending; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_users_pending IS 'Users that have started registration process but not completed it yet (used when end-user registration is enabled)';


--
-- Name: oskari_users_pending_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_users_pending_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_users_pending_id_seq OWNER TO oskari;

--
-- Name: oskari_users_pending_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_users_pending_id_seq OWNED BY public.oskari_users_pending.id;


--
-- Name: oskari_wfs_search_channels; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.oskari_wfs_search_channels (
    id bigint NOT NULL,
    wfs_layer_id integer NOT NULL,
    params_for_search text NOT NULL,
    is_default boolean,
    locale text DEFAULT '{}'::text,
    config text DEFAULT '{}'::text
);


ALTER TABLE public.oskari_wfs_search_channels OWNER TO oskari;

--
-- Name: TABLE oskari_wfs_search_channels; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.oskari_wfs_search_channels IS 'Configuration for using WFS-services as search services';


--
-- Name: oskari_wfs_search_channels_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.oskari_wfs_search_channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oskari_wfs_search_channels_id_seq OWNER TO oskari;

--
-- Name: oskari_wfs_search_channels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.oskari_wfs_search_channels_id_seq OWNED BY public.oskari_wfs_search_channels.id;


--
-- Name: peltodata_field; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.peltodata_field (
    id bigint NOT NULL,
    description character varying(256),
    user_id integer,
    crop_type character varying(20) DEFAULT 'wheat'::character varying NOT NULL,
    sowing_date date DEFAULT '2019-05-01'::date NOT NULL,
    farm_id character varying(30) DEFAULT 'test'::character varying,
    maplayergroup_id integer
);


ALTER TABLE public.peltodata_field OWNER TO oskari;

--
-- Name: peltodata_field_execution; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.peltodata_field_execution (
    id bigint NOT NULL,
    state integer,
    execution_started_at timestamp without time zone DEFAULT now(),
    field_id bigint NOT NULL,
    output_type character varying(50) NOT NULL,
    output_filename character varying(1024),
    field_file_id bigint
);


ALTER TABLE public.peltodata_field_execution OWNER TO oskari;

--
-- Name: COLUMN peltodata_field_execution.state; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON COLUMN public.peltodata_field_execution.state IS '-10 = error, 0 = started, 10 = completed';


--
-- Name: peltodata_field_execution_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.peltodata_field_execution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peltodata_field_execution_id_seq OWNER TO oskari;

--
-- Name: peltodata_field_execution_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.peltodata_field_execution_id_seq OWNED BY public.peltodata_field_execution.id;


--
-- Name: peltodata_field_file; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.peltodata_field_file (
    id bigint NOT NULL,
    original_file_name character varying(255) NOT NULL,
    full_path character varying(1024) NOT NULL,
    file_date date NOT NULL,
    type character varying(100),
    field_id bigint
);


ALTER TABLE public.peltodata_field_file OWNER TO oskari;

--
-- Name: peltodata_field_file_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.peltodata_field_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peltodata_field_file_id_seq OWNER TO oskari;

--
-- Name: peltodata_field_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.peltodata_field_file_id_seq OWNED BY public.peltodata_field_file.id;


--
-- Name: peltodata_field_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.peltodata_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peltodata_field_id_seq OWNER TO oskari;

--
-- Name: peltodata_field_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.peltodata_field_id_seq OWNED BY public.peltodata_field.id;


--
-- Name: peltodata_field_layer; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.peltodata_field_layer (
    field_id integer NOT NULL,
    layer_id integer NOT NULL
);


ALTER TABLE public.peltodata_field_layer OWNER TO oskari;

--
-- Name: peltodata_peltolohkot; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.peltodata_peltolohkot (
    gid integer NOT NULL,
    ely character varying(2),
    knro character varying(3),
    tiltu character varying(10),
    lohko character varying(10),
    pinta_ala double precision,
    ymparys double precision,
    geom public.geometry(MultiPolygon,3067)
);


ALTER TABLE public.peltodata_peltolohkot OWNER TO oskari;

--
-- Name: peltodata_peltolohkot_gid_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.peltodata_peltolohkot_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peltodata_peltolohkot_gid_seq OWNER TO oskari;

--
-- Name: peltodata_peltolohkot_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.peltodata_peltolohkot_gid_seq OWNED BY public.peltodata_peltolohkot.gid;


--
-- Name: portti_bundle; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.portti_bundle (
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    config character varying(20000) DEFAULT '{}'::character varying,
    state character varying(20000) DEFAULT '{}'::character varying,
    startup character varying(20000),
    CONSTRAINT nullchk CHECK ((startup IS NULL))
);


ALTER TABLE public.portti_bundle OWNER TO oskari;

--
-- Name: TABLE portti_bundle; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.portti_bundle IS 'List of available front-end functionality modules';


--
-- Name: COLUMN portti_bundle.startup; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON COLUMN public.portti_bundle.startup IS 'Deprecated column, always NULL';


--
-- Name: portti_bundle_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.portti_bundle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.portti_bundle_id_seq OWNER TO oskari;

--
-- Name: portti_bundle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.portti_bundle_id_seq OWNED BY public.portti_bundle.id;


--
-- Name: portti_inspiretheme_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.portti_inspiretheme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.portti_inspiretheme_id_seq OWNER TO oskari;

--
-- Name: portti_inspiretheme_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.portti_inspiretheme_id_seq OWNED BY public.oskari_maplayer_group.id;


--
-- Name: portti_keyword_association; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.portti_keyword_association (
    keyid1 bigint NOT NULL,
    keyid2 bigint NOT NULL,
    type character varying(10)
);


ALTER TABLE public.portti_keyword_association OWNER TO oskari;

--
-- Name: TABLE portti_keyword_association; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.portti_keyword_association IS 'Conceptual linking of keywords';


--
-- Name: portti_keywords; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.portti_keywords (
    id integer NOT NULL,
    keyword character varying(2000),
    uri character varying(2000),
    lang character varying(10),
    editable boolean
);


ALTER TABLE public.portti_keywords OWNER TO oskari;

--
-- Name: TABLE portti_keywords; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.portti_keywords IS 'Keywords that can be associated with resources';


--
-- Name: portti_keywords_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.portti_keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.portti_keywords_id_seq OWNER TO oskari;

--
-- Name: portti_keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.portti_keywords_id_seq OWNED BY public.portti_keywords.id;


--
-- Name: portti_layer_keywords; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.portti_layer_keywords (
    keyid bigint NOT NULL,
    layerid bigint NOT NULL
);


ALTER TABLE public.portti_layer_keywords OWNER TO oskari;

--
-- Name: TABLE portti_layer_keywords; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.portti_layer_keywords IS 'Bridge table linking map layers and keywords describing them';


--
-- Name: portti_view; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.portti_view (
    uuid uuid,
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    is_default boolean DEFAULT false,
    type character varying(16) DEFAULT 'USER'::character varying,
    description character varying(2000),
    page character varying(128) DEFAULT 'index'::character varying,
    application character varying(128) DEFAULT 'servlet'::character varying,
    application_dev_prefix character varying(256) DEFAULT '/applications/sample'::character varying,
    only_uuid boolean DEFAULT false,
    creator bigint DEFAULT '-1'::integer,
    domain character varying(512) DEFAULT ''::character varying,
    lang character varying(2) DEFAULT 'en'::character varying,
    is_public boolean DEFAULT false,
    metadata text DEFAULT '{}'::text,
    old_id bigint DEFAULT '-1'::integer,
    created timestamp without time zone DEFAULT now(),
    used timestamp without time zone DEFAULT now() NOT NULL,
    usagecount bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.portti_view OWNER TO oskari;

--
-- Name: TABLE portti_view; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.portti_view IS 'Map views/appsetups';


--
-- Name: portti_view_bundle_seq; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.portti_view_bundle_seq (
    view_id bigint NOT NULL,
    bundle_id bigint NOT NULL,
    seqno integer NOT NULL,
    config text DEFAULT '{}'::text,
    state text DEFAULT '{}'::text,
    startup text,
    bundleinstance character varying(128) DEFAULT ''::character varying,
    CONSTRAINT nullchk CHECK ((startup IS NULL))
);


ALTER TABLE public.portti_view_bundle_seq OWNER TO oskari;

--
-- Name: TABLE portti_view_bundle_seq; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.portti_view_bundle_seq IS 'Bundles present in a view/appsetup and their loading order';


--
-- Name: COLUMN portti_view_bundle_seq.startup; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON COLUMN public.portti_view_bundle_seq.startup IS 'Deprecated column, always NULL';


--
-- Name: portti_view_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.portti_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.portti_view_id_seq OWNER TO oskari;

--
-- Name: portti_view_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.portti_view_id_seq OWNED BY public.portti_view.id;


--
-- Name: ratings; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.ratings (
    id bigint NOT NULL,
    userid bigint NOT NULL,
    rating integer,
    category character varying(64) NOT NULL,
    categoryitem character varying(64) NOT NULL,
    comment character varying(1024),
    userrole character varying(64),
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.ratings OWNER TO oskari;

--
-- Name: TABLE ratings; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.ratings IS 'Table for rating metadata/anything really (not used currently)';


--
-- Name: ratings_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.ratings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ratings_id_seq OWNER TO oskari;

--
-- Name: ratings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.ratings_id_seq OWNED BY public.ratings.id;


--
-- Name: ratings_userid_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.ratings_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ratings_userid_seq OWNER TO oskari;

--
-- Name: ratings_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.ratings_userid_seq OWNED BY public.ratings.userid;


--
-- Name: user_layer; Type: TABLE; Schema: public; Owner: oskari
--

CREATE TABLE public.user_layer (
    id bigint NOT NULL,
    uuid character varying(64),
    layer_name character varying(256) NOT NULL,
    layer_desc character varying(256),
    layer_source character varying(256),
    publisher_name character varying(256),
    created timestamp with time zone DEFAULT now() NOT NULL,
    updated timestamp with time zone,
    fields json,
    wkt character varying(512) DEFAULT ''::character varying,
    options json
);


ALTER TABLE public.user_layer OWNER TO oskari;

--
-- Name: TABLE user_layer; Type: COMMENT; Schema: public; Owner: oskari
--

COMMENT ON TABLE public.user_layer IS 'Vector layer imported by user';


--
-- Name: user_layer_data_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.user_layer_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_layer_data_id_seq OWNER TO oskari;

--
-- Name: user_layer_id_seq; Type: SEQUENCE; Schema: public; Owner: oskari
--

CREATE SEQUENCE public.user_layer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_layer_id_seq OWNER TO oskari;

--
-- Name: user_layer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: oskari
--

ALTER SEQUENCE public.user_layer_id_seq OWNED BY public.user_layer.id;


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: oskari_capabilities_cache id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_capabilities_cache ALTER COLUMN id SET DEFAULT nextval('public.oskari_capabilities_cache_id_seq'::regclass);


--
-- Name: oskari_dataprovider id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_dataprovider ALTER COLUMN id SET DEFAULT nextval('public.oskari_layergroup_id_seq'::regclass);


--
-- Name: oskari_jaas_users id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_jaas_users ALTER COLUMN id SET DEFAULT nextval('public.oskari_jaas_users_id_seq'::regclass);


--
-- Name: oskari_maplayer id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer ALTER COLUMN id SET DEFAULT nextval('public.oskari_maplayer_id_seq'::regclass);


--
-- Name: oskari_maplayer_group id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_group ALTER COLUMN id SET DEFAULT nextval('public.portti_inspiretheme_id_seq'::regclass);


--
-- Name: oskari_maplayer_metadata id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_metadata ALTER COLUMN id SET DEFAULT nextval('public.oskari_maplayer_metadata_id_seq'::regclass);


--
-- Name: oskari_maplayer_projections id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_projections ALTER COLUMN id SET DEFAULT nextval('public.oskari_maplayer_projections_id_seq'::regclass);


--
-- Name: oskari_permission id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_permission ALTER COLUMN id SET DEFAULT nextval('public.oskari_permission_id_seq'::regclass);


--
-- Name: oskari_resource id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_resource ALTER COLUMN id SET DEFAULT nextval('public.oskari_resource_id_seq'::regclass);


--
-- Name: oskari_role_oskari_user id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_role_oskari_user ALTER COLUMN id SET DEFAULT nextval('public.oskari_role_oskari_user_id_seq'::regclass);


--
-- Name: oskari_roles id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_roles ALTER COLUMN id SET DEFAULT nextval('public.oskari_roles_id_seq'::regclass);


--
-- Name: oskari_statistical_datasource id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_statistical_datasource ALTER COLUMN id SET DEFAULT nextval('public.oskari_statistical_datasource_id_seq'::regclass);


--
-- Name: oskari_user_indicator id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator ALTER COLUMN id SET DEFAULT nextval('public.oskari_user_indicator_id_seq'::regclass);


--
-- Name: oskari_user_indicator_data id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator_data ALTER COLUMN id SET DEFAULT nextval('public.oskari_user_indicator_data_id_seq'::regclass);


--
-- Name: oskari_users id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_users ALTER COLUMN id SET DEFAULT nextval('public.oskari_users_id_seq'::regclass);


--
-- Name: oskari_users_pending id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_users_pending ALTER COLUMN id SET DEFAULT nextval('public.oskari_users_pending_id_seq'::regclass);


--
-- Name: oskari_wfs_search_channels id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_wfs_search_channels ALTER COLUMN id SET DEFAULT nextval('public.oskari_wfs_search_channels_id_seq'::regclass);


--
-- Name: peltodata_field id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field ALTER COLUMN id SET DEFAULT nextval('public.peltodata_field_id_seq'::regclass);


--
-- Name: peltodata_field_execution id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_execution ALTER COLUMN id SET DEFAULT nextval('public.peltodata_field_execution_id_seq'::regclass);


--
-- Name: peltodata_field_file id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_file ALTER COLUMN id SET DEFAULT nextval('public.peltodata_field_file_id_seq'::regclass);


--
-- Name: peltodata_peltolohkot gid; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_peltolohkot ALTER COLUMN gid SET DEFAULT nextval('public.peltodata_peltolohkot_gid_seq'::regclass);


--
-- Name: portti_bundle id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_bundle ALTER COLUMN id SET DEFAULT nextval('public.portti_bundle_id_seq'::regclass);


--
-- Name: portti_keywords id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_keywords ALTER COLUMN id SET DEFAULT nextval('public.portti_keywords_id_seq'::regclass);


--
-- Name: portti_view id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_view ALTER COLUMN id SET DEFAULT nextval('public.portti_view_id_seq'::regclass);


--
-- Name: ratings id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.ratings ALTER COLUMN id SET DEFAULT nextval('public.ratings_id_seq'::regclass);


--
-- Name: ratings userid; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.ratings ALTER COLUMN userid SET DEFAULT nextval('public.ratings_userid_seq'::regclass);


--
-- Name: user_layer id; Type: DEFAULT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.user_layer ALTER COLUMN id SET DEFAULT nextval('public.user_layer_id_seq'::regclass);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.categories (id, category_name, "default", uuid, publisher_name, options) FROM stdin;
\.


--
-- Data for Name: gt_pk_metadata_table; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.gt_pk_metadata_table (table_schema, table_name, pk_column, pk_column_idx, pk_policy, pk_sequence) FROM stdin;
\.


--
-- Data for Name: oskari_backendstatus; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_backendstatus (ts, maplayer_id, status, statusmessage, infourl) FROM stdin;
\.


--
-- Data for Name: oskari_capabilities_cache; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_capabilities_cache (id, layertype, url, data, created, updated, version) FROM stdin;
\.


--
-- Data for Name: oskari_dataprovider; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_dataprovider (id, locale) FROM stdin;
3	{"fi":{"name":"Porin kaupunki"},"sv":{"name":"Björneborg"},"en":{"name":"City of Pori"}}
4	{"fi":{"name":"GTK"},"sv":{"name":"GTK"},"en":{"name":"GTK"}}
5	{"fi":{"name":"Sentinel"},"sv":{"name":"Sentinel"},"en":{"name":"Sentinel"}}
6	{"fi":{"name":"MML"},"sv":{"name":"MML"},"en":{"name":"MML"}}
7	{"fi":{"name":"SYKE"},"sv":{"name":"SYKE"},"en":{"name":"SYKE"}}
9	{"fi":{"name":"OpenStreetMap"},"sv":{"name":"OpenStreetMap"},"en":{"name":"OpenStreetMap"}}
1	{"fi":{"name":"MIKADATA_internal_Geoserver"},"sv":{"name":"MIKADATA_internal_Geoserver"},"en":{"name":"MIKADATA_internal_Geoserver"}}
11	{"fi":{"name":"Ilmatieteen laitos"},"sv":{"name":"Ilmatieteen laitos"},"en":{"name":"Ilmatieteen laitos"}}
15	{"fi":{"name":"Turun kaupunki"},"sv":{"name":"Turun kaupunki"},"en":{"name":"Turun kaupunki"}}
16	{"fi":{"name":"MapAnt"},"sv":{"name":"MapAnt"},"en":{"name":"MapAnt"}}
17	{"fi":{"name":"Terrestris"},"sv":{"name":"Terrestris"},"en":{"name":"Terrestris"},"es":{"name":"Terrestris"}}
18	{"fi":{"name":"SYKE"},"sv":{"name":"SYKE"},"en":{"name":"SYKE"},"es":{"name":"SYKE"}}
19	{"fi":{"name":"Yieldgap"},"sv":{"name":"Yieldgap"},"en":{"name":"Yieldgap"},"es":{"name":"Yieldgap"}}
\.


--
-- Data for Name: oskari_jaas_users; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_jaas_users (id, login, password) FROM stdin;
1	admin	MD5:f75b872a23cd32775f7c3a2e4f974cda
2	sami	MD5:f75b872a23cd32775f7c3a2e4f974cda
\.


--
-- Data for Name: oskari_maplayer; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_maplayer (id, parentid, type, base_map, dataprovider_id, name, url, locale, opacity, style, minscale, maxscale, legend_image, metadataid, params, options, gfi_type, gfi_xslt, gfi_content, realtime, refresh_rate, created, updated, username, password, srs_name, version, attributes, capabilities, capabilities_last_updated, capabilities_update_rate_sec, internal) FROM stdin;
4	-1	wmtslayer	f	3	Ilmakuva1946_Pori	https://tiles.arcgis.com/tiles/5z5pRMWtM6euUyrs/arcgis/rest/services/Ilmakuva1946_Pori/MapServer/WMTS/1.0.0/WMTSCapabilities.xml?cacheKey=b4fdb30c1ef758c8	{"fi":{"subtitle":"","name":"Porin ilmakuva 1946"},"sv":{"subtitle":"","name":"Porin ilmakuva 1946"},"en":{"subtitle":"","name":"Porin ilmakuva 1946"}}	100	default	-1	-1			{}	{"requestEncoding":"REST","urlTemplate":"https://tiles.arcgis.com/tiles/5z5pRMWtM6euUyrs/arcgis/rest/services/Ilmakuva1946_Pori/MapServer/WMTS/tile/1.0.0/Ilmakuva1946_Pori/{Style}/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.png","format":"image/png"}	\N	\N		f	0	2019-02-19 14:15:12.741+02	2019-11-24 22:29:40.528+02			EPSG:3067	1.0.0	{}	{"srs":[],"tileMatrixIds":[{"EPSG:3857":"default028mm"}]}	2020-04-18 15:03:29.337+03	0	f
1	-1	wfslayer	f	1	oskari:my_places	https://peltodata.fi/geoserver/oskari/ows	{"fi":{"name":"Oma karttataso"},"sv":{"name":"Mitt kartlager"},"en":{"name":"My map layer"},"is":{"name":"Kortalagið mitt"}}	50	\N	-1	-1	\N	\N	{}	{"renderMode":"vector","labelProperty":"attention_text"}	\N	\N	\N	f	0	\N	\N	admin	geoserver	EPSG:3067	1.1.0	{"data":{"filter":{"default":["name","place_desc","image_url","link"],"fi":["name","place_desc","image_url","link"]},"format":{"image_url":{"noLabel":true,"skipEmpty":true,"type":"image","params":{"link":true}},"place_desc":{"noLabel":true,"skipEmpty":true,"type":"p"},"name":{"noLabel":true,"type":"h3"},"link":{"skipEmpty":true,"type":"link"},"attention_text":{"type":"hidden"}},"locale":{"fi":{"image_url":"Kuvalinkki","place_desc":"Kuvaus","name":"Nimi","link":"Linkki","attention_text":"Teksti kartalla"},"sv":{"image_url":"Bild-URL","place_desc":"Beskrivelse","name":"Namn","link":"Webbaddress","attention_text":"Placera text på kartan"},"en":{"image_url":"Image URL","place_desc":"Description","name":"Name","link":"URL","attention_text":"Text on map"}}},"maxFeatures":2000,"namespaceURL":"http://www.oskari.org"}	{}	\N	0	t
120	-1	wmtslayer	f	6	taustakartta	https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/WMTSCapabilities.xml	{"fi":{"subtitle":"","name":"Taustakartta"},"sv":{"subtitle":"","name":"Taustakartta"},"en":{"subtitle":"","name":"Taustakartta"}}	100	default	-1	-1			{}	{"requestEncoding":"REST","urlTemplate":"https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/taustakartta/default/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.png","format":"image/png"}	\N	\N		f	0	2020-04-02 01:16:20.725+03	2020-04-02 01:16:20.725+03			EPSG:3067	1.0.0	{}	{"srs":["EPSG:3067"],"tileMatrixIds":[{"EPSG:3067":"ETRS-TM35FIN"},{"EPSG:3857":"WGS84_Pseudo-Mercator"}]}	2020-04-18 15:03:34.789+03	0	f
6	-1	wfslayer	f	1	maankaytto:AK_ASEMAKAAVAINDEKSI_VIEW	https://geodata.tampere.fi/geoserver/ows?	{"fi":{"subtitle":"","name":"Asemakaavaindeksi"},"sv":{"subtitle":"","name":"Asemakaavaindeksi"},"en":{"subtitle":"","name":"Asemakaavaindeksi"}}	100	Oletustyyli	15000000	1			{}	{}	\N	\N		f	0	2019-02-19 15:37:34.263+02	2019-02-19 15:37:34.263+02			EPSG:3067	1.1.0	{"maxFeatures":2000,"namespaceURL":"https://geodata.tampere.fi/ns/maankaytto","wpsParams":"{}"}	{"srs":["EPSG:3878"]}	2019-02-19 15:37:34.768+02	0	f
123	-1	wmslayer	f	4	varjostettu_korkeusmalli	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"varjostettu_korkeusmalli"},"sv":{"subtitle":"","name":"varjostettu_korkeusmalli"},"en":{"subtitle":"","name":"varjostettu_korkeusmalli"}}	100	default	-1	-1	\N		{}	{}	text/html			f	0	2020-04-04 00:10:28.539+03	2020-04-04 00:10:28.539+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=varjostettu_korkeusmalli","name":"default","title":"varjostettu_korkeusmalli"}],"geom":"POLYGON ((16.170261 59.663009, 16.170261 70.095004, 33.107723 70.095004, 33.107723 59.663009, 16.170261 59.663009))","version":"1.3.0"}	2020-04-18 15:03:34.456+03	0	f
124	-1	wmslayer	f	4	maapera_20k_pintamaalajit	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"maapera_20k_pintamaalajit"},"sv":{"subtitle":"","name":"maapera_20k_pintamaalajit"},"en":{"subtitle":"","name":"maapera_20k_pintamaalajit"}}	100	default	-1	-1	\N		{}	{}	text/html			f	0	2020-04-04 00:12:15.49+03	2020-04-04 00:12:15.49+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=maapera_20k_pintamaalajit","name":"default","title":"maapera_20k_pintamaalajit"}],"geom":"POLYGON ((16.822327 59.534743, 16.822327 68.663294, 31.999602 68.663294, 31.999602 59.534743, 16.822327 59.534743))","version":"1.3.0"}	2020-04-18 15:03:34.461+03	0	f
155	-1	wmtslayer	f	6	maastokartta	https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/WMTSCapabilities.xml	{"fi":{"name":"Maastokartta"},"sv":{"name":"Maastokartta"},"en":{"name":"Maastokartta"},"es":{"name":"Maastokartta"}}	100	default	-1	-1	\N	\N	{}	{"requestEncoding":"REST","urlTemplate":"https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/maastokartta/default/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.png","format":"image/png"}	\N	\N	\N	f	0	2021-02-23 10:14:13.488+02	2021-05-04 14:50:34.386+03	8d3640ee-e25c-485f-b161-26506103f2a4	\N	EPSG:3067		{}	{"formats":{"available":[]},"srs":["EPSG:3067"],"isQueryable":false,"styles":[{"name":"default","title":"default"}],"tileMatrixIds":[{"EPSG:3067":"ETRS-TM35FIN"},{"EPSG:3857":"WGS84_Pseudo-Mercator"}]}	2021-05-04 14:50:34.451+03	-1	f
126	-1	wmslayer	f	4	maapera_20k_pienet_reunamoreenit	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"maapera_20k_pienet_reunamoreenit"},"sv":{"subtitle":"","name":"maapera_20k_pienet_reunamoreenit"},"en":{"subtitle":"","name":"maapera_20k_pienet_reunamoreenit"}}	100	default	-1	-1	\N		{}	{}	text/html			f	0	2020-04-04 00:14:22.183+03	2020-04-04 00:17:04.088+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=maapera_20k_pienet_reunamoreenit","name":"default","title":"maapera_20k_pienet_reunamoreenit"}],"geom":"POLYGON ((18.501084 59.740818, 18.501084 64.911135, 30.257204 64.911135, 30.257204 59.740818, 18.501084 59.740818))","version":"1.3.0"}	2020-04-18 15:03:34.465+03	0	f
98	-1	wmslayer	f	3	Opaskartta	https://kartta.pori.fi/TeklaOGCWeb/WMS.ashx	{"fi":{"subtitle":"","name":"Porin opaskartta"},"sv":{"subtitle":"","name":"Porin opaskartta"},"en":{"subtitle":"","name":"Porin opaskartta"}}	100		-1	-1			{}	{}	\N			f	0	2019-11-24 22:32:13.923+02	2019-11-24 22:32:13.923+02			EPSG:3067	1.1.1	{}	{"formats":{"available":[]},"srs":["EPSG:3067"],"isQueryable":false,"styles":[],"version":"1.1.1"}	2020-04-18 15:03:28.029+03	0	f
125	-1	wmslayer	f	4	maapera_20k_pohjamaalajit	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"maapera_20k_pohjamaalajit"},"sv":{"subtitle":"","name":"maapera_20k_pohjamaalajit"},"en":{"subtitle":"","name":"maapera_20k_pohjamaalajit"}}	100	default	-1	-1	\N		{}	{}	text/html			f	0	2020-04-04 00:13:26.738+03	2020-04-04 00:13:26.738+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=maapera_20k_pohjamaalajit","name":"default","title":"maapera_20k_pohjamaalajit"}],"geom":"POLYGON ((16.822327 59.534743, 16.822327 68.663294, 31.999602 68.663294, 31.999602 59.534743, 16.822327 59.534743))","version":"1.3.0"}	2020-04-18 15:03:34.469+03	0	f
36	-1	wmslayer	f	7	EO_HR_RGB_S2_20180602T000000_20180602T000000_FIN	http://geoserver2.ymparisto.fi:8080/geoserver/eo_hr_rgb/wms	{"fi":{"subtitle":"","name":"EO_HR_RGB_S2_20180602T000000_20180602T000000_FIN"},"sv":{"subtitle":"","name":"EO_HR_RGB_S2_20180602T000000_20180602T000000_FIN"},"en":{"subtitle":"","name":"EO_HR_RGB_S2_20180602T000000_20180602T000000_FIN"}}	100		-1	-1			{}	{}	text/html			f	0	2019-10-20 21:02:53.036+03	2019-10-20 21:02:53.036+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.gml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[],"geom":"POLYGON ((18.30448628494206 58.17705196066443, 18.30448628494206 70.59117021016672, 35.73983513350495 70.59117021016672, 35.73983513350495 58.17705196066443, 18.30448628494206 58.17705196066443))","version":"1.3.0"}	2019-11-21 01:12:10.993+02	0	f
34	-1	wmslayer	f	7	EO_HR_RGB_S2_20180605T000000_20180605T000000_UTM34	http://geoserver2.ymparisto.fi:8080/geoserver/eo_hr_rgb/wms	{"fi":{"subtitle":"","name":"EO_HR_RGB_S2_20180605T000000_20180605T000000_UTM34"},"sv":{"subtitle":"","name":"EO_HR_RGB_S2_20180605T000000_20180605T000000_UTM34"},"en":{"subtitle":"","name":"EO_HR_RGB_S2_20180605T000000_20180605T000000_UTM34"}}	100	raster	-1	-1	\N		{}	{}	text/html			f	0	2019-10-20 21:00:31.128+03	2019-10-20 21:00:31.128+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.gml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://geoserver2.ymparisto.fi:8080/geoserver/eo_hr_rgb/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=EO_HR_RGB_S2_20180605T000000_20180605T000000_UTM34","name":"raster","title":"Default Raster"}],"geom":"POLYGON ((15.696740043973152 58.50176805740183, 15.696740043973152 70.30597046166572, 26.561618577744703 70.30597046166572, 26.561618577744703 58.50176805740183, 15.696740043973152 58.50176805740183))","version":"1.3.0"}	2019-11-21 01:12:11.001+02	0	f
100	-1	wfslayer	f	3	akaava:MaaMetsatalousalue	https://kartta.pori.fi/TeklaOGCWeb/WFS.ashx	{"fi":{"subtitle":"","name":"akaava:MaaMetsatalousalue"},"sv":{"subtitle":"","name":"akaava:MaaMetsatalousalue"},"en":{"subtitle":"","name":"akaava:MaaMetsatalousalue"}}	100	Oletustyyli	15000000	1			{}	{}	\N	\N		f	0	2019-11-24 22:38:44.974+02	2019-11-24 22:38:44.974+02			EPSG:3067	1.1.0	{"maxFeatures":2000,"namespaceURL":"http://www.paikkatietopalvelu.fi/gml/asemakaava","wpsParams":"{}"}	{"srs":["EPSG:4326"]}	2019-11-24 22:38:45.094+02	0	f
122	-1	wmslayer	f	4	maapera_20k_50k_kattavuus	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"maapera_20k_50k_kattavuus"},"sv":{"subtitle":"","name":"maapera_20k_50k_kattavuus"},"en":{"subtitle":"","name":"maapera_20k_50k_kattavuus"}}	100	default	-1	141741.071429	\N		{}	{}	text/html			f	0	2020-04-04 00:09:19.725+03	2020-04-04 00:16:14.594+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=maapera_20k_50k_kattavuus","name":"default","title":"maapera_20k_50k_kattavuus"}],"geom":"POLYGON ((16.767003 59.539899, 16.767003 68.84759, 32.040889 68.84759, 32.040889 59.539899, 16.767003 59.539899))","version":"1.3.0"}	2020-04-18 15:03:34.478+03	0	f
157	-1	wmslayer	f	18	HY.PhysicalWaters.Catchments.DrainageBasin	https://paikkatieto.ymparisto.fi/arcgis/services/INSPIRE/SYKE_Hydrografia/MapServer/WmsServer	{"fi":{"name":"Valuma-alueet"},"sv":{"name":"Valuma-alueet"},"en":{"subtitle":"Catchment areas","name":"Valuma-alueet"},"es":{"name":"Valuma-alueet"}}	100	default	725668	-1	\N	%7B44394B13-85D7-4998-BD06-8ADC77C7455C%7D	{}	{}	\N	\N	\N	f	0	2021-03-01 15:46:53.593+02	2021-03-01 15:49:22.093+02			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://paikkatieto.ymparisto.fi/arcgis/services/INSPIRE/SYKE_Hydrografia/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=HY.PhysicalWaters.Catchments.DrainageBasin","name":"default","title":"HY.PhysicalWaters.Catchments.DrainageBasin"}],"geom":"POLYGON ((16.3424 59.673963, 16.3424 70.300162, 33.831184 70.300162, 33.831184 59.673963, 16.3424 59.673963))","version":"1.3.0"}	2021-03-01 15:49:22.182+02	-1	f
11	-1	wmslayer	f	4	happamat_sulfaattimaat_250k_alueet	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"happamat_sulfaattimaat_250k_alueet"},"sv":{"subtitle":"","name":"happamat_sulfaattimaat_250k_alueet"},"en":{"subtitle":"","name":"happamat_sulfaattimaat_250k_alueet"}}	100	default	-1	-1	\N		{}	{}	text/html			f	0	2019-07-13 13:06:52.721+03	2020-04-04 00:15:17.825+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=happamat_sulfaattimaat_250k_alueet","name":"default","title":"happamat_sulfaattimaat_250k_alueet"}],"geom":"POLYGON ((20.131746 59.761107, 20.131746 66.859342, 26.6905 66.859342, 26.6905 59.761107, 20.131746 59.761107))","version":"1.3.0"}	2020-04-18 15:03:34.474+03	0	f
8	-1	wmslayer	f	4	maaperan_kerrostumat_ja_muodostumat	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"maaperan_kerrostumat_ja_muodostumat"},"sv":{"subtitle":"","name":"maaperan_kerrostumat_ja_muodostumat"},"en":{"subtitle":"","name":"maaperan_kerrostumat_ja_muodostumat"}}	100	default	-1	-1	\N		{}	{}	text/html			f	0	2019-07-13 12:34:33.774+03	2020-04-04 00:15:53.13+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=maaperan_kerrostumat_ja_muodostumat","name":"default","title":"maaperan_kerrostumat_ja_muodostumat"}],"geom":"POLYGON ((15.915344 59.609486, 15.915344 70.096255, 33.108048 70.096255, 33.108048 59.609486, 15.915344 59.609486))","version":"1.3.0"}	2020-04-18 15:03:34.482+03	0	f
35	-1	wmslayer	f	7	EO_HR_RGB_S2_20180604T000000_20180604T000000_UTM35_lzw_tiles	http://geoserver2.ymparisto.fi:8080/geoserver/eo_hr_rgb/wms	{"fi":{"subtitle":"","name":"EO_HR_RGB_S2_20180604T000000_20180604T000000_UTM35_lzw_tiles"},"sv":{"subtitle":"","name":"EO_HR_RGB_S2_20180604T000000_20180604T000000_UTM35_lzw_tiles"},"en":{"subtitle":"","name":"EO_HR_RGB_S2_20180604T000000_20180604T000000_UTM35_lzw_tiles"}}	100	raster	-1	-1	\N		{}	{}	text/html			f	0	2019-10-20 21:02:30.139+03	2019-10-20 21:02:30.139+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.gml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://geoserver2.ymparisto.fi:8080/geoserver/eo_hr_rgb/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=EO_HR_RGB_S2_20180604T000000_20180604T000000_UTM35_lzw_tiles","name":"raster","title":"Default Raster"}],"geom":"POLYGON ((21.69674004397315 58.50176805740183, 21.69674004397315 70.30597046166572, 32.5616185777447 70.30597046166572, 32.5616185777447 58.50176805740183, 21.69674004397315 58.50176805740183))","version":"1.3.0"}	2019-11-21 01:12:11.008+02	0	f
99	-1	wmslayer	f	3	Asemakaava	https://kartta.pori.fi/TeklaOGCWeb/WMS.ashx	{"fi":{"subtitle":"","name":"Porin asemakaava"},"sv":{"subtitle":"","name":"Porin asemakaava"},"en":{"subtitle":"","name":"Porin asemakaava"}}	100		-1	-1			{}	{}	\N			f	0	2019-11-24 22:33:06.269+02	2019-11-24 22:33:53.22+02			EPSG:3067	1.1.1	{}	{"formats":{"available":[]},"srs":["EPSG:3067"],"isQueryable":false,"styles":[],"version":"1.1.1"}	2020-04-18 15:03:28.031+03	0	f
7	-1	wmtslayer	f	1	georaster:opaskartta_EPSG_3067	https://georaster.tampere.fi/geoserver/gwc/service/wmts?	{"fi":{"subtitle":"","name":"Tampereen opaskartta (VÄRI, TM35)"},"sv":{"subtitle":"","name":"Tampereen opaskartta (VÄRI, TM35)"},"en":{"subtitle":"","name":"Tampereen opaskartta (VÄRI, TM35)"}}	100	raster	-1	-1			{}	{"requestEncoding":"REST","urlTemplate":"https://georaster.tampere.fi/geoserver/gwc/rest/wmts/georaster:opaskartta_EPSG_3067/{style}/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}?format=image/png","format":"image/png"}	\N	\N		f	0	2019-02-19 16:31:52.948+02	2019-02-19 16:36:15.859+02	admin	kosk1kara	EPSG:3067	1.0.0	{"forceProxy":true}	{"srs":["EPSG:3067"],"tileMatrixIds":[{"EPSG:3067":"JHS"}]}	2020-04-18 15:03:31.489+03	0	f
97	-1	wmslayer	f	3	Ilmakuva	https://kartta.pori.fi/TeklaOGCWeb/WMS.ashx	{"fi":{"subtitle":"","name":"Porin ilmakuva 2013"},"sv":{"subtitle":"","name":"Porin ilmakuva 2013"},"en":{"subtitle":"","name":"Porin ilmakuva 2013"}}	100		-1	-1			{}	{}	\N			f	0	2019-11-24 21:56:46.368+02	2019-11-24 22:57:24.982+02			EPSG:3067	1.1.1	{}	{"formats":{"available":[]},"srs":["EPSG:3067"],"isQueryable":false,"styles":[],"version":"1.1.1"}	2020-04-18 15:03:28.032+03	0	f
103	-1	wmslayer	f	15	Turunseutu ilmakuva 2015	https://opaskartta.turku.fi/TeklaOGCWeb/WMS.ashx	{"fi":{"subtitle":"","name":"Turunseutu ilmakuva 2015"},"sv":{"subtitle":"","name":"Turunseutu ilmakuva 2015"},"en":{"subtitle":"","name":"Turunseutu ilmakuva 2015"}}	100		-1	-1			{}	{}	\N			f	0	2019-12-02 21:13:17.72+02	2019-12-02 21:13:17.72+02			EPSG:3067	1.1.1	{}	{"formats":{"available":[]},"srs":["EPSG:3067"],"isQueryable":false,"styles":[],"version":"1.1.1"}	2020-04-18 15:03:35.168+03	0	f
105	-1	collection	f	3	105_group	collection	{"fi":{"name":""},"sv":{"name":""},"en":{"name":""}}	100	\N	-1	-1	\N	\N	{}	{}	\N	\N	\N	f	0	2019-12-02 23:27:04.468+02	2019-12-02 23:27:04.468+02	\N	\N	\N		{}	{}	\N	0	f
104	-1	wmslayer	f	15	Turku ilmakuva 1973	https://opaskartta.turku.fi/TeklaOGCWeb/WMS.ashx	{"fi":{"subtitle":"","name":"Turku ilmakuva 1973"},"sv":{"subtitle":"","name":"Turku ilmakuva 1973"},"en":{"subtitle":"","name":"Turku ilmakuva 1973"}}	100		-1	-1			{}	{}	\N			f	0	2019-12-02 21:14:25.244+02	2019-12-02 21:14:25.244+02			EPSG:3067	1.1.1	{}	{"formats":{"available":[]},"srs":["EPSG:3067"],"isQueryable":false,"styles":[],"version":"1.1.1"}	2020-04-18 15:03:35.173+03	0	f
108	-1	wmtslayer	f	16	MapAnt.fi	http://wmts.mapant.fi/wmts.xml	{"fi":{"subtitle":"","name":"MapAnt.fi ETRS-TM35FIN"},"sv":{"subtitle":"","name":"MapAnt.fi ETRS-TM35FIN"},"en":{"subtitle":"","name":"MapAnt.fi ETRS-TM35FIN"}}	100	default	-1	-1			{}	{"requestEncoding":"REST","urlTemplate":"http://wmts.mapant.fi/wmts.php?z={TileMatrix}&y={TileRow}&x={TileCol}","format":"image/png"}	\N	\N		f	0	2020-03-23 11:25:23.42+02	2020-03-23 11:25:23.42+02			EPSG:3067	1.0.0	{}	{"srs":["EPSG:3067"],"tileMatrixIds":[{"EPSG:3067":"ETRS-TM35FIN"}]}	2020-04-18 15:03:29.419+03	0	f
10	-1	wmslayer	f	4	maapera_200k_maalajit	http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WMSServer?	{"fi":{"subtitle":"","name":"maapera_200k_maalajit"},"sv":{"subtitle":"","name":"maapera_200k_maalajit"},"en":{"subtitle":"","name":"maapera_200k_maalajit"}}	100	default	-1	-1	\N		{}	{}	text/html			f	0	2019-07-13 12:54:53.683+03	2020-04-04 00:15:35.349+03			EPSG:3067	1.3.0	{}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.wms_xml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://gtkdata.gtk.fi/arcgis/services/Rajapinnat/GTK_Maapera_WMS/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=maapera_200k_maalajit","name":"default","title":"maapera_200k_maalajit"}],"geom":"POLYGON ((15.608183 59.362218, 15.608183 70.094943, 33.106637 70.094943, 33.106637 59.362218, 15.608183 59.362218))","version":"1.3.0"}	2020-04-18 15:03:34.486+03	0	f
107	-1	wmtslayer	f	6	ortokuva	https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/WMTSCapabilities.xml	{"fi":{"subtitle":"","name":"Ortokuva koko suomi"},"sv":{"subtitle":"","name":"Ortokuva koko suomi"},"en":{"subtitle":"","name":"Ortokuva koko suomi"}}	100	default	-1	-1			{}	{"requestEncoding":"REST","urlTemplate":"https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/ortokuva/default/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.jpg","format":"image/jpeg"}	\N	\N		f	0	2020-03-23 00:00:57.467+02	2020-03-23 00:02:26.458+02			EPSG:3067	1.0.0	{}	{"srs":["EPSG:3067"],"tileMatrixIds":[{"EPSG:3067":"ETRS-TM35FIN"}]}	2020-04-18 15:03:34.791+03	0	f
37	-1	wmslayer	f	11	Weather:temperature-forecast	http://openwms.fmi.fi/geoserver/wms?service=WMS&version=1.3.0&request=GetCapabilities&	{"fi":{"subtitle":"","name":"temperature-forecast"},"sv":{"subtitle":"","name":"temperature-forecast"},"en":{"subtitle":"","name":"temperature-forecast"}}	100	temperature-forecast-ec2	-1	-1	\N		{}	{}	text/html			f	0	2019-10-20 21:27:43.772+03	2019-10-20 21:27:43.772+03			EPSG:3067	1.3.0	{}	{"times":{"start":"2020-04-18T10:00:00.000Z","end":"2020-04-20T12:00:00.000Z","interval":"PT1H"},"formats":{"available":["text/html","text/plain","application/vnd.ogc.gml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://openwms.fmi.fi:80/geoserver/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=Weather%3Atemperature-forecast","name":"temperature-forecast-ec2","title":"Temperature forecast"}],"geom":"POLYGON ((-148.87069057296216 19.51932216775788, -148.87069057296216 90.0, 140.58726775221507 90.0, 140.58726775221507 19.51932216775788, -148.87069057296216 19.51932216775788))","version":"1.3.0"}	2020-04-18 15:03:33.465+03	0	f
134	-1	wmslayer	f	1	oskari:PeltoAI_Ortho_2019	https://peltodata.fi/geoserver/ows?	{"fi":{"subtitle":"","name":"Ortoilmakuva_2019"},"sv":{"subtitle":"","name":"Ortoilmakuva_2019"},"en":{"subtitle":"","name":"Ortoilmakuva_2019"},"es":{}}	100	raster	-1	-1	\N		{}	{}	text/html			f	0	2020-04-09 10:58:00.442+03	2021-05-19 14:44:37.182+03	oskari	$dt!LpftAiX64EXG	EPSG:3067	1.3.0	{"clip_user_fields":true}	{"formats":{"available":["text/html","text/plain","application/vnd.ogc.gml","text/xml"],"value":"text/html"},"srs":["EPSG:3067"],"isQueryable":true,"styles":[{"legend":"http://peltodata.fi/geoserver/ows?service=WMS&request=GetLegendGraphic&format=image%2Fpng&width=20&height=20&layer=oskari%3APeltoAI_Ortho_2019","name":"raster","title":"A boring default style"}],"geom":"POLYGON ((21.309708044579278 60.93736283195419, 21.309708044579278 62.460259297391886, 23.781143834997703 62.460259297391886, 23.781143834997703 60.93736283195419, 21.309708044579278 60.93736283195419))","version":"1.3.0"}	2021-05-19 14:44:37.424+03	-1	f
\.


--
-- Data for Name: oskari_maplayer_externalid; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_maplayer_externalid (maplayerid, externalid) FROM stdin;
\.


--
-- Data for Name: oskari_maplayer_group; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_maplayer_group (id, locale, parentid, selectable, order_number) FROM stdin;
1	{"fi":{"name":"Koordinaattijärjestelmät"}, "sv": { "name" : "Referenskoordinatsystem"},"en": { "name" : "Coordinate reference systems"}}	-1	t	1000000
11	{"fi":{"name":"Korkeus"}, "sv": { "name" : "Höjd"},"en": { "name" : "Elevation"}}	-1	t	1000000
13	{"fi":{"name":"Ortoilmakuvat"}, "sv": { "name" : "Ortofoto"},"en": { "name" : "Orthoimagery"}}	-1	t	1000000
14	{"fi":{"name":"Geologia"}, "sv": { "name" : "Geologi"},"en": { "name" : "Geology"}}	-1	t	1000000
18	{"fi":{"name":"Maankäyttö"}, "sv": { "name" : "Markanvändning"},"en": { "name" : "Land use"}}	-1	t	1000000
27	{"fi":{"name":"Ilmakehän tila"}, "sv": { "name" : "Atmosfäriska förhållanden"},"en": { "name" : "Atmospheric conditions"}}	-1	t	1000000
36	{"fi":{"name":"Taustakartat"}, "sv": { "name" : "Bakgrundskartor"},"en": { "name" : "Background maps"}}	-1	t	1000000
38	{"fi":{"name":"Opaskartat"}, "sv": { "name" : "Guidekartor"},"en": { "name" : "Guide maps"}}	-1	t	1000000
45	{"fi":{"name":"asemakaava"},"sv":{"name":"asemakaava"},"en":{"name":"asemakaava"}}	-1	t	1000000
49	{"fi":{"name":"Hydrologia"},"sv":{"name":"Hydrologi"},"en":{"name":"Hydrology"},"es":{"name":"Hüdroloogia"}}	-1	t	1000000
\.


--
-- Data for Name: oskari_maplayer_group_link; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_maplayer_group_link (maplayerid, groupid, order_number) FROM stdin;
6	18	1000000
7	38	1000000
34	1	1000000
35	1	1000000
36	1	1000000
37	27	1000000
157	49	1000000
155	36	1000000
4	13	1000000
97	13	1000000
98	38	1000000
99	45	1000000
100	18	1000000
103	13	1000000
104	13	1000000
105	13	1000000
107	13	1000000
108	11	1000000
120	36	1000000
123	11	1000000
124	14	1000000
125	14	1000000
11	14	1000000
10	14	1000000
8	14	1000000
122	14	1000000
126	14	1000000
134	13	1000000
\.


--
-- Data for Name: oskari_maplayer_metadata; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_maplayer_metadata (id, metadataid, wkt, json, ts) FROM stdin;
\.


--
-- Data for Name: oskari_maplayer_projections; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_maplayer_projections (id, name, maplayerid) FROM stdin;
\.


--
-- Data for Name: oskari_permission; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_permission (id, oskari_resource_id, external_type, permission, external_id) FROM stdin;
2908	155	ROLE	VIEW_LAYER	11
2909	155	ROLE	PUBLISH	11
2910	155	ROLE	DOWNLOAD	11
2911	155	ROLE	VIEW_PUBLISHED	11
2912	155	ROLE	VIEW_LAYER	10
2913	155	ROLE	PUBLISH	10
2914	155	ROLE	DOWNLOAD	10
2915	155	ROLE	VIEW_PUBLISHED	10
2916	155	ROLE	VIEW_LAYER	9
2917	155	ROLE	PUBLISH	9
2918	155	ROLE	DOWNLOAD	9
2919	155	ROLE	VIEW_PUBLISHED	9
2920	155	ROLE	VIEW_LAYER	8
2921	155	ROLE	PUBLISH	8
2922	155	ROLE	DOWNLOAD	8
2923	155	ROLE	VIEW_PUBLISHED	8
2924	155	ROLE	VIEW_LAYER	7
2925	155	ROLE	PUBLISH	7
2926	155	ROLE	DOWNLOAD	7
2927	155	ROLE	VIEW_PUBLISHED	7
2928	155	ROLE	VIEW_LAYER	6
2929	155	ROLE	PUBLISH	6
2930	155	ROLE	DOWNLOAD	6
2931	155	ROLE	VIEW_PUBLISHED	6
2932	155	ROLE	VIEW_LAYER	5
2933	155	ROLE	PUBLISH	5
2934	155	ROLE	DOWNLOAD	5
2935	155	ROLE	VIEW_PUBLISHED	5
2936	155	ROLE	VIEW_LAYER	4
2937	155	ROLE	PUBLISH	4
2938	155	ROLE	DOWNLOAD	4
2939	155	ROLE	VIEW_PUBLISHED	4
2940	155	ROLE	VIEW_LAYER	2
2941	155	ROLE	VIEW_LAYER	12
2942	155	ROLE	PUBLISH	12
2943	155	ROLE	DOWNLOAD	12
2944	155	ROLE	VIEW_PUBLISHED	12
2945	155	ROLE	VIEW_PUBLISHED	3
2946	155	ROLE	DOWNLOAD	3
2947	155	ROLE	PUBLISH	3
2948	155	ROLE	VIEW_LAYER	3
2990	233	ROLE	VIEW_PUBLISHED	3
2991	233	ROLE	DOWNLOAD	3
2992	233	ROLE	PUBLISH	3
2993	233	ROLE	VIEW_LAYER	3
3036	237	ROLE	VIEW_LAYER	2
3037	237	ROLE	VIEW_LAYER	1
3042	235	ROLE	VIEW_PUBLISHED	2
3043	235	ROLE	DOWNLOAD	2
3044	235	ROLE	PUBLISH	2
3045	235	ROLE	VIEW_LAYER	2
3046	235	ROLE	VIEW_PUBLISHED	1
3047	235	ROLE	DOWNLOAD	1
3048	235	ROLE	PUBLISH	1
3049	235	ROLE	VIEW_LAYER	1
1690	137	ROLE	VIEW_LAYER	1
1691	137	ROLE	VIEW_PUBLISHED	3
1692	137	ROLE	DOWNLOAD	3
1693	137	ROLE	PUBLISH	3
1694	137	ROLE	VIEW_LAYER	3
1695	137	ROLE	VIEW_PUBLISHED	12
1696	137	ROLE	DOWNLOAD	12
1697	137	ROLE	PUBLISH	12
1698	137	ROLE	VIEW_LAYER	12
1699	137	ROLE	VIEW_PUBLISHED	4
1700	137	ROLE	DOWNLOAD	4
1701	137	ROLE	PUBLISH	4
1702	137	ROLE	VIEW_LAYER	4
1703	137	ROLE	VIEW_PUBLISHED	5
1704	137	ROLE	DOWNLOAD	5
1705	137	ROLE	PUBLISH	5
1706	137	ROLE	VIEW_LAYER	5
1707	137	ROLE	VIEW_PUBLISHED	6
1708	137	ROLE	DOWNLOAD	6
1709	137	ROLE	PUBLISH	6
1710	137	ROLE	VIEW_LAYER	6
1711	137	ROLE	VIEW_PUBLISHED	7
1712	137	ROLE	DOWNLOAD	7
1713	137	ROLE	PUBLISH	7
1714	137	ROLE	VIEW_LAYER	7
1715	137	ROLE	VIEW_PUBLISHED	8
1716	137	ROLE	DOWNLOAD	8
1717	137	ROLE	PUBLISH	8
1718	137	ROLE	VIEW_LAYER	8
1719	137	ROLE	VIEW_PUBLISHED	9
1720	137	ROLE	DOWNLOAD	9
1721	137	ROLE	PUBLISH	9
1722	137	ROLE	VIEW_LAYER	9
1723	137	ROLE	VIEW_PUBLISHED	10
1724	137	ROLE	DOWNLOAD	10
1725	137	ROLE	PUBLISH	10
1726	137	ROLE	VIEW_LAYER	10
1727	137	ROLE	VIEW_PUBLISHED	11
1728	137	ROLE	DOWNLOAD	11
1729	137	ROLE	PUBLISH	11
1730	137	ROLE	VIEW_LAYER	11
1731	138	ROLE	VIEW_LAYER	1
1732	138	ROLE	VIEW_PUBLISHED	3
1733	138	ROLE	DOWNLOAD	3
1734	138	ROLE	PUBLISH	3
1735	138	ROLE	VIEW_LAYER	3
1736	138	ROLE	VIEW_PUBLISHED	12
1737	138	ROLE	DOWNLOAD	12
1738	138	ROLE	PUBLISH	12
1739	138	ROLE	VIEW_LAYER	12
1740	138	ROLE	VIEW_PUBLISHED	4
1741	138	ROLE	DOWNLOAD	4
1742	138	ROLE	PUBLISH	4
1743	138	ROLE	VIEW_LAYER	4
1744	138	ROLE	VIEW_PUBLISHED	5
1745	138	ROLE	DOWNLOAD	5
1746	138	ROLE	PUBLISH	5
1747	138	ROLE	VIEW_LAYER	5
1748	138	ROLE	VIEW_PUBLISHED	6
1749	138	ROLE	DOWNLOAD	6
1750	138	ROLE	PUBLISH	6
1751	138	ROLE	VIEW_LAYER	6
1752	138	ROLE	VIEW_PUBLISHED	7
1753	138	ROLE	DOWNLOAD	7
1754	138	ROLE	PUBLISH	7
1755	138	ROLE	VIEW_LAYER	7
1756	138	ROLE	VIEW_PUBLISHED	8
1757	138	ROLE	DOWNLOAD	8
1758	138	ROLE	PUBLISH	8
1759	138	ROLE	VIEW_LAYER	8
1760	138	ROLE	VIEW_PUBLISHED	9
1761	138	ROLE	DOWNLOAD	9
1762	138	ROLE	PUBLISH	9
1763	138	ROLE	VIEW_LAYER	9
1764	138	ROLE	VIEW_PUBLISHED	10
1765	138	ROLE	DOWNLOAD	10
1766	138	ROLE	PUBLISH	10
1767	138	ROLE	VIEW_LAYER	10
1768	138	ROLE	VIEW_PUBLISHED	11
1769	138	ROLE	DOWNLOAD	11
1770	138	ROLE	PUBLISH	11
1771	138	ROLE	VIEW_LAYER	11
1773	139	ROLE	VIEW_LAYER	3
1775	139	ROLE	PUBLISH	3
1776	139	ROLE	DOWNLOAD	3
1777	139	ROLE	VIEW_PUBLISHED	3
1778	139	ROLE	VIEW_LAYER	1
1779	139	ROLE	VIEW_PUBLISHED	12
1780	139	ROLE	DOWNLOAD	12
1781	139	ROLE	PUBLISH	12
1782	139	ROLE	VIEW_LAYER	12
1783	139	ROLE	VIEW_PUBLISHED	4
1784	139	ROLE	DOWNLOAD	4
1785	139	ROLE	PUBLISH	4
1786	139	ROLE	VIEW_LAYER	4
1787	139	ROLE	VIEW_PUBLISHED	5
1788	139	ROLE	DOWNLOAD	5
1789	139	ROLE	PUBLISH	5
1790	139	ROLE	VIEW_LAYER	5
1791	139	ROLE	VIEW_PUBLISHED	6
1792	139	ROLE	DOWNLOAD	6
1793	139	ROLE	PUBLISH	6
1794	139	ROLE	VIEW_LAYER	6
1795	139	ROLE	VIEW_PUBLISHED	7
1796	139	ROLE	DOWNLOAD	7
1797	139	ROLE	PUBLISH	7
1798	139	ROLE	VIEW_LAYER	7
1799	139	ROLE	VIEW_PUBLISHED	8
1800	139	ROLE	DOWNLOAD	8
1801	139	ROLE	PUBLISH	8
1802	139	ROLE	VIEW_LAYER	8
1803	139	ROLE	VIEW_PUBLISHED	9
1804	139	ROLE	DOWNLOAD	9
1805	139	ROLE	PUBLISH	9
1806	139	ROLE	VIEW_LAYER	9
1807	139	ROLE	VIEW_PUBLISHED	10
1808	139	ROLE	DOWNLOAD	10
1809	139	ROLE	PUBLISH	10
1810	139	ROLE	VIEW_LAYER	10
1811	139	ROLE	VIEW_PUBLISHED	11
1812	139	ROLE	DOWNLOAD	11
1813	139	ROLE	PUBLISH	11
1814	139	ROLE	VIEW_LAYER	11
1857	142	ROLE	VIEW_LAYER	3
1859	142	ROLE	PUBLISH	3
1860	142	ROLE	DOWNLOAD	3
1861	142	ROLE	VIEW_PUBLISHED	3
1862	143	ROLE	VIEW_LAYER	3
1864	143	ROLE	PUBLISH	3
1865	143	ROLE	DOWNLOAD	3
1866	143	ROLE	VIEW_PUBLISHED	3
1867	143	ROLE	VIEW_LAYER	1
1868	144	ROLE	VIEW_LAYER	3
1870	144	ROLE	PUBLISH	3
1871	144	ROLE	DOWNLOAD	3
1872	144	ROLE	VIEW_PUBLISHED	3
1873	144	ROLE	VIEW_LAYER	1
1874	145	ROLE	VIEW_LAYER	3
1876	145	ROLE	PUBLISH	3
1877	145	ROLE	DOWNLOAD	3
1878	145	ROLE	VIEW_PUBLISHED	3
1879	145	ROLE	VIEW_PUBLISHED	7
1880	145	ROLE	DOWNLOAD	7
1881	145	ROLE	PUBLISH	7
1882	145	ROLE	VIEW_LAYER	7
1883	145	ROLE	VIEW_PUBLISHED	13
1884	145	ROLE	DOWNLOAD	13
1885	145	ROLE	PUBLISH	13
1886	145	ROLE	VIEW_LAYER	13
1887	146	ROLE	VIEW_LAYER	3
1889	146	ROLE	PUBLISH	3
1890	146	ROLE	DOWNLOAD	3
1891	146	ROLE	VIEW_PUBLISHED	3
1892	146	ROLE	VIEW_LAYER	1
1893	147	ROLE	VIEW_LAYER	3
1895	147	ROLE	PUBLISH	3
1896	147	ROLE	DOWNLOAD	3
1897	147	ROLE	VIEW_PUBLISHED	3
1898	147	ROLE	VIEW_LAYER	1
1899	148	ROLE	VIEW_LAYER	3
1901	148	ROLE	PUBLISH	3
1902	148	ROLE	DOWNLOAD	3
1903	148	ROLE	VIEW_PUBLISHED	3
1904	148	ROLE	VIEW_LAYER	1
1905	149	ROLE	VIEW_LAYER	2
1907	149	ROLE	VIEW_LAYER	3
1909	149	ROLE	PUBLISH	3
1910	149	ROLE	DOWNLOAD	3
1911	149	ROLE	VIEW_PUBLISHED	3
1912	150	ROLE	VIEW_LAYER	2
1914	150	ROLE	VIEW_LAYER	3
1916	150	ROLE	PUBLISH	3
1917	150	ROLE	DOWNLOAD	3
1918	150	ROLE	VIEW_PUBLISHED	3
1919	151	ROLE	VIEW_LAYER	3
1921	151	ROLE	PUBLISH	3
1922	151	ROLE	DOWNLOAD	3
1923	151	ROLE	VIEW_PUBLISHED	3
1924	151	ROLE	VIEW_LAYER	1
1925	152	ROLE	VIEW_LAYER	3
1927	152	ROLE	PUBLISH	3
1928	152	ROLE	DOWNLOAD	3
1929	152	ROLE	VIEW_PUBLISHED	3
1930	152	ROLE	VIEW_PUBLISHED	8
1931	152	ROLE	DOWNLOAD	8
1932	152	ROLE	PUBLISH	8
1933	152	ROLE	VIEW_LAYER	8
1934	152	ROLE	VIEW_PUBLISHED	13
1935	152	ROLE	DOWNLOAD	13
1936	152	ROLE	PUBLISH	13
1937	152	ROLE	VIEW_LAYER	13
1938	153	ROLE	VIEW_LAYER	3
1940	153	ROLE	PUBLISH	3
1941	153	ROLE	DOWNLOAD	3
1942	153	ROLE	VIEW_PUBLISHED	3
1943	153	ROLE	VIEW_PUBLISHED	5
1944	153	ROLE	DOWNLOAD	5
1945	153	ROLE	PUBLISH	5
1946	153	ROLE	VIEW_LAYER	5
1947	153	ROLE	VIEW_PUBLISHED	13
1948	153	ROLE	DOWNLOAD	13
1949	153	ROLE	PUBLISH	13
1950	153	ROLE	VIEW_LAYER	13
1951	154	ROLE	VIEW_LAYER	3
1953	154	ROLE	PUBLISH	3
1954	154	ROLE	DOWNLOAD	3
1955	154	ROLE	VIEW_PUBLISHED	3
1956	154	ROLE	VIEW_LAYER	1
2000	156	ROLE	VIEW_LAYER	3
2002	156	ROLE	PUBLISH	3
2003	156	ROLE	DOWNLOAD	3
2004	156	ROLE	VIEW_PUBLISHED	3
2005	157	ROLE	VIEW_LAYER	3
2007	157	ROLE	PUBLISH	3
2008	157	ROLE	DOWNLOAD	3
2009	157	ROLE	VIEW_PUBLISHED	3
2010	158	ROLE	VIEW_LAYER	3
2012	158	ROLE	PUBLISH	3
2013	158	ROLE	DOWNLOAD	3
2014	158	ROLE	VIEW_PUBLISHED	3
2015	159	ROLE	VIEW_LAYER	3
2017	159	ROLE	PUBLISH	3
2018	159	ROLE	DOWNLOAD	3
2019	159	ROLE	VIEW_PUBLISHED	3
2020	160	ROLE	VIEW_LAYER	3
2022	160	ROLE	PUBLISH	3
2023	160	ROLE	DOWNLOAD	3
2024	160	ROLE	VIEW_PUBLISHED	3
2025	161	ROLE	VIEW_LAYER	3
2027	161	ROLE	PUBLISH	3
2028	161	ROLE	DOWNLOAD	3
2029	161	ROLE	VIEW_PUBLISHED	3
2030	161	ROLE	DOWNLOAD	14
2031	161	ROLE	VIEW_LAYER	14
2032	162	ROLE	VIEW_LAYER	3
2034	162	ROLE	PUBLISH	3
2035	162	ROLE	DOWNLOAD	3
2036	162	ROLE	VIEW_PUBLISHED	3
2037	163	ROLE	VIEW_LAYER	3
2039	163	ROLE	PUBLISH	3
2040	163	ROLE	DOWNLOAD	3
2041	163	ROLE	VIEW_PUBLISHED	3
2042	163	ROLE	VIEW_PUBLISHED	8
2043	163	ROLE	DOWNLOAD	8
2044	163	ROLE	PUBLISH	8
2045	163	ROLE	VIEW_LAYER	8
2046	163	ROLE	VIEW_PUBLISHED	13
2047	163	ROLE	DOWNLOAD	13
2048	163	ROLE	PUBLISH	13
2049	163	ROLE	VIEW_LAYER	13
2050	164	ROLE	VIEW_LAYER	3
2052	164	ROLE	PUBLISH	3
2053	164	ROLE	DOWNLOAD	3
2054	164	ROLE	VIEW_PUBLISHED	3
2055	165	ROLE	VIEW_LAYER	3
2057	165	ROLE	PUBLISH	3
2058	165	ROLE	DOWNLOAD	3
2059	165	ROLE	VIEW_PUBLISHED	3
2060	165	ROLE	VIEW_PUBLISHED	7
2061	165	ROLE	DOWNLOAD	7
2062	165	ROLE	PUBLISH	7
2063	165	ROLE	VIEW_LAYER	7
2064	165	ROLE	VIEW_PUBLISHED	13
2065	165	ROLE	DOWNLOAD	13
2066	165	ROLE	PUBLISH	13
2067	165	ROLE	VIEW_LAYER	13
2068	166	ROLE	VIEW_LAYER	3
2070	166	ROLE	PUBLISH	3
2071	166	ROLE	DOWNLOAD	3
2072	166	ROLE	VIEW_PUBLISHED	3
2073	166	ROLE	VIEW_PUBLISHED	1
2074	166	ROLE	VIEW_LAYER	1
2075	166	ROLE	VIEW_PUBLISHED	12
2076	166	ROLE	DOWNLOAD	12
2077	166	ROLE	PUBLISH	12
2078	166	ROLE	VIEW_LAYER	12
2079	166	ROLE	VIEW_PUBLISHED	4
2080	166	ROLE	DOWNLOAD	4
2081	166	ROLE	PUBLISH	4
2082	166	ROLE	VIEW_LAYER	4
2083	166	ROLE	VIEW_PUBLISHED	5
2084	166	ROLE	DOWNLOAD	5
2085	166	ROLE	PUBLISH	5
2086	166	ROLE	VIEW_LAYER	5
2087	166	ROLE	VIEW_PUBLISHED	6
2088	166	ROLE	DOWNLOAD	6
2089	166	ROLE	PUBLISH	6
2090	166	ROLE	VIEW_LAYER	6
2091	166	ROLE	VIEW_PUBLISHED	7
2092	166	ROLE	DOWNLOAD	7
2093	166	ROLE	PUBLISH	7
2094	166	ROLE	VIEW_LAYER	7
2095	166	ROLE	VIEW_PUBLISHED	8
2096	166	ROLE	DOWNLOAD	8
2097	166	ROLE	PUBLISH	8
2098	166	ROLE	VIEW_LAYER	8
2099	166	ROLE	VIEW_PUBLISHED	9
2100	166	ROLE	DOWNLOAD	9
2101	166	ROLE	PUBLISH	9
2102	166	ROLE	VIEW_LAYER	9
2103	166	ROLE	VIEW_PUBLISHED	10
2104	166	ROLE	DOWNLOAD	10
2105	166	ROLE	PUBLISH	10
2106	166	ROLE	VIEW_LAYER	10
2107	166	ROLE	VIEW_PUBLISHED	11
2108	166	ROLE	DOWNLOAD	11
2109	166	ROLE	PUBLISH	11
2110	166	ROLE	VIEW_LAYER	11
2111	167	ROLE	VIEW_LAYER	3
2113	167	ROLE	PUBLISH	3
2114	167	ROLE	DOWNLOAD	3
2115	167	ROLE	VIEW_PUBLISHED	3
2116	167	ROLE	VIEW_LAYER	1
2117	167	ROLE	VIEW_PUBLISHED	12
2118	167	ROLE	DOWNLOAD	12
2119	167	ROLE	PUBLISH	12
2120	167	ROLE	VIEW_LAYER	12
2121	167	ROLE	VIEW_PUBLISHED	4
2122	167	ROLE	DOWNLOAD	4
2123	167	ROLE	PUBLISH	4
2124	167	ROLE	VIEW_LAYER	4
2125	167	ROLE	VIEW_PUBLISHED	5
2126	167	ROLE	DOWNLOAD	5
2127	167	ROLE	PUBLISH	5
2128	167	ROLE	VIEW_LAYER	5
2129	167	ROLE	VIEW_PUBLISHED	6
2130	167	ROLE	DOWNLOAD	6
2131	167	ROLE	PUBLISH	6
2132	167	ROLE	VIEW_LAYER	6
2133	167	ROLE	VIEW_PUBLISHED	7
2134	167	ROLE	DOWNLOAD	7
2135	167	ROLE	PUBLISH	7
2136	167	ROLE	VIEW_LAYER	7
2137	167	ROLE	VIEW_PUBLISHED	8
2138	167	ROLE	DOWNLOAD	8
2139	167	ROLE	PUBLISH	8
2140	167	ROLE	VIEW_LAYER	8
2141	167	ROLE	VIEW_PUBLISHED	9
2142	167	ROLE	DOWNLOAD	9
2143	167	ROLE	PUBLISH	9
2144	167	ROLE	VIEW_LAYER	9
2145	167	ROLE	VIEW_PUBLISHED	10
2146	167	ROLE	DOWNLOAD	10
2147	167	ROLE	PUBLISH	10
2148	167	ROLE	VIEW_LAYER	10
2149	167	ROLE	VIEW_PUBLISHED	11
2150	167	ROLE	DOWNLOAD	11
2151	167	ROLE	PUBLISH	11
2152	167	ROLE	VIEW_LAYER	11
2153	168	ROLE	VIEW_LAYER	3
2155	168	ROLE	PUBLISH	3
2156	168	ROLE	DOWNLOAD	3
2157	168	ROLE	VIEW_PUBLISHED	3
2158	168	ROLE	VIEW_PUBLISHED	8
2159	168	ROLE	DOWNLOAD	8
2160	168	ROLE	PUBLISH	8
2161	168	ROLE	VIEW_LAYER	8
2162	168	ROLE	VIEW_PUBLISHED	13
2163	168	ROLE	DOWNLOAD	13
2164	168	ROLE	PUBLISH	13
2165	168	ROLE	VIEW_LAYER	13
2166	169	ROLE	VIEW_LAYER	3
2168	169	ROLE	PUBLISH	3
2169	169	ROLE	DOWNLOAD	3
2170	169	ROLE	VIEW_PUBLISHED	3
2171	170	ROLE	VIEW_LAYER	3
2173	170	ROLE	PUBLISH	3
2174	170	ROLE	DOWNLOAD	3
2175	170	ROLE	VIEW_PUBLISHED	3
2176	170	ROLE	VIEW_PUBLISHED	4
2177	170	ROLE	DOWNLOAD	4
2178	170	ROLE	PUBLISH	4
2179	170	ROLE	VIEW_LAYER	4
2180	170	ROLE	VIEW_PUBLISHED	5
2181	170	ROLE	DOWNLOAD	5
2182	170	ROLE	PUBLISH	5
2183	170	ROLE	VIEW_LAYER	5
2184	170	ROLE	VIEW_PUBLISHED	6
2185	170	ROLE	DOWNLOAD	6
2186	170	ROLE	PUBLISH	6
2187	170	ROLE	VIEW_LAYER	6
2188	170	ROLE	VIEW_PUBLISHED	7
2189	170	ROLE	DOWNLOAD	7
2190	170	ROLE	PUBLISH	7
2191	170	ROLE	VIEW_LAYER	7
2192	170	ROLE	VIEW_PUBLISHED	8
2193	170	ROLE	DOWNLOAD	8
2194	170	ROLE	PUBLISH	8
2195	170	ROLE	VIEW_LAYER	8
2196	170	ROLE	VIEW_PUBLISHED	9
2197	170	ROLE	DOWNLOAD	9
2198	170	ROLE	PUBLISH	9
2199	170	ROLE	VIEW_LAYER	9
2200	170	ROLE	VIEW_PUBLISHED	10
2201	170	ROLE	DOWNLOAD	10
2202	170	ROLE	PUBLISH	10
2203	170	ROLE	VIEW_LAYER	10
2204	170	ROLE	VIEW_PUBLISHED	11
2205	170	ROLE	DOWNLOAD	11
2206	170	ROLE	PUBLISH	11
2207	170	ROLE	VIEW_LAYER	11
2208	170	ROLE	VIEW_PUBLISHED	12
2209	170	ROLE	DOWNLOAD	12
2210	170	ROLE	PUBLISH	12
2211	170	ROLE	VIEW_LAYER	12
2212	171	ROLE	VIEW_LAYER	3
2214	171	ROLE	PUBLISH	3
2215	171	ROLE	DOWNLOAD	3
2216	171	ROLE	VIEW_PUBLISHED	3
2217	171	ROLE	VIEW_PUBLISHED	5
2218	171	ROLE	DOWNLOAD	5
2219	171	ROLE	PUBLISH	5
2220	171	ROLE	VIEW_LAYER	5
2221	171	ROLE	VIEW_PUBLISHED	13
2222	171	ROLE	DOWNLOAD	13
2223	171	ROLE	PUBLISH	13
2224	171	ROLE	VIEW_LAYER	13
2225	172	ROLE	VIEW_LAYER	3
2227	172	ROLE	PUBLISH	3
2228	172	ROLE	DOWNLOAD	3
2229	172	ROLE	VIEW_PUBLISHED	3
2230	172	ROLE	VIEW_PUBLISHED	13
2231	172	ROLE	DOWNLOAD	13
2232	172	ROLE	PUBLISH	13
2233	172	ROLE	VIEW_LAYER	13
2236	174	ROLE	VIEW_LAYER	3
2238	174	ROLE	PUBLISH	3
2239	174	ROLE	DOWNLOAD	3
2240	174	ROLE	VIEW_PUBLISHED	3
2241	174	ROLE	VIEW_PUBLISHED	7
2242	174	ROLE	DOWNLOAD	7
2243	174	ROLE	PUBLISH	7
2244	174	ROLE	VIEW_LAYER	7
2245	174	ROLE	VIEW_PUBLISHED	13
2246	174	ROLE	DOWNLOAD	13
2247	174	ROLE	PUBLISH	13
2248	174	ROLE	VIEW_LAYER	13
2249	175	ROLE	VIEW_LAYER	3
2251	175	ROLE	PUBLISH	3
2252	175	ROLE	DOWNLOAD	3
2253	175	ROLE	VIEW_PUBLISHED	3
2254	176	ROLE	VIEW_LAYER	3
2256	176	ROLE	PUBLISH	3
2257	176	ROLE	DOWNLOAD	3
2258	176	ROLE	VIEW_PUBLISHED	3
2259	176	ROLE	VIEW_PUBLISHED	5
2260	176	ROLE	DOWNLOAD	5
2261	176	ROLE	PUBLISH	5
2262	176	ROLE	VIEW_LAYER	5
2263	176	ROLE	VIEW_PUBLISHED	13
2264	176	ROLE	DOWNLOAD	13
2265	176	ROLE	PUBLISH	13
2266	176	ROLE	VIEW_LAYER	13
2267	177	ROLE	VIEW_LAYER	3
2269	177	ROLE	PUBLISH	3
2270	177	ROLE	DOWNLOAD	3
2271	177	ROLE	VIEW_PUBLISHED	3
2272	177	ROLE	VIEW_PUBLISHED	7
2273	177	ROLE	DOWNLOAD	7
2274	177	ROLE	PUBLISH	7
2275	177	ROLE	VIEW_LAYER	7
2276	177	ROLE	VIEW_PUBLISHED	13
2277	177	ROLE	DOWNLOAD	13
2278	177	ROLE	PUBLISH	13
2279	177	ROLE	VIEW_LAYER	13
2280	178	ROLE	VIEW_LAYER	3
2282	178	ROLE	PUBLISH	3
2283	178	ROLE	DOWNLOAD	3
2284	178	ROLE	VIEW_PUBLISHED	3
2285	178	ROLE	VIEW_PUBLISHED	7
2286	178	ROLE	DOWNLOAD	7
2287	178	ROLE	PUBLISH	7
2288	178	ROLE	VIEW_LAYER	7
2289	178	ROLE	VIEW_PUBLISHED	13
2290	178	ROLE	DOWNLOAD	13
2291	178	ROLE	PUBLISH	13
2292	178	ROLE	VIEW_LAYER	13
2293	179	ROLE	VIEW_LAYER	3
2295	179	ROLE	PUBLISH	3
2296	179	ROLE	DOWNLOAD	3
2297	179	ROLE	VIEW_PUBLISHED	3
2298	179	ROLE	VIEW_PUBLISHED	7
2299	179	ROLE	DOWNLOAD	7
2300	179	ROLE	PUBLISH	7
2301	179	ROLE	VIEW_LAYER	7
2302	179	ROLE	VIEW_PUBLISHED	13
2303	179	ROLE	DOWNLOAD	13
2304	179	ROLE	PUBLISH	13
2305	179	ROLE	VIEW_LAYER	13
2306	180	ROLE	VIEW_LAYER	3
2308	180	ROLE	PUBLISH	3
2309	180	ROLE	DOWNLOAD	3
2310	180	ROLE	VIEW_PUBLISHED	3
2311	180	ROLE	VIEW_LAYER	1
2312	181	ROLE	VIEW_LAYER	2
2314	181	ROLE	VIEW_LAYER	3
2316	181	ROLE	PUBLISH	3
2317	181	ROLE	DOWNLOAD	3
2318	181	ROLE	VIEW_PUBLISHED	3
2319	182	ROLE	VIEW_LAYER	3
2321	182	ROLE	PUBLISH	3
2322	182	ROLE	DOWNLOAD	3
2323	182	ROLE	VIEW_PUBLISHED	3
2324	182	ROLE	VIEW_PUBLISHED	8
2325	182	ROLE	DOWNLOAD	8
2326	182	ROLE	PUBLISH	8
2327	182	ROLE	VIEW_LAYER	8
2328	182	ROLE	VIEW_PUBLISHED	13
2329	182	ROLE	DOWNLOAD	13
2330	182	ROLE	PUBLISH	13
2331	182	ROLE	VIEW_LAYER	13
2332	183	ROLE	VIEW_LAYER	3
2334	183	ROLE	PUBLISH	3
2335	183	ROLE	DOWNLOAD	3
2336	183	ROLE	VIEW_PUBLISHED	3
2337	183	ROLE	VIEW_LAYER	1
2338	184	ROLE	VIEW_LAYER	3
2340	184	ROLE	PUBLISH	3
2341	184	ROLE	DOWNLOAD	3
2342	184	ROLE	VIEW_PUBLISHED	3
2343	184	ROLE	VIEW_LAYER	1
2344	185	ROLE	VIEW_LAYER	3
2346	185	ROLE	PUBLISH	3
2347	185	ROLE	DOWNLOAD	3
2348	185	ROLE	VIEW_PUBLISHED	3
2349	185	ROLE	VIEW_LAYER	1
2350	186	ROLE	VIEW_LAYER	3
2352	186	ROLE	PUBLISH	3
2353	186	ROLE	DOWNLOAD	3
2354	186	ROLE	VIEW_PUBLISHED	3
2355	186	ROLE	VIEW_PUBLISHED	12
2356	186	ROLE	DOWNLOAD	12
2357	186	ROLE	PUBLISH	12
2358	186	ROLE	VIEW_LAYER	12
2359	187	ROLE	VIEW_LAYER	2
2361	187	ROLE	VIEW_LAYER	3
2363	187	ROLE	PUBLISH	3
2364	187	ROLE	DOWNLOAD	3
2365	187	ROLE	VIEW_PUBLISHED	3
2366	188	ROLE	VIEW_LAYER	3
2368	188	ROLE	PUBLISH	3
2369	188	ROLE	DOWNLOAD	3
2370	188	ROLE	VIEW_PUBLISHED	3
2371	188	ROLE	VIEW_PUBLISHED	12
2372	188	ROLE	DOWNLOAD	12
2373	188	ROLE	PUBLISH	12
2374	188	ROLE	VIEW_LAYER	12
2375	188	ROLE	VIEW_LAYER	1
2376	189	ROLE	VIEW_LAYER	3
2378	189	ROLE	PUBLISH	3
2379	189	ROLE	DOWNLOAD	3
2380	189	ROLE	VIEW_PUBLISHED	3
2381	189	ROLE	VIEW_PUBLISHED	12
2382	189	ROLE	DOWNLOAD	12
2383	189	ROLE	PUBLISH	12
2384	189	ROLE	VIEW_LAYER	12
2385	190	ROLE	VIEW_LAYER	3
2387	190	ROLE	PUBLISH	3
2388	190	ROLE	DOWNLOAD	3
2389	190	ROLE	VIEW_PUBLISHED	3
2390	190	ROLE	VIEW_PUBLISHED	12
2391	190	ROLE	DOWNLOAD	12
2392	190	ROLE	PUBLISH	12
2393	190	ROLE	VIEW_LAYER	12
2394	190	ROLE	VIEW_LAYER	1
2395	191	ROLE	VIEW_LAYER	3
2397	191	ROLE	PUBLISH	3
2398	191	ROLE	DOWNLOAD	3
2399	191	ROLE	VIEW_PUBLISHED	3
2400	191	ROLE	VIEW_PUBLISHED	7
2401	191	ROLE	DOWNLOAD	7
2402	191	ROLE	PUBLISH	7
2403	191	ROLE	VIEW_LAYER	7
2404	191	ROLE	VIEW_PUBLISHED	13
2405	191	ROLE	DOWNLOAD	13
2406	191	ROLE	PUBLISH	13
2407	191	ROLE	VIEW_LAYER	13
2408	192	ROLE	VIEW_LAYER	3
2410	192	ROLE	PUBLISH	3
2411	192	ROLE	DOWNLOAD	3
2412	192	ROLE	VIEW_PUBLISHED	3
2413	192	ROLE	VIEW_PUBLISHED	4
2414	192	ROLE	DOWNLOAD	4
2415	192	ROLE	PUBLISH	4
2416	192	ROLE	VIEW_LAYER	4
2417	192	ROLE	VIEW_PUBLISHED	5
2418	192	ROLE	DOWNLOAD	5
2419	192	ROLE	PUBLISH	5
2420	192	ROLE	VIEW_LAYER	5
2421	192	ROLE	VIEW_PUBLISHED	6
2422	192	ROLE	DOWNLOAD	6
2423	192	ROLE	PUBLISH	6
2424	192	ROLE	VIEW_LAYER	6
2425	192	ROLE	VIEW_PUBLISHED	7
2426	192	ROLE	DOWNLOAD	7
2427	192	ROLE	PUBLISH	7
2428	192	ROLE	VIEW_LAYER	7
2429	192	ROLE	VIEW_PUBLISHED	8
2430	192	ROLE	DOWNLOAD	8
2431	192	ROLE	PUBLISH	8
2432	192	ROLE	VIEW_LAYER	8
2433	192	ROLE	VIEW_PUBLISHED	9
2434	192	ROLE	DOWNLOAD	9
2435	192	ROLE	PUBLISH	9
2436	192	ROLE	VIEW_LAYER	9
2437	192	ROLE	VIEW_PUBLISHED	10
2438	192	ROLE	DOWNLOAD	10
2439	192	ROLE	PUBLISH	10
2440	192	ROLE	VIEW_LAYER	10
2441	192	ROLE	VIEW_PUBLISHED	11
2442	192	ROLE	DOWNLOAD	11
2443	192	ROLE	PUBLISH	11
2444	192	ROLE	VIEW_LAYER	11
2445	192	ROLE	VIEW_PUBLISHED	12
2446	192	ROLE	DOWNLOAD	12
2447	192	ROLE	PUBLISH	12
2448	192	ROLE	VIEW_LAYER	12
2449	193	ROLE	VIEW_PUBLISHED	8
2450	193	ROLE	DOWNLOAD	8
2451	193	ROLE	PUBLISH	8
2452	193	ROLE	VIEW_LAYER	8
2453	193	ROLE	VIEW_LAYER	3
2455	193	ROLE	PUBLISH	3
2456	193	ROLE	DOWNLOAD	3
2457	193	ROLE	VIEW_PUBLISHED	3
2458	193	ROLE	VIEW_PUBLISHED	13
2459	193	ROLE	DOWNLOAD	13
2460	193	ROLE	PUBLISH	13
2461	193	ROLE	VIEW_LAYER	13
2462	194	ROLE	VIEW_LAYER	3
2464	194	ROLE	PUBLISH	3
2465	194	ROLE	DOWNLOAD	3
2466	194	ROLE	VIEW_PUBLISHED	3
2467	195	ROLE	VIEW_LAYER	3
2469	195	ROLE	PUBLISH	3
2470	195	ROLE	DOWNLOAD	3
2471	195	ROLE	VIEW_PUBLISHED	3
2472	195	ROLE	VIEW_LAYER	1
2473	197	ROLE	VIEW_LAYER	3
2475	197	ROLE	PUBLISH	3
2476	197	ROLE	DOWNLOAD	3
2477	197	ROLE	VIEW_PUBLISHED	3
2478	197	ROLE	VIEW_PUBLISHED	4
2479	197	ROLE	DOWNLOAD	4
2480	197	ROLE	PUBLISH	4
2481	197	ROLE	VIEW_LAYER	4
2482	197	ROLE	VIEW_PUBLISHED	13
2483	197	ROLE	DOWNLOAD	13
2484	197	ROLE	PUBLISH	13
2485	197	ROLE	VIEW_LAYER	13
2486	198	ROLE	VIEW_LAYER	3
2488	198	ROLE	PUBLISH	3
2489	198	ROLE	DOWNLOAD	3
2490	198	ROLE	VIEW_PUBLISHED	3
2491	198	ROLE	VIEW_LAYER	1
2492	199	ROLE	VIEW_LAYER	3
2494	199	ROLE	PUBLISH	3
2495	199	ROLE	DOWNLOAD	3
2496	199	ROLE	VIEW_PUBLISHED	3
2497	199	ROLE	VIEW_PUBLISHED	12
2498	199	ROLE	DOWNLOAD	12
2499	199	ROLE	PUBLISH	12
2500	199	ROLE	VIEW_LAYER	12
2501	199	ROLE	VIEW_LAYER	1
2502	200	ROLE	VIEW_LAYER	3
2504	200	ROLE	PUBLISH	3
2505	200	ROLE	DOWNLOAD	3
2506	200	ROLE	VIEW_PUBLISHED	3
2507	200	ROLE	VIEW_LAYER	1
2508	200	ROLE	VIEW_PUBLISHED	12
2509	200	ROLE	DOWNLOAD	12
2510	200	ROLE	PUBLISH	12
2511	200	ROLE	VIEW_LAYER	12
2512	200	ROLE	VIEW_PUBLISHED	4
2513	200	ROLE	DOWNLOAD	4
2514	200	ROLE	PUBLISH	4
2515	200	ROLE	VIEW_LAYER	4
2516	200	ROLE	VIEW_PUBLISHED	5
2517	200	ROLE	DOWNLOAD	5
2518	200	ROLE	PUBLISH	5
2519	200	ROLE	VIEW_PUBLISHED	6
2520	200	ROLE	DOWNLOAD	6
2521	200	ROLE	PUBLISH	6
2522	200	ROLE	VIEW_LAYER	6
2523	200	ROLE	VIEW_PUBLISHED	7
2524	200	ROLE	DOWNLOAD	7
2525	200	ROLE	PUBLISH	7
2526	200	ROLE	VIEW_LAYER	7
2527	200	ROLE	VIEW_PUBLISHED	8
2528	200	ROLE	DOWNLOAD	8
2529	200	ROLE	PUBLISH	8
2530	200	ROLE	VIEW_LAYER	8
2531	200	ROLE	VIEW_PUBLISHED	9
2532	200	ROLE	DOWNLOAD	9
2533	200	ROLE	PUBLISH	9
2534	200	ROLE	VIEW_LAYER	9
2535	200	ROLE	VIEW_PUBLISHED	10
2536	200	ROLE	DOWNLOAD	10
2537	200	ROLE	PUBLISH	10
2538	200	ROLE	VIEW_LAYER	10
2539	200	ROLE	VIEW_PUBLISHED	11
2540	200	ROLE	DOWNLOAD	11
2541	200	ROLE	PUBLISH	11
2542	200	ROLE	VIEW_LAYER	11
2543	200	ROLE	VIEW_LAYER	5
2544	201	ROLE	VIEW_LAYER	3
2546	201	ROLE	PUBLISH	3
2547	201	ROLE	DOWNLOAD	3
2548	201	ROLE	VIEW_PUBLISHED	3
2549	201	ROLE	VIEW_LAYER	1
2550	202	ROLE	VIEW_LAYER	3
2552	202	ROLE	PUBLISH	3
2553	202	ROLE	DOWNLOAD	3
2554	202	ROLE	VIEW_PUBLISHED	3
2555	202	ROLE	VIEW_PUBLISHED	8
2556	202	ROLE	DOWNLOAD	8
2557	202	ROLE	PUBLISH	8
2558	202	ROLE	VIEW_LAYER	8
2559	202	ROLE	VIEW_PUBLISHED	13
2560	202	ROLE	DOWNLOAD	13
2561	202	ROLE	PUBLISH	13
2562	202	ROLE	VIEW_LAYER	13
2563	203	ROLE	VIEW_LAYER	3
2565	203	ROLE	PUBLISH	3
2566	203	ROLE	DOWNLOAD	3
2567	203	ROLE	VIEW_PUBLISHED	3
2568	203	ROLE	VIEW_LAYER	1
2569	204	ROLE	VIEW_LAYER	3
2571	204	ROLE	PUBLISH	3
2572	204	ROLE	DOWNLOAD	3
2573	204	ROLE	VIEW_PUBLISHED	3
2574	204	ROLE	VIEW_LAYER	1
2575	205	ROLE	VIEW_LAYER	3
2577	205	ROLE	PUBLISH	3
2578	205	ROLE	DOWNLOAD	3
2579	205	ROLE	VIEW_PUBLISHED	3
2580	205	ROLE	VIEW_PUBLISHED	13
2581	205	ROLE	DOWNLOAD	13
2582	205	ROLE	PUBLISH	13
2583	205	ROLE	VIEW_LAYER	13
2584	206	ROLE	VIEW_LAYER	3
2586	206	ROLE	PUBLISH	3
2587	206	ROLE	DOWNLOAD	3
2588	206	ROLE	VIEW_PUBLISHED	3
2589	206	ROLE	VIEW_PUBLISHED	4
2590	206	ROLE	DOWNLOAD	4
2591	206	ROLE	PUBLISH	4
2592	206	ROLE	VIEW_LAYER	4
2593	206	ROLE	VIEW_PUBLISHED	5
2594	206	ROLE	DOWNLOAD	5
2595	206	ROLE	PUBLISH	5
2596	206	ROLE	VIEW_LAYER	5
2597	206	ROLE	VIEW_PUBLISHED	6
2598	206	ROLE	DOWNLOAD	6
2599	206	ROLE	PUBLISH	6
2600	206	ROLE	VIEW_LAYER	6
2601	206	ROLE	VIEW_PUBLISHED	7
2602	206	ROLE	DOWNLOAD	7
2603	206	ROLE	PUBLISH	7
2604	206	ROLE	VIEW_LAYER	7
2605	206	ROLE	VIEW_PUBLISHED	8
2606	206	ROLE	DOWNLOAD	8
2607	206	ROLE	PUBLISH	8
2608	206	ROLE	VIEW_LAYER	8
2609	206	ROLE	VIEW_PUBLISHED	9
2610	206	ROLE	DOWNLOAD	9
2611	206	ROLE	PUBLISH	9
2612	206	ROLE	VIEW_LAYER	9
2613	206	ROLE	VIEW_PUBLISHED	10
2614	206	ROLE	DOWNLOAD	10
2615	206	ROLE	PUBLISH	10
2616	206	ROLE	VIEW_LAYER	10
2617	206	ROLE	VIEW_PUBLISHED	11
2618	206	ROLE	VIEW_PUBLISHED	12
2619	206	ROLE	DOWNLOAD	12
2620	206	ROLE	PUBLISH	12
2621	206	ROLE	VIEW_LAYER	12
2622	207	ROLE	VIEW_LAYER	3
2624	207	ROLE	PUBLISH	3
2625	207	ROLE	DOWNLOAD	3
2626	207	ROLE	VIEW_PUBLISHED	3
2627	207	ROLE	VIEW_PUBLISHED	8
2628	207	ROLE	DOWNLOAD	8
2629	207	ROLE	PUBLISH	8
2630	207	ROLE	VIEW_LAYER	8
2631	207	ROLE	VIEW_PUBLISHED	13
2632	207	ROLE	DOWNLOAD	13
2633	207	ROLE	PUBLISH	13
2634	207	ROLE	VIEW_LAYER	13
2635	208	ROLE	VIEW_LAYER	3
2637	208	ROLE	PUBLISH	3
2638	208	ROLE	DOWNLOAD	3
2639	208	ROLE	VIEW_PUBLISHED	3
2640	208	ROLE	VIEW_LAYER	1
2641	209	ROLE	VIEW_LAYER	3
2643	209	ROLE	PUBLISH	3
2644	209	ROLE	DOWNLOAD	3
2645	209	ROLE	VIEW_PUBLISHED	3
2646	209	ROLE	VIEW_PUBLISHED	5
2647	209	ROLE	DOWNLOAD	5
2648	209	ROLE	PUBLISH	5
2649	209	ROLE	VIEW_LAYER	5
2650	209	ROLE	VIEW_PUBLISHED	13
2651	209	ROLE	DOWNLOAD	13
2652	209	ROLE	PUBLISH	13
2653	209	ROLE	VIEW_LAYER	13
2654	210	ROLE	VIEW_LAYER	3
2656	210	ROLE	PUBLISH	3
2657	210	ROLE	DOWNLOAD	3
2658	210	ROLE	VIEW_PUBLISHED	3
2659	210	ROLE	VIEW_PUBLISHED	8
2660	210	ROLE	DOWNLOAD	8
2661	210	ROLE	PUBLISH	8
2662	210	ROLE	VIEW_LAYER	8
2663	210	ROLE	VIEW_PUBLISHED	13
2664	210	ROLE	DOWNLOAD	13
2665	210	ROLE	PUBLISH	13
2666	210	ROLE	VIEW_LAYER	13
2667	211	ROLE	VIEW_LAYER	3
2669	211	ROLE	PUBLISH	3
2670	211	ROLE	DOWNLOAD	3
2671	211	ROLE	VIEW_PUBLISHED	3
2672	211	ROLE	VIEW_PUBLISHED	5
2673	211	ROLE	DOWNLOAD	5
2674	211	ROLE	PUBLISH	5
2675	211	ROLE	VIEW_LAYER	5
2676	211	ROLE	VIEW_PUBLISHED	13
2677	211	ROLE	DOWNLOAD	13
2678	211	ROLE	PUBLISH	13
2679	211	ROLE	VIEW_LAYER	13
2680	212	ROLE	VIEW_LAYER	3
2682	212	ROLE	PUBLISH	3
2683	212	ROLE	DOWNLOAD	3
2684	212	ROLE	VIEW_PUBLISHED	3
2685	212	ROLE	VIEW_PUBLISHED	5
2686	212	ROLE	DOWNLOAD	5
2687	212	ROLE	PUBLISH	5
2688	212	ROLE	VIEW_LAYER	5
2689	212	ROLE	VIEW_PUBLISHED	13
2690	212	ROLE	DOWNLOAD	13
2691	212	ROLE	PUBLISH	13
2692	212	ROLE	VIEW_LAYER	13
2735	214	ROLE	VIEW_LAYER	3
2737	214	ROLE	PUBLISH	3
2738	214	ROLE	DOWNLOAD	3
2739	214	ROLE	VIEW_PUBLISHED	3
2740	214	ROLE	VIEW_LAYER	1
2741	215	ROLE	VIEW_LAYER	3
2743	215	ROLE	PUBLISH	3
2744	215	ROLE	DOWNLOAD	3
2745	215	ROLE	VIEW_PUBLISHED	3
2746	215	ROLE	VIEW_PUBLISHED	5
2747	215	ROLE	DOWNLOAD	5
2748	215	ROLE	PUBLISH	5
2749	215	ROLE	VIEW_LAYER	5
2750	215	ROLE	VIEW_PUBLISHED	13
2751	215	ROLE	DOWNLOAD	13
2752	215	ROLE	PUBLISH	13
2753	215	ROLE	VIEW_LAYER	13
2754	216	ROLE	VIEW_LAYER	3
2756	216	ROLE	PUBLISH	3
2757	216	ROLE	DOWNLOAD	3
2758	216	ROLE	VIEW_PUBLISHED	3
2759	217	ROLE	VIEW_LAYER	3
2761	217	ROLE	PUBLISH	3
2762	217	ROLE	DOWNLOAD	3
2763	217	ROLE	VIEW_PUBLISHED	3
2764	218	ROLE	VIEW_LAYER	3
2766	218	ROLE	PUBLISH	3
2767	218	ROLE	DOWNLOAD	3
2768	218	ROLE	VIEW_PUBLISHED	3
2769	219	ROLE	VIEW_LAYER	3
2771	219	ROLE	PUBLISH	3
2772	219	ROLE	DOWNLOAD	3
2773	219	ROLE	VIEW_PUBLISHED	3
2774	219	ROLE	VIEW_PUBLISHED	7
2775	219	ROLE	DOWNLOAD	7
2776	219	ROLE	PUBLISH	7
2777	219	ROLE	VIEW_LAYER	7
2778	219	ROLE	VIEW_PUBLISHED	13
2779	219	ROLE	DOWNLOAD	13
2780	219	ROLE	PUBLISH	13
2781	219	ROLE	VIEW_LAYER	13
2782	220	ROLE	VIEW_LAYER	3
2784	220	ROLE	PUBLISH	3
2785	220	ROLE	DOWNLOAD	3
2786	220	ROLE	VIEW_PUBLISHED	3
2787	220	ROLE	VIEW_PUBLISHED	4
2788	220	ROLE	DOWNLOAD	4
2789	220	ROLE	PUBLISH	4
2790	220	ROLE	VIEW_LAYER	4
2791	220	ROLE	VIEW_PUBLISHED	13
2792	220	ROLE	DOWNLOAD	13
2793	220	ROLE	PUBLISH	13
2794	220	ROLE	VIEW_LAYER	13
2795	221	ROLE	VIEW_LAYER	3
2797	221	ROLE	PUBLISH	3
2798	221	ROLE	DOWNLOAD	3
2799	221	ROLE	VIEW_PUBLISHED	3
2800	222	ROLE	VIEW_LAYER	3
2802	222	ROLE	PUBLISH	3
2803	222	ROLE	DOWNLOAD	3
2804	222	ROLE	VIEW_PUBLISHED	3
2805	223	ROLE	VIEW_LAYER	3
2807	223	ROLE	PUBLISH	3
2808	223	ROLE	DOWNLOAD	3
2809	223	ROLE	VIEW_PUBLISHED	3
2810	223	ROLE	DOWNLOAD	14
2811	223	ROLE	VIEW_LAYER	14
2812	224	ROLE	VIEW_LAYER	3
2814	224	ROLE	PUBLISH	3
2815	224	ROLE	DOWNLOAD	3
2816	224	ROLE	VIEW_PUBLISHED	3
2817	225	ROLE	VIEW_LAYER	3
2819	225	ROLE	PUBLISH	3
2820	225	ROLE	DOWNLOAD	3
2821	225	ROLE	VIEW_PUBLISHED	3
2822	226	ROLE	VIEW_LAYER	3
2824	226	ROLE	PUBLISH	3
2825	226	ROLE	DOWNLOAD	3
2826	226	ROLE	VIEW_PUBLISHED	3
2827	226	ROLE	VIEW_LAYER	1
2828	227	ROLE	VIEW_LAYER	3
2830	227	ROLE	PUBLISH	3
2831	227	ROLE	DOWNLOAD	3
2832	227	ROLE	VIEW_PUBLISHED	3
2833	228	ROLE	VIEW_LAYER	3
2835	228	ROLE	PUBLISH	3
2836	228	ROLE	DOWNLOAD	3
2837	228	ROLE	VIEW_PUBLISHED	3
2838	229	ROLE	VIEW_LAYER	3
2840	229	ROLE	PUBLISH	3
2841	229	ROLE	DOWNLOAD	3
2842	229	ROLE	VIEW_PUBLISHED	3
2843	229	ROLE	DOWNLOAD	14
2844	229	ROLE	VIEW_LAYER	14
2845	230	ROLE	VIEW_LAYER	3
2847	230	ROLE	PUBLISH	3
2848	230	ROLE	DOWNLOAD	3
2849	230	ROLE	VIEW_PUBLISHED	3
2850	230	ROLE	VIEW_PUBLISHED	4
2851	230	ROLE	DOWNLOAD	4
2852	230	ROLE	PUBLISH	4
2853	230	ROLE	VIEW_LAYER	4
2854	230	ROLE	VIEW_PUBLISHED	5
2855	230	ROLE	DOWNLOAD	5
2856	230	ROLE	PUBLISH	5
2857	230	ROLE	VIEW_LAYER	5
2858	230	ROLE	VIEW_PUBLISHED	6
2859	230	ROLE	DOWNLOAD	6
2860	230	ROLE	PUBLISH	6
2861	230	ROLE	VIEW_LAYER	6
2862	230	ROLE	VIEW_PUBLISHED	7
2863	230	ROLE	DOWNLOAD	7
2864	230	ROLE	PUBLISH	7
2865	230	ROLE	VIEW_LAYER	7
2866	230	ROLE	VIEW_PUBLISHED	8
2867	230	ROLE	DOWNLOAD	8
2868	230	ROLE	PUBLISH	8
2869	230	ROLE	VIEW_LAYER	8
2870	230	ROLE	VIEW_PUBLISHED	9
2871	230	ROLE	DOWNLOAD	9
2872	230	ROLE	PUBLISH	9
2873	230	ROLE	VIEW_LAYER	9
2874	230	ROLE	VIEW_PUBLISHED	10
2875	230	ROLE	DOWNLOAD	10
2876	230	ROLE	PUBLISH	10
2877	230	ROLE	VIEW_LAYER	10
2878	230	ROLE	VIEW_PUBLISHED	11
2879	230	ROLE	DOWNLOAD	11
2880	230	ROLE	PUBLISH	11
2881	230	ROLE	VIEW_LAYER	11
2882	230	ROLE	VIEW_PUBLISHED	12
2883	230	ROLE	DOWNLOAD	12
2884	230	ROLE	PUBLISH	12
2885	230	ROLE	VIEW_LAYER	12
2886	231	ROLE	VIEW_LAYER	3
2888	231	ROLE	PUBLISH	3
2889	231	ROLE	DOWNLOAD	3
2890	231	ROLE	VIEW_PUBLISHED	3
2891	231	ROLE	VIEW_PUBLISHED	7
2892	231	ROLE	DOWNLOAD	7
2893	231	ROLE	PUBLISH	7
2894	231	ROLE	VIEW_LAYER	7
2895	231	ROLE	VIEW_PUBLISHED	13
2896	231	ROLE	DOWNLOAD	13
2897	231	ROLE	PUBLISH	13
2898	231	ROLE	VIEW_LAYER	13
2899	232	ROLE	VIEW_LAYER	3
2901	232	ROLE	PUBLISH	3
2902	232	ROLE	DOWNLOAD	3
2903	232	ROLE	VIEW_PUBLISHED	3
2904	232	ROLE	VIEW_PUBLISHED	13
2905	232	ROLE	DOWNLOAD	13
2906	232	ROLE	PUBLISH	13
2907	232	ROLE	VIEW_LAYER	13
\.


--
-- Data for Name: oskari_resource; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_resource (id, resource_type, resource_mapping) FROM stdin;
233	maplayer	153
235	maplayer	155
237	maplayer	157
137	maplayer	1
138	maplayer	2
139	maplayer	4
141	maplayer	6
142	maplayer	19
143	maplayer	120
144	maplayer	150
145	maplayer	73
146	maplayer	123
147	maplayer	152
148	maplayer	124
149	maplayer	32
150	maplayer	33
151	maplayer	126
152	maplayer	69
153	maplayer	59
154	maplayer	125
155	maplayer	28
156	maplayer	116
157	maplayer	111
158	maplayer	36
159	maplayer	34
160	maplayer	43
161	maplayer	113
162	maplayer	122
163	maplayer	63
164	maplayer	149
165	maplayer	77
166	maplayer	11
167	maplayer	8
168	maplayer	70
169	maplayer	35
170	maplayer	56
171	maplayer	94
172	maplayer	87
173	maplayer	7
174	maplayer	78
175	maplayer	109
176	maplayer	91
177	maplayer	60
178	maplayer	75
179	maplayer	61
180	maplayer	118
181	maplayer	101
182	maplayer	72
183	maplayer	127
184	maplayer	135
185	maplayer	136
186	maplayer	100
187	maplayer	102
188	maplayer	98
189	maplayer	99
190	maplayer	97
191	maplayer	62
192	maplayer	58
193	maplayer	67
194	maplayer	147
195	maplayer	103
196	maplayer	105
197	maplayer	65
198	maplayer	104
199	maplayer	108
200	maplayer	10
201	maplayer	107
202	maplayer	64
203	maplayer	137
204	maplayer	151
205	maplayer	85
206	maplayer	57
207	maplayer	71
208	maplayer	134
209	maplayer	92
210	maplayer	68
211	maplayer	95
212	maplayer	96
214	maplayer	139
215	maplayer	93
216	maplayer	145
217	maplayer	112
218	maplayer	110
219	maplayer	76
220	maplayer	66
221	maplayer	140
222	maplayer	146
223	maplayer	114
224	maplayer	142
225	maplayer	144
226	maplayer	138
227	maplayer	143
228	maplayer	141
229	maplayer	148
230	maplayer	37
231	maplayer	74
232	maplayer	86
\.


--
-- Data for Name: oskari_role_external_mapping; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_role_external_mapping (roleid, name, external_type) FROM stdin;
\.


--
-- Data for Name: oskari_role_oskari_user; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_role_oskari_user (id, role_id, user_id) FROM stdin;
4	2	1
5	3	1
\.


--
-- Data for Name: oskari_roles; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_roles (id, name, is_guest) FROM stdin;
1	Guest	t
2	User	f
3	Admin	f
\.


--
-- Data for Name: oskari_statistical_datasource; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_statistical_datasource (id, locale, config, plugin) FROM stdin;
\.


--
-- Data for Name: oskari_statistical_layer; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_statistical_layer (datasource_id, layer_id, config) FROM stdin;
\.


--
-- Data for Name: oskari_status; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_status (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	0.1	<< Flyway Baseline >>	BASELINE	<< Flyway Baseline >>	\N	oskari	2019-02-19 12:19:23.333751	0	t
9	1.31.6	register admin and metrics bundles	JDBC	flyway.oskari.V1_31_6__register_admin_and_metrics_bundles	\N	oskari	2019-02-19 12:19:23.500378	5	t
10	1.31.7	add metadata for portti view	JDBC	flyway.oskari.V1_31_7__add_metadata_for_portti_view	\N	oskari	2019-02-19 12:19:23.516146	5	t
12	1.32.1	populate capabilities cache	JDBC	flyway.oskari.V1_32_1__populate_capabilities_cache	\N	oskari	2019-02-19 12:19:23.546176	1	t
13	1.32.2	drop wmts capabilties json	SQL	V1_32_2__drop_wmts_capabilties_json.sql	1859439171	oskari	2019-02-19 12:19:23.556107	2	t
15	1.32.4	populate WMTS layer resourceUrl	JDBC	flyway.oskari.V1_32_4__populate_WMTS_layer_resourceUrl	\N	oskari	2019-02-19 12:19:23.577384	1	t
17	1.33.0.1	register wfs search bundles	JDBC	flyway.oskari.V1_33_0_1__register_wfs_search_bundles	\N	oskari	2019-02-19 12:19:23.600528	2	t
19	1.33.1.1	populate preparsed layer capabilities	JDBC	flyway.oskari.V1_33_1_1__populate_preparsed_layer_capabilities	\N	oskari	2019-02-19 12:19:23.626591	0	t
22	1.33.4	register publisher2 bundle	JDBC	flyway.oskari.V1_33_4__register_publisher2_bundle	\N	oskari	2019-02-19 12:19:23.676442	1	t
23	1.33.5	replace deprecated bundles with new versions	JDBC	flyway.oskari.V1_33_5__replace_deprecated_bundles_with_new_versions	\N	oskari	2019-02-19 12:19:23.686461	1	t
24	1.33.6	fix and repopulate preparsed layer capabilities for layers	JDBC	flyway.oskari.V1_33_6__fix_and_repopulate_preparsed_layer_capabilities_for_layers	\N	oskari	2019-02-19 12:19:23.696535	0	t
25	1.34.0	register drawtools bundles	JDBC	flyway.oskari.V1_34_0__register_drawtools_bundles	\N	oskari	2019-02-19 12:19:23.705905	1	t
27	1.35.1	fix mapfull imagelocation conf	JDBC	flyway.oskari.V1_35_1__fix_mapfull_imagelocation_conf	\N	oskari	2019-02-19 12:19:23.726977	1	t
28	1.35.2	register timeseries bundle	JDBC	flyway.oskari.V1_35_2__register_timeseries_bundle	\N	oskari	2019-02-19 12:19:23.736196	2	t
31	1.35.5	update layer WMTS matrixes	JDBC	flyway.oskari.V1_35_5__update_layer_WMTS_matrixes	\N	oskari	2019-02-19 12:19:23.770056	1	t
38	1.36.6	register contenteditor bundle	JDBC	flyway.oskari.V1_36_6__register_contenteditor_bundle	\N	oskari	2019-02-19 12:19:23.883842	1	t
39	1.36.7	migrate published views toolbar	JDBC	flyway.oskari.V1_36_7__migrate_published_views_toolbar	\N	oskari	2019-02-19 12:19:23.893031	1	t
40	1.36.8	add toolbar to published view where missing	JDBC	flyway.oskari.V1_36_8__add_toolbar_to_published_view_where_missing	\N	oskari	2019-02-19 12:19:23.901969	1	t
43	1.37.0	add source column to portti backendstatus	SQL	V1_37_0__add_source_column_to_portti_backendstatus.sql	977279610	oskari	2019-02-19 12:19:23.931414	1	t
44	1.37.1	publisher2 migration	JDBC	flyway.oskari.V1_37_1__publisher2_migration	\N	oskari	2019-02-19 12:19:23.940606	65	t
47	1.39.1	migrate publish template ol3	JDBC	flyway.oskari.V1_39_1__migrate_publish_template_ol3	\N	oskari	2019-02-19 12:19:24.036071	53	t
48	1.39.2	migrate published maps to ol3	JDBC	flyway.oskari.V1_39_2__migrate_published_maps_to_ol3	\N	oskari	2019-02-19 12:19:24.097551	0	t
2	1.30.0	initial flyway script	SQL	V1_30_0__initial_flyway_script.sql	-848686863	oskari	2019-02-19 12:19:23.412387	2	t
4	1.31.1	add used and usagecount for portti view	SQL	V1_31_1__add_used_and_usagecount_for_portti_view.sql	306197364	oskari	2019-02-19 12:19:23.437764	10	t
5	1.31.2	add routingUI bundle to portti bundle table	SQL	V1_31_2__add_routingUI_bundle_to_portti_bundle_table.sql	438502017	oskari	2019-02-19 12:19:23.457026	2	t
6	1.31.3	add routingService bundle to portti bundle table	SQL	V1_31_3__add_routingService_bundle_to_portti_bundle_table.sql	766431387	oskari	2019-02-19 12:19:23.468183	1	t
7	1.31.5	add contraint permission	SQL	V1_31_5__add_contraint_permission.sql	232623068	oskari	2019-02-19 12:19:23.479032	2	t
8	1.31.5.1	fix sequence on portti bundle	SQL	V1_31_5_1__fix_sequence_on_portti_bundle.sql	-1760867977	oskari	2019-02-19 12:19:23.490069	2	t
11	1.32.0	new capabilities cache	SQL	V1_32_0__new_capabilities_cache.sql	-1573719337	oskari	2019-02-19 12:19:23.530568	7	t
14	1.32.3	drop portti capabilties cache table	SQL	V1_32_3__drop_portti_capabilties_cache_table.sql	81683488	oskari	2019-02-19 12:19:23.56615	2	t
16	1.33.0	create oskari wfs search channels table	SQL	V1_33_0__create_oskari_wfs_search_channels_table.sql	969651741	oskari	2019-02-19 12:19:23.587339	4	t
18	1.33.1	add parsed capabilities for oskari maplayer	SQL	V1_33_1__add_parsed_capabilities_for_oskari_maplayer.sql	-1758544730	oskari	2019-02-19 12:19:23.611669	6	t
20	1.33.2	create-rating-table	SQL	V1_33_2__create-rating-table.sql	1896623185	oskari	2019-02-19 12:19:23.636935	14	t
21	1.33.3	add capabillities service constraint	SQL	V1_33_3__add_capabillities_service_constraint.sql	239274023	oskari	2019-02-19 12:19:23.659852	8	t
26	1.35.0	alter oskari jaas users field types	SQL	V1_35_0__alter_oskari_jaas_users_field_types.sql	-1233557739	oskari	2019-02-19 12:19:23.715385	3	t
29	1.35.3	insert oskari wfs parser config default	SQL	V1_35_3__insert_oskari_wfs_parser_config_default.sql	1105349115	oskari	2019-02-19 12:19:23.74595	2	t
30	1.35.4	create oskari maplayer projections constraints	SQL	V1_35_4__create_oskari_maplayer_projections_constraints.sql	1773935927	oskari	2019-02-19 12:19:23.756584	5	t
32	1.36.0	add selectedfeaturedata bundle to portti bundle table	SQL	V1_36_0__add_selectedfeaturedata_bundle_to_portti_bundle_table.sql	-1948721967	oskari	2019-02-19 12:19:23.779094	2	t
33	1.36.1	indexes for oskari resource and permissions	SQL	V1_36_1__indexes_for_oskari_resource_and_permissions.sql	1946352588	oskari	2019-02-19 12:19:23.788811	12	t
34	1.36.2	create statsgrid tables	SQL	V1_36_2__create_statsgrid_tables.sql	331008360	oskari	2019-02-19 12:19:23.811379	29	t
35	1.36.3	add title column to oskari wfs parser config	SQL	V1_36_3__add_title_column_to_oskari_wfs_parser_config.sql	-1799455316	oskari	2019-02-19 12:19:23.849291	5	t
36	1.36.4	add version column to oskari capabilities cache	SQL	V1_36_4__add_version_column_to_oskari_capabilities_cache.sql	-1725958133	oskari	2019-02-19 12:19:23.86332	2	t
37	1.36.5	upd oskari capabilities cache unique service key	SQL	V1_36_5__upd_oskari_capabilities_cache_unique_service_key.sql	860768801	oskari	2019-02-19 12:19:23.872994	3	t
41	1.36.9	setup default version oskari maplayer	SQL	V1_36_9__setup_default_version_oskari_maplayer.sql	-644008777	oskari	2019-02-19 12:19:23.911366	2	t
42	1.36.10	remove capabilities with null version	SQL	V1_36_10__remove_capabilities_with_null_version.sql	-646989824	oskari	2019-02-19 12:19:23.921311	2	t
45	1.38.0	reset statsgrid default config	SQL	V1_38_0__reset_statsgrid_default_config.sql	-1923924225	oskari	2019-02-19 12:19:24.014138	1	t
46	1.39.0	add created field to ratings	SQL	V1_39_0__add_created_field_to_ratings.sql	235605803	oskari	2019-02-19 12:19:24.023698	4	t
51	1.40.1	migrate wfs channels	JDBC	flyway.oskari.V1_40_1__migrate_wfs_channels	\N	oskari	2019-02-19 12:19:24.134607	1	t
53	1.40.3	register feedback bundle	JDBC	flyway.oskari.V1_40_3__register_feedback_bundle	\N	oskari	2019-02-19 12:19:24.153931	1	t
59	1.41.4	statistics layer table config	JDBC	flyway.oskari.V1_41_4__statistics_layer_table_config	\N	oskari	2019-02-19 12:19:24.217536	6	t
60	1.41.5	system message bundle registration	JDBC	flyway.oskari.V1_41_5__system_message_bundle_registration	\N	oskari	2019-02-19 12:19:24.231038	1	t
61	1.41.6	remove empty packages	JDBC	flyway.oskari.V1_41_6__remove_empty_packages	\N	oskari	2019-02-19 12:19:24.240234	2	t
62	1.42.0	register maprotator bundle	JDBC	flyway.oskari.V1_42_0__register_maprotator_bundle	\N	oskari	2019-02-19 12:19:24.250058	1	t
64	1.42.2	update wfs search channels defaults config	JDBC	flyway.oskari.V1_42_2__update_wfs_search_channels_defaults_config	\N	oskari	2019-02-19 12:19:24.269962	1	t
65	1.42.3	remove selected params and locales	JDBC	flyway.oskari.V1_42_3__remove_selected_params_and_locales	\N	oskari	2019-02-19 12:19:24.278767	1	t
66	1.42.4	fix mapfull startup	JDBC	flyway.oskari.V1_42_4__fix_mapfull_startup	\N	oskari	2019-02-19 12:19:24.287747	2	t
70	1.43.3	register appsetup bundle	JDBC	flyway.oskari.V1_43_3__register_appsetup_bundle	\N	oskari	2019-02-19 12:19:24.329894	1	t
72	1.45.0	limit geoserver max request memory	JDBC	flyway.oskari.V1_45_0__limit_geoserver_max_request_memory	\N	oskari	2019-02-19 12:19:24.358005	0	t
73	1.45.1	migrate mapfull config	JDBC	flyway.oskari.V1_45_1__migrate_mapfull_config	\N	oskari	2019-02-19 12:19:24.3672	2	t
74	1.45.2	register admin publish transfer bundle	JDBC	flyway.oskari.V1_45_2__register_admin_publish_transfer_bundle	\N	oskari	2019-02-19 12:19:24.377827	1	t
76	1.45.4	migrate thematic maps	JDBC	flyway.oskari.V1_45_4__migrate_thematic_maps	\N	oskari	2019-02-19 12:19:24.396718	3	t
78	1.45.6	alter table maplayer rename groupid to dataproviderid	SQL	V1_45_6__alter_table_maplayer_rename_groupid_to_dataproviderid.sql	-30565000	oskari	2019-02-19 12:19:24.417955	1	t
81	1.45.9	alter table oskari maplayer group link rename themeid to groupid	SQL	V1_45_9__alter_table_oskari_maplayer_group_link_rename_themeid_to_groupid.sql	1108831455	oskari	2019-02-19 12:19:24.446061	1	t
87	1.45.13	migrate user indicator layers	JDBC	flyway.oskari.V1_45_13__migrate_user_indicator_layers	\N	oskari	2019-02-19 12:19:24.515153	1	t
88	1.45.14	migrate user indicator data	JDBC	flyway.oskari.V1_45_14__migrate_user_indicator_data	\N	oskari	2019-02-19 12:19:24.524376	20	t
90	1.45.16	migrate legacy filtered region ids	JDBC	flyway.oskari.V1_45_16__migrate_legacy_filtered_region_ids	\N	oskari	2019-02-19 12:19:24.564256	12	t
91	1.45.17	register download basket bundle	JDBC	flyway.oskari.V1_45_17__register_download_basket_bundle	\N	oskari	2019-02-19 12:19:24.585539	53	t
93	1.46.0	register hierarchical layerlist bundle	JDBC	flyway.oskari.V1_46_0__register_hierarchical_layerlist_bundle	\N	oskari	2019-02-19 12:19:24.658439	1	t
94	1.46.1	register admin hierarchical layerlist bundle	JDBC	flyway.oskari.V1_46_1__register_admin_hierarchical_layerlist_bundle	\N	oskari	2019-02-19 12:19:24.668098	2	t
49	1.39.3	create table user registration	SQL	V1_39_3__create_table_user_registration.sql	-1009361866	oskari	2019-02-19 12:19:24.10653	4	t
50	1.40.0	add locale config wfs channels	SQL	V1_40_0__add_locale_config_wfs_channels.sql	1071314894	oskari	2019-02-19 12:19:24.119483	6	t
52	1.40.2	drop deprecated columns wfs channels	SQL	V1_40_2__drop_deprecated_columns_wfs_channels.sql	-1170306627	oskari	2019-02-19 12:19:24.143831	1	t
54	1.40.4	update default statsgrid	SQL	V1_40_4__update_default_statsgrid.sql	-291190483	oskari	2019-02-19 12:19:24.165031	1	t
55	1.41.0	update mapfull template	SQL	V1_41_0__update_mapfull_template.sql	970394404	oskari	2019-02-19 12:19:24.174776	2	t
57	1.41.2	drop jaas roles table unsupported	SQL	V1_41_2__drop_jaas_roles_table_unsupported.sql	1656853636	oskari	2019-02-19 12:19:24.195464	2	t
58	1.41.3	alter role mapping constraints	SQL	V1_41_3__alter_role_mapping_constraints.sql	-1729041781	oskari	2019-02-19 12:19:24.20537	4	t
63	1.42.1	alter wfslayer custom style constraints	SQL	V1_42_1__alter_wfslayer_custom_style_constraints.sql	-435520849	oskari	2019-02-19 12:19:24.259206	3	t
67	1.43.0	alter oskari roles field types	SQL	V1_43_0__alter_oskari_roles_field_types.sql	-1255668427	oskari	2019-02-19 12:19:24.298172	3	t
68	1.43.1	alter portti view bundle seq field types	SQL	V1_43_1__alter_portti_view_bundle_seq_field_types.sql	-1813505342	oskari	2019-02-19 12:19:24.308735	3	t
69	1.43.2	alter portti view constraints	SQL	V1_43_2__alter_portti_view_constraints.sql	-126623884	oskari	2019-02-19 12:19:24.319672	2	t
71	1.44.0	update backendstatus schema	SQL	V1_44_0__update_backendstatus_schema.sql	186019441	oskari	2019-02-19 12:19:24.338706	10	t
75	1.45.3	update statsgrid template	SQL	V1_45_3__update_statsgrid_template.sql	518538820	oskari	2019-02-19 12:19:24.387034	1	t
77	1.45.5	rename layergroup table to dataprovider	SQL	V1_45_5__rename_layergroup_table_to_dataprovider.sql	-957272928	oskari	2019-02-19 12:19:24.40867	1	t
79	1.45.7	rename inspiretheme table to maplayer group	SQL	V1_45_7__rename_inspiretheme_table_to_maplayer_group.sql	371612295	oskari	2019-02-19 12:19:24.427332	2	t
80	1.45.8	rename oskari maplayer themes table to oskari map layer group link	SQL	V1_45_8__rename_oskari_maplayer_themes_table_to_oskari_map_layer_group_link.sql	1994321802	oskari	2019-02-19 12:19:24.436692	1	t
82	1.45.10	alter oskari maplayer schema	SQL	V1_45_10__alter_oskari_maplayer_schema.sql	2027073522	oskari	2019-02-19 12:19:24.455236	6	t
83	1.45.11.1	reset user indicator sequence	SQL	V1_45_11_1__reset_user_indicator_sequence.sql	2046464521	oskari	2019-02-19 12:19:24.470425	1	t
84	1.45.11.2	resolve duplicate ids for user indicators	SQL	V1_45_11_2__resolve_duplicate_ids_for_user_indicators.sql	-2001462430	oskari	2019-02-19 12:19:24.479957	1	t
85	1.45.11.3	add constraint user indicator	SQL	V1_45_11_3__add_constraint_user_indicator.sql	-1171946702	oskari	2019-02-19 12:19:24.489615	3	t
86	1.45.12	add user indicator data	SQL	V1_45_12__add_user_indicator_data.sql	397659154	oskari	2019-02-19 12:19:24.500943	6	t
89	1.45.15	remove deprecated user indicator data columns	SQL	V1_45_15__remove_deprecated_user_indicator_data_columns.sql	390355357	oskari	2019-02-19 12:19:24.553703	2	t
92	1.45.18	fix backendstatus constraint	SQL	V1_45_18__fix_backendstatus_constraint.sql	1448046469	oskari	2019-02-19 12:19:24.64706	3	t
96	1.46.3	add selectable column to oskari maplayer group	SQL	V1_46_3__add_selectable_column_to_oskari_maplayer_group.sql	1590912808	oskari	2019-02-19 12:19:24.691495	7	t
99	1.46.6	drop column tile matrix set id	SQL	V1_46_6__drop_column_tile_matrix_set_id.sql	-353705174	oskari	2019-02-19 12:19:24.727824	1	t
100	1.46.7	cleanup publish template	JDBC	flyway.oskari.V1_46_7__cleanup_publish_template	\N	oskari	2019-02-19 12:19:24.737353	0	t
102	1.46.9	replace externalids in mapful bglayerselection plugin config	JDBC	flyway.oskari.V1_46_9__replace_externalids_in_mapful_bglayerselection_plugin_config	\N	oskari	2019-02-19 12:19:24.761238	2	t
103	1.46.10	replace externalids in mapful layerselection plugin config	JDBC	flyway.oskari.V1_46_10__replace_externalids_in_mapful_layerselection_plugin_config	\N	oskari	2019-02-19 12:19:24.770844	1	t
104	1.47.0	remove portti stats layer	SQL	V1_47_0__remove_portti_stats_layer.sql	-721235568	oskari	2019-02-19 12:19:24.780433	2	t
105	1.47.1	register myplaces3 bundle	JDBC	flyway.oskari.V1_47_1__register_myplaces3_bundle	\N	oskari	2019-02-19 12:19:24.791082	1	t
113	1.48.1	remove statslayerplugin	JDBC	flyway.oskari.V1_48_1__remove_statslayerplugin	\N	oskari	2019-02-19 12:19:24.884519	1	t
115	1.50.0	drop startup default value	SQL	V1_50_0__drop_startup_default_value.sql	-23951171	oskari	2019-02-19 12:19:24.905969	1	t
118	1.52.0	rename portti terms of use for publishing	SQL	V1_52_0__rename_portti_terms_of_use_for_publishing.sql	-182523590	oskari	2019-09-24 11:26:55.890373	2	t
3	1.31.0	add coordinatetool bundle to portti bundle table	SQL	V1_31_0__add_coordinatetool_bundle_to_portti_bundle_table.sql	-1468535068	oskari	2019-02-19 12:19:23.424352	5	t
56	1.41.1	remove length restriction from user credentials	SQL	V1_41_1__remove_length_restriction_from_user_credentials.sql	1360197829	oskari	2019-02-19 12:19:24.185414	2	t
95	1.46.2	add parentid column to oskari maplayer group	SQL	V1_46_2__add_parentid_column_to_oskari_maplayer_group.sql	-1576190056	oskari	2019-02-19 12:19:24.678532	4	t
97	1.46.4	add order column to oskari maplayer group and oskari maplayer group link	SQL	V1_46_4__add_order_column_to_oskari_maplayer_group_and_oskari_maplayer_group_link.sql	-1218299453	oskari	2019-02-19 12:19:24.707389	1	t
98	1.46.5	add default order number	SQL	V1_46_5__add_default_order_number.sql	-913054058	oskari	2019-02-19 12:19:24.717314	3	t
101	1.46.8	move externalid to separate table	SQL	V1_46_8__move_externalid_to_separate_table.sql	-1182604806	oskari	2019-02-19 12:19:24.746519	6	t
106	1.47.2	convert bundle startups to ol4	SQL	V1_47_2__convert_bundle_startups_to_ol4.sql	1599107902	oskari	2019-02-19 12:19:24.800298	6	t
107	1.47.3	migrate appsetup to ol4	SQL	V1_47_3__migrate_appsetup_to_ol4.sql	-2134302644	oskari	2019-02-19 12:19:24.814066	7	t
108	1.47.4	drop ol2 bundles	SQL	V1_47_4__drop_ol2_bundles.sql	2100953161	oskari	2019-02-19 12:19:24.829132	4	t
109	1.47.5	add internal flag for maplayers	SQL	V1_47_5__add_internal_flag_for_maplayers.sql	894704981	oskari	2019-02-19 12:19:24.841153	7	t
110	1.47.6	set baselayers as internal	SQL	V1_47_6__set_baselayers_as_internal.sql	1387616453	oskari	2019-02-19 12:19:24.855852	1	t
111	1.47.7	drop externalid from oskari maplayer	SQL	V1_47_7__drop_externalid_from_oskari_maplayer.sql	1447142684	oskari	2019-02-19 12:19:24.865622	1	t
112	1.48.0	set statslayers as internal	SQL	V1_48_0__set_statslayers_as_internal.sql	-1075538553	oskari	2019-02-19 12:19:24.875253	1	t
114	1.49.0	remove startup	SQL	V1_49_0__remove_startup.sql	863838253	oskari	2019-02-19 12:19:24.894557	3	t
116	1.51.0	add table comments	SQL	V1_51_0__add_table_comments.sql	2062559553	oskari	2019-09-24 11:26:55.820646	23	t
117	1.51.1	drop unused embedded tracking tables	SQL	V1_51_1__drop_unused_embedded_tracking_tables.sql	947736793	oskari	2019-09-24 11:26:55.863762	10	t
119	1.53.0	add language selector bundle	SQL	V1_53_0__add_language_selector_bundle.sql	1453266118	oskari	2019-09-24 11:26:55.905621	2	t
120	1.54.0	layer resource mapping change	JDBC	flyway.oskari.V1_54_0__layer_resource_mapping_change	\N	oskari	2020-08-20 09:52:26.854546	300	t
121	1.54.1	migrate wfs system	JDBC	flyway.oskari.V1_54_1__migrate_wfs_system	\N	oskari	2020-08-20 09:52:26.871849	9	t
122	1.54.2	force migrate wfs system	JDBC	flyway.oskari.V1_54_2__force_migrate_wfs_system	\N	oskari	2020-08-20 09:52:26.880263	3	t
123	1.54.3	remove edit layer permissions	JDBC	flyway.oskari.V1_54_3__remove_edit_layer_permissions	\N	oskari	2020-08-20 09:52:26.885637	1	t
124	1.55.0	register 3d camera controls bundle	JDBC	flyway.oskari.V1_55_0__register_3d_camera_controls_bundle	\N	oskari	2020-08-20 09:52:26.891955	7	t
125	1.55.1	register shadow plugin bundle	JDBC	flyway.oskari.V1_55_1__register_shadow_plugin_bundle	\N	oskari	2020-08-20 09:52:26.905048	1	t
126	1.55.2	register dimension change bundle	JDBC	flyway.oskari.V1_55_2__register_dimension_change_bundle	\N	oskari	2020-08-20 09:52:26.91067	1	t
127	1.55.3	rename shadow plugin 3d to time control 3d	SQL	V1_55_3__rename_shadow_plugin_3d_to_time_control_3d.sql	-999587834	oskari	2020-08-20 09:52:26.916952	1	t
128	1.55.4	fix null styles	SQL	V1_55_4__fix_null_styles.sql	-603665225	oskari	2020-08-20 09:52:26.923215	1	t
129	1.55.5	migrate wfslayers	JDBC	flyway.oskari.V1_55_5__migrate_wfslayers	\N	oskari	2020-08-20 09:52:26.928895	7	t
130	1.55.6	drop wfs-tables	SQL	V1_55_6__drop_wfs-tables.sql	-828717522	oskari	2020-08-20 09:52:26.94255	11	t
131	1.55.7	register layereditor and layerlist bundles	JDBC	flyway.oskari.V1_55_7__register_layereditor_and_layerlist_bundles	\N	oskari	2020-08-20 09:52:26.961419	1	t
\.


--
-- Data for Name: oskari_status_myapp; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_status_myapp (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	1	0.1	<< Flyway Baseline >>	BASELINE	<< Flyway Baseline >>	\N	oskari	2019-02-19 12:19:25.348525	0	t
2	2	1.0.0	initial db content	JDBC	flyway.myapp.V1_0_0__initial_db_content	\N	oskari	2019-02-19 12:19:25.372904	476	t
\.


--
-- Data for Name: oskari_status_myplaces; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_status_myplaces (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	0.1	<< Flyway Baseline >>	BASELINE	<< Flyway Baseline >>	\N	oskari	2019-02-19 12:19:24.931052	0	t
6	1.0.4	add baselayer	JDBC	flyway.myplaces.V1_0_4__add_baselayer	\N	oskari	2019-02-19 12:19:25.015307	148	t
2	1.0	create-tables	SQL	V1_0__create-tables.sql	1210922681	oskari	2019-02-19 12:19:24.958325	11	t
3	1.0.1	create-triggers	SQL	V1_0_1__create-triggers.sql	-1613830364	oskari	2019-02-19 12:19:24.976363	4	t
4	1.0.2	create indexes	SQL	V1_0_2__create_indexes.sql	-918583594	oskari	2019-02-19 12:19:24.987391	4	t
5	1.0.3	create spatial index	SQL	V1_0_3__create_spatial_index.sql	-1934189726	oskari	2019-02-19 12:19:24.998023	10	t
7	1.0.5	add table comments	SQL	V1_0_5__add_table_comments.sql	1091383510	oskari	2019-09-24 11:26:55.959529	3	t
8	1.0.6	fix border dasharray	SQL	V1_0_6__fix_border_dasharray.sql	851679094	oskari	2019-09-24 11:26:55.972529	17	t
9	1.0.7	convert line styles from double to solid	SQL	V1_0_7__convert_line_styles_from_double_to_solid.sql	-1489865804	oskari	2019-09-24 11:26:55.998446	2	t
10	1.0.8	update property fields	JDBC	flyway.myplaces.V1_0_8__update_property_fields	\N	oskari	2020-08-20 09:52:27.070824	16	t
11	1.0.9	add options column	SQL	V1_0_9__add_options_column.sql	-949283792	oskari	2020-08-20 09:52:27.09505	1	t
12	1.0.10	migrate style to json	JDBC	flyway.myplaces.V1_0_10__migrate_style_to_json	\N	oskari	2020-08-20 09:52:27.101234	19	t
13	1.0.11	update baselayer locale	JDBC	flyway.myplaces.V1_0_11__update_baselayer_locale	\N	oskari	2020-08-20 09:52:27.124834	136	t
14	1.0.12	remove style columns	SQL	V1_0_12__remove_style_columns.sql	850928018	oskari	2020-08-20 09:52:27.266822	4	t
15	1.0.13	add formatting metadata	JDBC	flyway.myplaces.V1_0_13__add_formatting_metadata	\N	oskari	2020-08-20 09:52:27.275729	2	t
\.


--
-- Data for Name: oskari_status_peltodata; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_status_peltodata (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	0.1	<< Flyway Baseline >>	BASELINE	<< Flyway Baseline >>	\N	oskari	2019-09-24 11:26:56.101435	0	t
4	1.1.3	AddPeltodataBundle	JDBC	flyway.peltodata.V1_1_3__AddPeltodataBundle	\N	oskari	2019-09-24 11:26:56.170235	6	t
2	1.1.1	create-tables	SQL	V1_1_1__create-tables.sql	-787015412	oskari	2019-09-24 11:26:56.141639	11	t
5	1.1.5	rename servlet app	SQL	V1_1_5__rename_servlet_app.sql	1816729753	oskari	2019-09-24 11:26:56.184636	2	t
6	1.1.6	alter-peltodatafields-add-columns	SQL	V1_1_6__alter-peltodatafields-add-columns.sql	-1501957250	oskari	2019-09-24 11:26:56.195356	5	t
7	1.1.7	add-peltodata-field-executions	SQL	V1_1_7__add-peltodata-field-executions.sql	910679412	oskari	2019-09-24 11:26:56.208551	4	t
8	1.1.8	alter-peltodata-field-add-column	SQL	V1_1_8__alter-peltodata-field-add-column.sql	1548409435	oskari	2019-09-24 11:26:56.22099	4	t
9	1.1.9	create-farmfield-files	SQL	V1_1_9__create-farmfield-files.sql	494169686	oskari	2019-09-24 11:26:56.232429	6	t
10	1.1.10	alter-peltodata-field-executions-add-columns	SQL	V1_1_10__alter-peltodata-field-executions-add-columns.sql	-831523527	oskari	2019-09-24 11:26:56.246464	4	t
11	1.1.11	alter-farmfield-add-maplayergroupid	SQL	V1_1_11__alter-farmfield-add-maplayergroupid.sql	-2037422750	oskari	2019-09-24 11:26:56.258423	1	t
\.


--
-- Data for Name: oskari_status_userlayer; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_status_userlayer (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	0.1	<< Flyway Baseline >>	BASELINE	<< Flyway Baseline >>	\N	oskari	2019-02-19 12:19:25.187342	0	t
4	1.0.2	Insert id for userlayerdata	JDBC	flyway.userlayer.V1_0_2__Insert_id_for_userlayerdata	\N	oskari	2019-02-19 12:19:25.250574	2	t
7	1.0.6	migrate fields to jsonarray	JDBC	flyway.userlayer.V1_0_6__migrate_fields_to_jsonarray	\N	oskari	2019-02-19 12:19:25.278145	1	t
9	1.0.8	add name and type to fields	JDBC	flyway.userlayer.V1_0_8__add_name_and_type_to_fields	\N	oskari	2019-02-19 12:19:25.293051	0	t
10	1.0.9	add baselayer	JDBC	flyway.userlayer.V1_0_9__add_baselayer	\N	oskari	2019-02-19 12:19:25.299896	11	t
12	1.0.11	populate userlayer wkt	JDBC	flyway.userlayer.V1_0_11__populate_userlayer_wkt	\N	oskari	2019-02-19 12:19:25.32882	1	t
2	1.0	create-tables	SQL	V1_0__create-tables.sql	1056101548	oskari	2019-02-19 12:19:25.215073	17	t
3	1.0.1	create triggers	SQL	V1_0_1__create_triggers.sql	1215533696	oskari	2019-02-19 12:19:25.239798	3	t
5	1.0.3	create indexes	SQL	V1_0_3__create_indexes.sql	-410531695	oskari	2019-02-19 12:19:25.258741	4	t
6	1.0.4	create spatial index	SQL	V1_0_4__create_spatial_index.sql	-608279846	oskari	2019-02-19 12:19:25.269918	1	t
8	1.0.7	update corrupted fields	SQL	V1_0_7__update_corrupted_fields.sql	-1491497472	oskari	2019-02-19 12:19:25.285752	1	t
11	1.0.10	alter table user layer add column wkt	SQL	V1_0_10__alter_table_user_layer_add_column_wkt.sql	-57301180	oskari	2019-02-19 12:19:25.317279	5	t
13	1.0.12	add table comments	SQL	V1_0_12__add_table_comments.sql	111067779	oskari	2019-09-24 11:26:56.047635	3	t
14	1.0.13	fix dasharrays	SQL	V1_0_13__fix_dasharrays.sql	1515274580	oskari	2019-09-24 11:26:56.060433	3	t
15	1.0.14	convert line styles from double to solid	SQL	V1_0_14__convert_line_styles_from_double_to_solid.sql	-1094726626	oskari	2019-09-24 11:26:56.071781	1	t
16	1.0.15	add options column	SQL	V1_0_15__add_options_column.sql	-1473216223	oskari	2020-08-20 09:52:27.381247	0	t
17	1.0.16	migrate style to json	JDBC	flyway.userlayer.V1_0_16__migrate_style_to_json	\N	oskari	2020-08-20 09:52:27.388213	4	t
18	1.0.17	remove style table	SQL	V1_0_17__remove_style_table.sql	1613285946	oskari	2020-08-20 09:52:27.397531	5	t
\.


--
-- Data for Name: oskari_terms_of_use_for_publishing; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_terms_of_use_for_publishing (userid, agreed, "time") FROM stdin;
8	t	2019-10-24 18:02:15.671+03
16	t	2019-11-26 10:58:10.05+02
\.


--
-- Data for Name: oskari_user_indicator; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_user_indicator (id, user_id, title, source, description, published) FROM stdin;
\.


--
-- Data for Name: oskari_user_indicator_data; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_user_indicator_data (id, indicator_id, regionset_id, year, data) FROM stdin;
\.


--
-- Data for Name: oskari_users; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_users (id, user_name, first_name, last_name, email, uuid, attributes) FROM stdin;
1	admin	Oskari	Admin	seints.pori@tut.fi	asdf-asdf-asdf-asdf-asdf	{}
\.


--
-- Data for Name: oskari_users_pending; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_users_pending (id, user_name, email, uuid, expiry_timestamp) FROM stdin;
1	uusi	uusi@example.com	uid	2050-01-01 00:00:00.789+03
\.


--
-- Data for Name: oskari_wfs_search_channels; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.oskari_wfs_search_channels (id, wfs_layer_id, params_for_search, is_default, locale, config) FROM stdin;
\.


--
-- Data for Name: peltodata_field; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.peltodata_field (id, description, user_id, crop_type, sowing_date, farm_id, maplayergroup_id) FROM stdin;
\.


--
-- Data for Name: peltodata_field_execution; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.peltodata_field_execution (id, state, execution_started_at, field_id, output_type, output_filename, field_file_id) FROM stdin;
\.


--
-- Data for Name: peltodata_field_file; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.peltodata_field_file (id, original_file_name, full_path, file_date, type, field_id) FROM stdin;
\.


--
-- Data for Name: peltodata_field_layer; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.peltodata_field_layer (field_id, layer_id) FROM stdin;
\.


--
-- Data for Name: peltodata_peltolohkot; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.peltodata_peltolohkot (gid, ely, knro, tiltu, lohko, pinta_ala, ymparys, geom) FROM stdin;
1	03	609	1	1	317	1043.63000000000011	0106000020FB0B00000100000001030000000100000009000000039A081BB1EC0A41A245B65B75115A41A345B6B363F30A41CDCCCC6C98115A4117FBCB2E49F80A41DF4F8DBFB1115A415BF5B9DA7AF80A419CC420C0B2115A41738A8E64D6F90A4139B4C84EA5115A41BB6B0979C9F70A411D5A646B9A115A41C542ADE9ACF20A41022B87AE7F115A411A04564EF9ED0A4191ED7C0F67115A41039A081BB1EC0A41A245B65B75115A41
2	03	609	1	2	900	1399.02999999999997	0106000020FB0B0000010000000103000000010000000E00000027C286A7BFB80A41CBA1452E31115A411995D409CEBE0A418FC2F59063115A41A401BC05F9C40A4185EB51D896115A41351136BC4BC70A41894160156D115A414CEA04F43EC70A4183C0CAA96C115A4140575BF128C50A4117D9CE975A115A418E28ED4DB4C00A41333333C333115A418E976E1294BD0A41DBF97E8218115A4112363C7D91BD0A41D9CEF76B18115A41875AD37C8BBD0A4183C0CA3918115A410E4FAF1416BB0A410681951B24115A41F953E365F4B90A414260E56029115A41C00E9CF39CB80A41759318CC2F115A4127C286A7BFB80A41CBA1452E31115A41
222348	03	609	609010557	6090004550	207	590.539999999999964	0106000020FB0B0000010000000103000000010000003C00000021D26F9F077C0B41C1CAA165450F5A4183E2C7D80B7C0B416F128350460F5A416619E218267C0B41CFF7532B470F5A41BA8D06B0347C0B4191ED7C97470F5A41117A362B467C0B41105839F4470F5A41290F0B35947C0B411B2FDDB4480F5A4186EB5178A07C0B41819543DB480F5A4158EC2F3BB77C0B41E17A1406490F5A41A03C2C14D37C0B4196438B5C490F5A41DD68002FDB7C0B416666669E490F5A4145FAEDEBE77C0B41F6285CE7490F5A41AAA44EC0F17C0B41759318344A0F5A41F3D24D62FA7C0B41CDCCCCA44A0F5A4149E17A14FE7C0B4104560EDD4A0F5A41D222DBB9067D0B41560E2D424B0F5A412D6519620D7D0B413108AC844B0F5A41A3B437B81A7D0B4104560EFD4B0F5A4194A982512D7D0B411283C0424C0F5A412CF697DD3E7D0B41FA7E6AAC4C0F5A412D6519625A7D0B41A245B6EB4C0F5A411A0456CE827D0B41B6F3FD1C4D0F5A416029CBD09B7D0B418D976E0A4D0F5A417B36AB7EAD7D0B41C1CAA1FD4C0F5A414360E590C67D0B41986E12EB4C0F5A415F4BC807E67D0B411D5A64034D0F5A41DCF97EAA097E0B41D9CEF7834D0F5A41557424971C7E0B41F4FDD4E04D0F5A41D144D830357E0B417368912D4E0F5A41C1CAA185557E0B416891ED8C4E0F5A41E6D0221B837E0B41BA490CE24E0F5A41D678E9668D7E0B413F355EDA4E0F5A41A8E848AE9E7E0B413F355EAA4E0F5A410D93A9C2B07E0B417368913D4E0F5A41A2F831A6C77E0B41FED478F14D0F5A4119265385DA7E0B41A245B6CB4D0F5A411EC9E5FFF87E0B413BDF4F854D0F5A41AF47E13A157F0B416F1283884D0F5A41BE5296A12E7F0B41355EBA994D0F5A41F3B050EB517F0B41273108044E0F5A41EA263148757F0B415EBA496C4E0F5A4144AD695E8C7F0B418716D9AE4E0F5A419DC42070987F0B411283C0D24E0F5A4149BF7DDDB57F0B413F355E2A4F0F5A41C84B37C9D67F0B41BA490C424F0F5A41806ABC7402800B4191ED7CFF4E0F5A414950FC1805820B41355EBAB9410F5A41645DDC8637830B41F853E3CD3A0F5A41BD74931807830B416F1283F0380F5A41F7285C4F5F810B410AD7A3A0280F5A413580B7C022810B41F2D24D1A260F5A41EE0DBE70D5800B41E17A141E270F5A41C00E9C3368800B41D122DBC1280F5A41AF47E17AF17E0B4160E5D072300F5A41F0384547867E0B4154E3A5BB320F5A415D8FC2F5097D0B412DB29DBF3C0F5A4171CE8812167C0B41D7A370CD420F5A41C84B3749147C0B41D7A3702D430F5A4183E2C7D80B7C0B41931804CE430F5A4187C95481067C0B41AC1C5A84440F5A4121D26F9F077C0B41C1CAA165450F5A41
222349	03	609	609010557	6090004651	196	620.690000000000055	0106000020FB0B0000010000000103000000010000001E0000002A5C8F02DF7A0B41C3F528E40A0F5A41B637F8C2E77A0B41A8C64B1F0C0F5A413208AC1CF87A0B41B07268390D0F5A41845149DD037B0B41A8C64BAF0D0F5A411383C00AD87C0B41E17A1406200F5A41B5C8767E917E0B41D9CEF713310F5A4104098ADF947E0B415EBA4954310F5A4147B6F3BDB57F0B414A0C02632B0F5A4167F7E4619B800B4177BE9FC2260F5A4126068195DA800B41068195FB250F5A41EB95B2CC11810B415EBA495C250F5A41343333F309810B4177BE9FD2240F5A412BA91350F97F0B41EE7C3F6D1A0F5A41381AC05B707F0B4125068135150F5A41FFD47829DC7E0B415C8FC2850F0F5A417C832F0C007E0B410AD7A328070F5A4104098A9FB07D0B41CBA14526040F5A4147B6F3BDA87C0B41643BDFA7F90E5A419D33A2B4677C0B417D3F354EF80E5A41AAA44E003F7C0B415EBA499CF80E5A414A9D80661F7C0B415839B400FA0E5A417524973FBE7B0B41C520B0CAFF0E5A41A6BDC157947B0B411904569E040F5A4140355EFA8F7B0B41B0726811050F5A41C8BAB88D407B0B41D34D6248050F5A41C84B3749177B0B41250681ED050F5A41CAE53FA4F37A0B410AD7A300070F5A4181B740C2E27A0B41F0A7C63B080F5A419B779C62DE7A0B41000000B8090F5A412A5C8F02DF7A0B41C3F528E40A0F5A41
222350	03	609	609010557	6090006368	116	415.310000000000002	0106000020FB0B000001000000010300000001000000190000004725750225700B417F6ABC1CD20E5A417BC729FAE4700B41AE47E1DADB0E5A41E4A59BC475710B41CDCCCC4CE30E5A41782D211F9D710B4127310804E50E5A412085EB91C2710B411283C052E50E5A4134333373FA710B413108AC64E50E5A4190C2F5284E720B412DB29DFFE40E5A41A4923A41D0720B4100000028E40E5A41433EE899B8730B41F2D24D02E20E5A41E12D9060F6730B415EBA494CE10E5A415FBA490C25740B41295C8FB2E00E5A41F41FD2AF33740B4123DBF9D6DF0E5A4189F4DB973F740B419CC420C8DE0E5A41C317269352740B414C378999DD0E5A41C7DCB5C468740B41713D0A77DC0E5A414794F68660740B410AD7A360DA0E5A41032B87D691740B41AC1C5AFCD80E5A4134333333C8740B413BDF4F85D80E5A41BEE3141D13740B418941609DCF0E5A4145696F7063730B413D0AD7FBC60E5A41306EA3417E720B41A4703D72C80E5A414A9D8026F3710B418FC2F5D8C90E5A41FE87F49B16710B4185EB51E8CC0E5A41312AA9532C700B414E6210A0D00E5A414725750225700B417F6ABC1CD20E5A41
222351	03	609	609010557	6090006469	243	747.299999999999955	0106000020FB0B0000010000000103000000010000001B00000043CF6695CE680B41333333C3F40E5A41B17268D188690B410C022B6FF90E5A416C9A771C1D6A0B41000000F8FC0E5A419587855A716A0B4108AC1C72FF0E5A418816D98EB56A0B41E3A59BE4010F5A41842F4C66CB6A0B4139B4C8FE020F5A41238E7531E86A0B41E7FBA9B1020F5A41E20B93A9AD6D0B415EBA4944FB0E5A416991EDBC6E6D0B419EEFA7CEF80E5A4117FBCB6E526D0B41BE9F1A67F60E5A41B9AF0327676D0B414A0C028BF30E5A418816D90E9C6D0B416ABC74D3F00E5A4177711BCDED6D0B414A0C02F3ED0E5A413208ACDC586E0B4146B6F39DEB0E5A41D52B6599D06E0B41E5D022F3E90E5A41A62C431C376F0B418FC2F5B8E80E5A413B92CBFF97700B41DBF97E9AE30E5A416788639D20710B41448B6C57E10E5A4130DD24C69F700B41448B6C8FDA0E5A41CCA145F61C700B419A9999F1D30E5A41C7DCB584E86F0B4154E3A573D10E5A41BD749318C76F0B41448B6CEFD10E5A4102DE0289A66E0B417B14AEFFD50E5A419CE61D270E6C0B41A69BC4A0DF0E5A410C2428FE73690B411B2FDDECE90E5A412085EB5104680B4160E5D092EF0E5A4143CF6695CE680B41333333C3F40E5A41
222352	03	609	609010557	6090006671	386	1518.29999999999995	0106000020FB0B00000100000001030000000400000079000000C00E9CF31EA80B41CBA1455E60105A4178BE9F5A4DA80B41378941A062105A41A4923A8157A80B41E926311864105A416EC5FE328CA80B414E62104066105A41E04F8D57D0A80B41F6285CF766105A4120F46C5628A90B41508D976667105A41FF43FAED3CA90B415839B4B067105A41B91E85EBB2A90B41D122DB8968105A4131BB278FF4A90B41B81E855368105A41E5839E8DB4AA0B41759318DC66105A4162C3D3AB14AB0B415A643BC765105A410D022B473AAB0B41D122DBD965105A4176931844DEAB0B419EEFA75664105A41723D0A1720AC0B419CC4200064105A413CDF4F4D86AC0B4104560EED62105A41441CEBE2C5AC0B418716D9EE61105A41CF19511A4DAD0B410E2DB2A55E105A41DD4603B8A5AD0B41CDCCCC9C5C105A41D066D5A74FAE0B417B14AEBF58105A41A345B6338AAE0B4123DBF9B657105A4168D5E72AF0AE0B410681955B55105A419587851A13AF0B418FC2F56054105A41F1A7C60B17AF0B41621058D953105A4133772DE12FAF0B419CC4205853105A41EA2631C842AF0B41A4703D5A53105A41DA3D79586BAF0B410E2DB24D52105A410712147F6AAF0B413D0AD7A351105A41FB7E6A7C85AF0B418716D9E650105A411926534597AF0B41BE9F1A4F51105A41B637F8C2F8AF0B4183C0CA214F105A41A857CA327CB00B41E17A14064C105A413DBD5256E8B00B414A0C02B349105A41297E8CF92EB10B41F4FDD40848105A41FB7E6A7C8AB10B417F6ABC1446105A41B09465C8D9B10B415A643BAF44105A414282E2076FB20B41C3F5284C41105A414872F94FF3B20B41B07268313E105A41623255B054B30B4154E3A59B3B105A4140355EFAA2B30B41333333333A105A41CFAACF9598B30B417F6ABC4C39105A416007CE99ACB30B41E7FBA9E938105A41F9C2646ABBB30B417F6ABC8439105A41526B9A3717B40B4160E5D07A37105A414B0C02EB63B40B41D7A370F535105A413CDF4FCDC0B40B4152B81E7533105A4117D9CE370CB50B41C976BE9F31105A41396744296BB50B41BE9F1AAF2F105A41DB1B7CE1EDB50B41666666362C105A415D8FC2354EB60B410AD7A3E829105A4152DA1BFC97B60B41E3A59B2428105A411DEBE276AEB60B41F853E32D28105A4158EC2F7BE2B60B4148E17AB426105A418F75719BFDB60B417B14AE9725105A41290F0B351AB70B41CBA145F624105A41CD7F483F79B70B415839B44023105A41AD1C5A64E0B70B410C022BD720105A41A189B0E137B80B41B29DEFFF1E105A414113618399B80B4146B6F3AD1C105A416619E218DDB80B41B07268011B105A41ABF1D28DFBB80B418B6CE7BB19105A4155E3A59BCEB80B41A245B6B317105A41244A7B838AB80B413D0AD7AB19105A410D93A9C270B80B418FC2F5E019105A4137AB3ED738B80B411058395C1A105A41B91E85ABF9B70B41250681551A105A41A5DFBECEC1B70B418D976EF219105A41FC3A70CE96B70B410681959B19105A41FA0FE9F746B70B41C976BEB71A105A411B51DA5BDAB60B4152B81E851D105A41B7847CD0A1B60B411F85EB691F105A4149E17A147CB60B412DB29D1F21105A411A0456CE50B60B413F355E0222105A41D609686211B60B412FDD248622105A412AED0DFEE8B50B41DF4F8D5722105A41B103E7CC75B50B417B14AE1721105A415A8638562DB50B415839B47821105A41943A01CDEDB40B4146B6F33522105A41E8FBA9B149B40B41EC51B82E25105A418148BFFDE4B30B411F85EB4128105A41A6BDC157BDB30B4152B81E7D29105A413CDF4F4D50B30B418D976E0A2C105A4144AD695E06B30B4108AC1CD22D105A4183E2C7189BB20B41AAF1D26530105A410C2428FEFBB10B418D976EE233105A4160984CD5E9B00B417B14AE7739105A41D3DEE00BCEAF0B419CC420A83E105A418DB96B4939AF0B41CDCCCC7441105A410AF9A0A71DAF0B41D122DBA141105A41B7F3FD94C7AE0B411B2FDD7C42105A41EE0DBE7077AE0B4177BE9F7A43105A41F5FDD4B861AE0B41AC1C5AE442105A414C59867839AE0B4191ED7CF740105A417BC729BA22AE0B41B07268A940105A4124DBF93EF0AD0B41273108F43F105A412C189514A9AD0B41E7FBA9A13F105A414282E2477BAD0B41DBF97E5A3E105A4121D26FDF4DAD0B41621058213C105A413580B78025AD0B416891ED8C3A105A41A70A4625E0AC0B413D0AD7BB3B105A4155742497B0AC0B41A8C64B673C105A41EF7C3FF561AC0B419EEFA73E3E105A41ACCFD5D609AC0B411904565640105A4139F8C224B5AB0B41295C8FB242105A412B3A928B88AB0B4114AE47F944105A41DEB584BC5EAB0B415C8FC20548105A41EC73B5953BAB0B41A8C64B6749105A41EB04349125AB0B41CBA145264A105A41AB825149FCAA0B41FA7E6AFC4A105A412ACB1087B7AA0B41295C8F924C105A4139F8C22481AA0B4146B6F37D4E105A414B0C02EB60AA0B41AAF1D2FD4F105A412FFF217D36AA0B41E92631E051105A41FC3A70CEECA90B41B6F3FDC453105A410BD7A3306CA90B41F4FDD4C856105A41920F7AB648A90B415C8FC29557105A41A345B63315A90B414A0C029B58105A41AF47E13AB8A80B417593187C5A105A415FBA494C83A80B41CDCCCCBC5A105A411EC9E53F41A80B41986E12D35B105A411DEBE27607A80B41C3F528245E105A41C00E9CF31EA80B41CBA1455E60105A410900000064EE5A027EA80B413F355E4A5E105A41E20B93E99EA80B413108ACFC5E105A41DF02094AAFA80B418D976E1260105A41CAE53FA4B6A80B41F4FDD4C860105A415BD3BCE3B1A80B412506817D61105A41905374249EA80B412DB29DC761105A415D2041318EA80B410E2DB24561105A414282E2076CA80B412B8716895F105A4164EE5A027EA80B413F355E4A5E105A410E000000F5DBD7411EA90B414C3789615B105A41E8FBA9F146A90B4117D9CE975B105A412F90A0784FA90B41D122DB015A105A41EFEBC03968A90B41EC51B86E59105A41752497FF8EA90B41C520B04A59105A4140575B319DA90B41B81E85EB59105A41B9AF03E7B2A90B413BDF4F3D5B105A4132992A58C0A90B41E3A59B545C105A4117FBCB2ECAA90B41EE7C3F5D5D105A412597FF9096A90B41B07268695E105A41F797DD9373A90B41D7A370B55D105A41F6B9DACA4FA90B4117D9CEE75C105A411461C35317A90B41819543835C105A41F5DBD7411EA90B414C3789615B105A41100000007DF2B0D023AA0B41250681F554105A41A267B32A3EAA0B4146B6F3CD53105A41913177ED58AA0B412B8716F152105A4152499DC071AA0B416891ED6452105A41B5C876BE8EAA0B41BC74939852105A41DE2406C1A3AA0B41D7A3705553105A411A04560EB4AA0B41643BDF7754105A417D6132D5B2AA0B41F853E37D54105A41BBDA8AFDBAAA0B414C37893155105A411C2FDDE4ABAA0B410E2DB28556105A414725750295AA0B41022B870E57105A41FE87F45B81AA0B419EEFA73E57105A41B094650852AA0B41CBA1456657105A41D52B659932AA0B416666668E56105A41C68F313724AA0B41295C8F7255105A417DF2B0D023AA0B41250681F554105A41
222353	03	609	609010557	6090239572	79	480.350000000000023	0106000020FB0B0000010000000103000000020000000E000000B2BFECDEFEAF0A41F853E3D5690D5A411926538542B00A413108ACBC690D5A41168C4AAA0FB50A412B8716A96B0D5A4184C0CAA132B50A41BE9F1AB76B0D5A419E11A5FD7DB50A41C1CAA10D640D5A419B779C229CB50A41BE9F1AC75E0D5A4117FBCB2EA4B50A41AC1C5A945C0D5A41D9817306B5B40A411058396C5C0D5A4181B74002E3B30A412731084C5C0D5A41E27A146E6BB30A414E6210905F0D5A410100004075B20A41B29DEF6F5F0D5A4117FBCB6EFDB00A41713D0AB75E0D5A41BF30996A2BB00A41A4703DCA670D5A41B2BFECDEFEAF0A41F853E3D5690D5A4108000000F0384547EAB40A4123DBF926650D5A41A9C64B7710B50A4193180456640D5A41BD96908F24B50A41250681FD630D5A418A4160A535B50A410E2DB215640D5A419F5E29CB1DB50A41333333E3670D5A4107819543EBB40A410C022B27670D5A41E4A59B84D6B40A41E92631F8650D5A41F0384547EAB40A4123DBF926650D5A41
222354	03	609	609010557	6090321620	491	1002.27999999999997	0106000020FB0B00000100000001030000000100000021000000FF65F7E491940B413D0AD763A90E5A412C871699E3940B41BA490CA2B00E5A41752497FF72950B41022B878EBD0E5A41DD4603B8E2950B411B2FDDBCC50E5A4152499DC01E960B41AE47E1CACA0E5A41E3E9957261960B41B4C876C6D10E5A415D8FC2351C970B41A245B61BE50E5A41B9AF03E759970B41A8C64BEFEB0E5A417693184480970B4117D9CEFFEF0E5A41C9073D9B9C970B41643BDF4FF00E5A4187C95441079A0B4119045696E40E5A41B7F3FD94FB990B418FC2F5C0E30E5A417DF2B0D0B39B0B4177BE9F0ADC0E5A41A79BC4E02B9C0B41BE9F1AAFD50E5A418A4160E5539C0B41713D0A37D50E5A41579FABED859C0B41355EBA29D50E5A4150AF94E5DA9C0B415A643B97D30E5A4164EE5A021B9D0B41C520B002D30E5A41782D211F5B9D0B41EE7C3FF5D20E5A413E2CD4DA979D0B41621058B1D10E5A41B6A67947BB9D0B41B4C876E6CF0E5A417E3F351EF89D0B4146B6F3EDCC0E5A4191A0F8B1409D0B416891ED44C50E5A410BD7A330EE9B0B41C1CAA185B60E5A41F797DD93C4990B41000000E89F0E5A414D378981A2960B41AAF1D245B10E5A41A0ABAD18F6950B41AE47E1E2B40E5A41996E1243DA950B41FA7E6A7CB50E5A41C542ADE9C8950B415A643BA7B50E5A41996E12C3BE950B414260E590B50E5A413B92CBFFAC950B415C8FC2DDB40E5A41184850BCD7940B4137894138AC0E5A41FF65F7E491940B413D0AD763A90E5A41
222355	03	609	609010557	6090322125	165	544.440000000000055	0106000020FB0B0000010000000103000000010000001500000046D8F03466940B41FED47879A10E5A41526B9AF76E940B41B6F3FD5CA50E5A4183E2C7187C940B412FDD248EA60E5A41927EFB7A8A940B41EE7C3F15A70E5A4177711BCD12950B416891ED4CAC0E5A41F0C9C3C2B4950B41C976BE97B20E5A41F64A59C6CD950B41AE47E1AAB30E5A414FD1915CD4950B414E6210D8B30E5A41A11A2F1DEB950B418B6CE78BB30E5A41A857CA722D960B410E2DB23DB20E5A41E3C7983BA8990B4152B81EB59E0E5A41715F074EEB970B41508D97968B0E5A41F3D24DA290970B41BE9F1A3F880E5A41A935CD3B64970B41D9CEF7DB860E5A41176A4D33CC960B41B29DEF5F8B0E5A411BC05B2040960B41AAF1D2CD8F0E5A41238E753130950B41190456D6980E5A4158EC2F3B90940B410681959B930E5A41C998BBD66B940B41643BDF47950E5A41723D0A1768940B411F85EB599D0E5A4146D8F03466940B41FED47879A10E5A41
222356	03	609	609010557	6090322529	90	391.269999999999982	0106000020FB0B000001000000010300000001000000170000009FCDAA8F0FBF0B41BA490CD28F125A419A2A1895AEC00B411F85EB319D125A41DE24068114C10B41A245B66398125A413B234A7B86C10B41BA490C0293125A410CB5A63921C20B41E7FBA9998D125A41312AA9937DC20B414A0C02538A125A4155E3A59BD1C20B41B07268B987125A4127E4831E2DC30B419A99992984125A4104780BA45AC20B41759318247B125A41570E2DB237C20B414C3789E17A125A41AB6054121DC20B41FCA9F18A7A125A4127E4831E04C20B4123DBF99E7A125A41335530AAE3C10B41D578E9C67A125A41B637F8C2ACC10B4152B81EBD7B125A41585BB1FF16C10B41EC51B8567E125A418CFD653795C00B4160E5D03281125A4115AE472164C00B41A4703D9A82125A4115AE472101C00B41A69BC40886125A41184850FC8ABF0B4196438BAC89125A410F2DB29D3DBF0B41DD2406C18B125A419B9999D914BF0B41333333F38C125A413E9B55DF05BF0B412731082C8E125A419FCDAA8F0FBF0B41BA490CD28F125A41
222357	03	609	609010557	6090322630	77	543.830000000000041	0106000020FB0B000001000000010300000001000000360000008C6CE7FBEAC90B414A0C023B23135A418AD2DE2009CA0B41B07268C124135A41AF47E1BA8ECA0B41250681C529135A413F7958A8EFCA0B411F85EBF92C135A418B8EE432FBCA0B411D5A64932B135A41F1A7C68B0ACB0B416F1283102A135A41653BDF0F2BCB0B41F2D24D1228135A41BD05121458CB0B4121B0724829135A418C6CE7BB8FCB0B411904569E28135A4170128300AACB0B417D3F35AE27135A4190537424DACB0B419CC420A026135A41B6A67947ECCB0B41B6F3FD8C25135A41D3DEE00B09CC0B41D578E94625135A419131772D11CC0B41D122DBF124135A41966588A319CC0B416DE7FB7924135A41D4BCE31440CC0B414C37894124135A410022FD7673CC0B416DE7FB3924135A41E5839E4DA9CC0B415EBA49AC23135A418204C58FF7CC0B41A4703D7221135A41115839B434CD0B41E17A14AE20135A412831086C09CE0B41F853E31D1B135A41AA13D0C4C1CE0B410681951316135A41518D97AEF4CF0B416DE7FB410E135A41A03C2C5416D00B416DE7FB490D135A41A5DFBECE13D00B4146B6F3850C135A414872F94FFFCF0B412DB29D8F0B135A41D678E9E6C8CF0B41F6285C5F09135A41472575029FCF0B41C3F528D407135A417B36ABBE7ACF0B415A643BBF08135A41D756ECAF1BCF0B41295C8F3A0B135A4153B81EC5B5CE0B418FC2F5280E135A419F5E29CBF5CD0B41508D972E13135A417524973F99CD0B41D578E98615135A41CB32C4F12ECD0B413789412018135A4135A2B43715CD0B41DBF97EAA18135A417DD0B399E9CC0B41B29DEF6718135A4197218E35B9CC0B41FCA9F12A18135A41CECCCC8C8FCC0B41BA490C5A19135A412E211FF46FCC0B41C520B0A219135A41E96A2BF62ACC0B4146B6F32519135A413D4ED15105CC0B4191ED7C8F18135A4113143F06D2CB0B41FED478F119135A41D4BCE3144ECB0B41D122DB111D135A41CECCCC8C19CB0B419A9999311B135A41365EBA49FACA0B4148E17A141A135A41C464AA20B6CA0B41C520B09A1B135A4109AC1C9A7CCA0B413D0AD7F31C135A412675025A57CA0B41EC51B81E1E135A418B1F63AE5BCA0B413BDF4FED1E135A41D3DEE04B38CA0B4191ED7C5F1E135A414950FCD80BCA0B413F355ECA1F135A412BA91350ECC90B419318043621135A41C464AA20E5C90B4196438B0422135A418C6CE7FBEAC90B414A0C023B23135A41
222358	03	609	609010557	6090322933	54	374.310000000000002	0106000020FB0B00000100000001030000000100000016000000D44D625033B20B41C74B37D1B1105A410A8A1F23A9B20B414A0C0273B6105A41B17268511EB30B41FCA9F1D2BA105A41FEF6752048B30B41D7A3706DBC105A41CA54C16882B30B417B14AED7BE105A41FA31E62EB3B30B41894160CDC0105A4190E4F29FF0B30B415C8FC28DC3105A4197B20C712DB40B41894160C5C5105A41715F070E81B40B415839B418C9105A4144AD691EC1B40B41C3F52804CC105A41FBEDEB80E4B40B414E6210B8CC105A41C8BAB88D16B50B414E6210B8CC105A41312AA9535AB50B41666666A6CB105A41518D97AE7CB50B41A01A2F4DCB105A41F9C264AABDB50B414E621098CA105A41EDC039E3E0B50B41D9CEF7CBC9105A41774F1E16FAB50B41F4FDD4F8C8105A41E4A59BC4F1B20B413108AC6CAB105A41CC10C77AE8B20B4139B4C83EAB105A41EB95B20C58B20B41FCA9F182AE105A413DBD525606B20B41B81E8573B0105A41D44D625033B20B41C74B37D1B1105A41
222359	03	609	609010557	6090323034	59	387.550000000000011	0106000020FB0B0000010000000103000000010000000E000000E4A59BC4F1B20B413108AC6CAB105A41774F1E16FAB50B41F4FDD4F8C8105A4189855A934AB60B41DF4F8D77C7105A412A5C8F82B4B60B410AD7A340C5105A418A4160E502B70B415839B4F0C3105A4112C7BA78A0B50B41B4C876D6B5105A41B2506B1A94B50B4121B07258B5105A4118B7D1C03DB50B4179E92659B3105A41441CEB2219B50B41F0A7C64BB2105A41AFB6623F02B50B4160E5D042B1105A419DC42070F6B40B41DBF97E9AB0105A41C464AA20E5B30B41D9CEF7A3A5105A4163A1D67441B30B4114AE4711A9105A41E4A59BC4F1B20B413108AC6CAB105A41
222360	03	609	609010557	6090323640	604	1587.90000000000009	0106000020FB0B0000010000000103000000010000001300000058EC2F3B45810B414C378951CD0D5A410D93A9C2A3860B41713D0A4F070E5A41085F98CC678E0B4117D9CEAF5A0E5A411995D409798E0B411904569E5A0E5A41C1EC9E7C4E8F0B412DB29D77550E5A41AB605452DF8F0B41FA7E6A4C510E5A4164EE5AC24F900B4104560E154E0E5A41FCCBEE0990900B414A0C02034D0E5A41EF7C3F7534910B41C3F5285C4A0E5A41E1BE0E9CF4900B419CC42028480E5A41A11A2F1DBB8F0B41448B6CD73C0E5A412A5C8F02228D0B415839B4B8240E5A415EDC4683328B0B41EE7C3FAD120E5A41C620B072E2870B413BDF4FB5F30D5A4161E5D02276850B41986E1223DD0D5A4121D26F9FC3830B41F853E31DCD0D5A41C00E9C33C5820B41E7FBA9F9C30D5A41B91E85AB3B810B417F6ABCF4CC0D5A4158EC2F3B45810B414C378951CD0D5A41
222361	03	609	609010557	6090323741	169	798.950000000000045	0106000020FB0B00000100000001030000000100000015000000388941A008920B414A0C02D3CD0D5A411B51DA5B96920B41190456C6D20D5A4125287E0CC0920B411904569ED20D5A41B39DEF67FA920B41508D9756D40D5A414360E510CF920B4139B4C80ED60D5A414282E20798920B41C3F52844D60D5A41585BB1FF56920B415EBA495CD70D5A4150AF94A535920B4196438B14D90D5A411A04564E28920B411F85EB11DA0D5A41842F4CA63A920B41CDCCCC6CDB0D5A41032B8716EA940B41B6F3FD8CF00D5A411F166A8DE7980B41D9CEF743100E5A41FE87F49B01990B413F355EBA0F0E5A41A9C64B3753990B41DBF97E4A0D0E5A4104098ADFD2990B41D9CEF70B090E5A41752497FF0B9A0B41E92631D8060E5A4117D9CEF795930B4137894130D30D5A41927EFB7A77930B4179E926A9D30D5A4188A7570AB1920B41AE47E142CC0D5A41C386A75769920B413BDF4F45CB0D5A41388941A008920B414A0C02D3CD0D5A41
222362	03	609	609010557	6090324549	171	668.529999999999973	0106000020FB0B00000100000001030000000100000026000000790B24A8DE7E0B416ABC74B3510F5A41625452E7B67F0B41BA490C02760F5A416BBC745372800B41A69BC478950F5A418E976ED294800B41DF4F8D3F950F5A4130DD24C6FE800B4137894110940F5A41F6B9DA8A72810B4137894180920F5A413C70CEC888810B4185EB5130920F5A41458B6CA71A830B410AD7A388890F5A417E3F355E1A830B411B2FDDE4880F5A41865AD3BC04830B414C378971880F5A41A0ABADD8C3820B41CFF75323880F5A41A0ABADD897820B4108AC1CE2870F5A41EB95B20C85820B41C1CAA15D870F5A41BB490CC27A820B414C378911860F5A41F6B9DACA77820B418FC2F5C87D0F5A41A5DFBECE63820B416891EDAC7A0F5A41799CA2E335820B41C74B3799770F5A4137CD3B0E07820B41C3F528A4750F5A4117D9CE37DF810B41C520B0DA730F5A41B8408262C2810B41DF4F8D67710F5A4125B9FC07B1810B41E7FBA9796E0F5A414C59867894810B411D5A64636B0F5A419E80260261810B4123DBF99E680F5A416007CE992F810B4100000028670F5A419B99999916810B4125068105660F5A4101917EFB04810B41E3A59B6C640F5A410C2428BE02810B41FED478F9620F5A41CC10C77AEF800B41DBF97E92610F5A4136EF3845A4800B4117D9CE975C0F5A41782D211F7B800B41333333AB590F5A413E9B55DF5F800B4125068105570F5A41FF65F7E449800B413BDF4FAD540F5A418A4160A526800B41BC7493C0520F5A41E81DA76801800B41068195E3510F5A41063411B6B57F0B416891ED2C510F5A41D9F0F4CA507F0B41B4C87606510F5A414FD1911C117F0B41333333F3500F5A41790B24A8DE7E0B416ABC74B3510F5A41
222363	03	609	609010557	6090324751	1023	2244.13000000000011	0106000020FB0B0000010000000103000000020000005A000000CCA145F6D5570B419CC42090D60E5A410022FD36955B0B419CC42018040F5A41F8E461A1B55C0B41643BDFAF110F5A41FBEDEBC0E15C0B41E9263140110F5A410A8A1FE3FD5C0B41F2D24D12120F5A413355306A235D0B416F1283C0130F5A41FA31E62E4A5D0B415C8FC27D150F5A410BD7A3305E5D0B41643BDF77160F5A41AFB6623FBD5D0B414C3789191B0F5A415327A0890E5E0B41931804EE1E0F5A415D8FC235645E0B41508D97FE220F5A41B91E852BF75E0B416F1283D0290F5A414B7B83EF185F0B415839B4382B0F5A418B8EE472345F0B411F85EBB92C0F5A411A73D71227600B41C74B37B9270F5A414E158CCA86600B4108AC1C222C0F5A412831086C11610B41621058F1310F5A41905374248C610B4100000068340F5A41ABF1D28DCC610B4110583934360F5A41774F1E56EB610B41FCA9F162370F5A41A0ABAD1824620B411283C0EA390F5A415E6DC5BE47620B4104560E753C0F5A41381AC09B35620B4114AE47C93D0F5A41F341CF262D620B410C022B5F3E0F5A4170F0858911620B41E5D022833F0F5A41859ECD6ABA610B41A69BC498410F5A4133E6AEE58A610B411F85EBC9420F5A41A345B6F376610B41C976BECF430F5A4168D5E72A77610B41D34D62C8440F5A41CE3B4E917B610B41022B87E6450F5A417BC729FA98610B41736891F5470F5A4122B072A8E7610B41B29DEF374C0F5A414FD1911C3C620B41AAF1D24D500F5A41B47BF27056620B41F4FDD400510F5A41AC3E579B9D620B41E9263170510F5A412675025ADF620B411B2FDDB4510F5A41E661A1D605630B412FDD24EE520F5A4124DBF9BE1B630B41E3A59BEC530F5A41B91E852B43630B41E3A59B14560F5A417A58A835F6620B41D122DBE9570F5A415E6DC57E76620B41F6285C175B0F5A413CDF4F8D2D620B4183C0CAC15C0F5A4147B6F3BD0F620B41DF4F8D3F5E0F5A41F185C91401620B41D578E9A65F0F5A4149E17AD404620B419CC42050610F5A41EF7C3F3509620B4185EB51E0620F5A41B840822230620B416DE7FB39660F5A41EB95B2CCB0620B41E5D02243700F5A4106A3927A1A630B41986E12E3760F5A4108F01648A1630B41819543037F0F5A41DACEF793D3630B416ABC746B810F5A41585BB13F2E640B415A643B07870F5A41842F4C6659640B41B81E8523890F5A419DC420F0A1640B41B29DEF7F8C0F5A414A2EFF612D650B41CDCCCCAC920F5A418816D90E88650B41621058A9960F5A41A9C64BF7C5650B411283C092990F5A412CF697DDF4650B416DE7FB819B0F5A41D066D5A72A660B41AE47E1B29D0F5A41365EBA8955660B412FDD24CE9C0F5A412DD49A668E680B410AD7A3F88F0F5A410FBE30D992670B4123DBF976830F5A41C1CAA18592660B4146B6F3E5760F5A41411361431C660B415A643BF7700F5A41306EA30124650B419EEFA77E650F5A411D7C6172FF640B41C3F52874630F5A41BB6B0939CC650B4160E5D0F25E0F5A411C2FDDE409670B41CDCCCCE4570F5A4146D8F07492670B41A69BC4C8540F5A4123FDF6F59E660B41D578E90E4A0F5A4192ED7C3FF9650B416DE7FBF1420F5A41570E2D7296650B41CFF753733E0F5A418E976ED2BC640B41713D0A17350F5A410CB5A639FC630B41D578E96E2C0F5A41996E12438F610B413333338B110F5A4145696F70FF600B41AC1C5A540B0F5A410A1B9E5E73600B414C378931050F5A4177E09C911A600B4123DBF93E010F5A414E840DCF02600B4139B4C8AEFF0E5A4189855A93895F0B4179E926C9FA0E5A417E3F351EA25E0B41A245B6BBF00E5A4170128300CB5D0B41448B6C37E70E5A410EE02D50485D0B412DB29D5FE10E5A41501E166A405C0B4108AC1C32D60E5A415939B4C81F5C0B41508D976ED40E5A414360E510775B0B4123DBF97ED40E5A41FFD47829565A0B414A0C020BD50E5A418B8EE432DB580B41986E12D3D50E5A415CB1BF6CCE570B412DB29D3FD60E5A41CCA145F6D5570B419CC42090D60E5A410B000000D222DBB9DD650B4104560EC5590F5A416991ED3CEF650B41713D0A7F590F5A41A4923A81FF650B4127310884590F5A41B84082620B660B41E17A14DE590F5A41472575020C660B410C022B6F5A0F5A413580B7C005660B41A4703DBA5A0F5A41AFB6627FFA650B41378941D85A0F5A41A11A2F1DEC650B41333333D35A0F5A41E9D9AC7AE1650B411058399C5A0F5A4158EC2F3BDB650B419CC420385A0F5A41D222DBB9DD650B4104560EC5590F5A41
222364	03	609	609010557	6090328690	246	798.019999999999982	0106000020FB0B00000100000001030000000100000006000000085F980C77510B41AE47E1A2D90E5A41DE24068155570B418716D9261F0F5A413580B7C06E590B416891ED7C140F5A414950FC1813550B419A9999D1E10E5A4117D9CEB737540B4121B072D0D70E5A41085F980C77510B41AE47E1A2D90E5A41
222365	03	609	609010557	6090328993	297	828.019999999999982	0106000020FB0B0000010000000103000000010000000A0000004950FC1813550B419A9999D1E10E5A413580B7C06E590B416891ED7C140F5A4111E9B7AF385A0B419EEFA7EE1C0F5A41AD1C5A64025C0B41C3F528E4140F5A41F8E461A1B55C0B41643BDFAF110F5A410022FD36955B0B419CC42018040F5A41CCA145F6D5570B419CC42090D60E5A415CB1BF6CCE570B412DB29D3FD60E5A4117D9CEB737540B4121B072D0D70E5A414950FC1813550B419A9999D1E10E5A41
222366	03	609	609010557	6090329195	209	633.129999999999995	0106000020FB0B00000100000001030000000100000030000000EE9E3CAC08590B4177BE9F02330F5A412B3A924B4D590B41448B6CF7350F5A41B9AF03E779590B41190456EE370F5A41A4923A81D6590B411D5A64233C0F5A4145696FB01C5A0B412731085C3F0F5A417DD0B3593D5A0B41E9263188400F5A41ABF1D20D585A0B41CFF753AB410F5A41F1A7C64B8F5A0B417368916D410F5A41C8BAB84DD25A0B410E2DB2AD400F5A41CF19511A305B0B41B6F3FD7C3F0F5A41CBC3422D7A5B0B411F85EBB13E0F5A412FFF213DB85B0B41CFF7539B3E0F5A41BF30992A5B5C0B41986E12A33D0F5A41A11A2F1DE65C0B4191ED7C9F3C0F5A41645DDCC6225D0B41BE9F1A673C0F5A417BC729FA995D0B41CBA145F63B0F5A4112C7BA78C15D0B41560E2D7A3B0F5A41B39DEF67155E0B41621058A93A0F5A415D2041F15B5E0B41FED478E9390F5A416EE7FBA9935E0B41F853E345390F5A41AC3E57DBC75E0B4100000048380F5A4199DD9387F85E0B411D5A6493370F5A41684469EF485F0B4177BE9FC2360F5A417C832F4C865F0B41E17A140E360F5A41579FABED975F0B415C8FC275350F5A41ED2FBBE76C5F0B410C022BE7320F5A4141A4DFBE575F0B41333333AB310F5A41351136FC0C5F0B41190456E62D0F5A4138894120EC5E0B41AE47E1E22C0F5A4166AA6014D85E0B413BDF4FBD2B0F5A41EC73B555755E0B4139B4C8B6260F5A417AE92631055E0B418FC2F5C8200F5A4166AA6054AC5D0B4146B6F3251D0F5A41D1B359F56E5D0B41C3F5282C1A0F5A414950FCD8295D0B419CC420D8160F5A413F795868FD5C0B418D976EBA140F5A41ADADD85FDE5C0B41E5D02273130F5A41B6A67947B85C0B41D578E9B6130F5A416EE7FBA98E5C0B414A0C0233140F5A411C2FDDE4B15B0B41986E1263180F5A41BB490CC2F25A0B41508D97DE1B0F5A4183734654775A0B413F355E3A1E0F5A41D6E76A2B625A0B413108AC941E0F5A41B7847CD03C5B0B411B2FDD0C290F5A41769318C4975A0B41CFF753FB2B0F5A4117D9CEF72D5A0B41EC51B8BE2D0F5A41DE24068144590B415839B410320F5A41EE9E3CAC08590B4177BE9F02330F5A41
222367	03	609	609010557	6090388712	161	861.879999999999995	0106000020FB0B0000010000000103000000010000001300000017D9CEF795930B4137894130D30D5A41752497FF0B9A0B41E92631D8060E5A412D6519629A9B0B41E7FBA949130E5A41F41FD2EF9F9C0B41333333CB0D0E5A4153B81EC5019C0B41819543BB080E5A4183734654259A0B41621058B9F90D5A41ADADD85F249A0B41B6F3FDCCF80D5A41ADADD85F249A0B417F6ABCECF60D5A4147257502C0980B41AC1C5A94EB0D5A41D222DBF983980B41B29DEF8FEC0D5A41C68F31372E980B41DD240601EF0D5A412BA91310F6970B415A643B17ED0D5A413E2CD41A41980B414C378989EA0D5A415FBA49CC4A980B41448B6C2FE80D5A41F48E537407960B419A999951D60D5A41FF43FAED91940B4104560EADCA0D5A41BA8D06B0E1930B41CDCCCC44D00D5A41701283C08E930B4139B4C8BED20D5A4117D9CEF795930B4137894130D30D5A41
222368	03	609	609010557	6090396186	173	619.07000000000005	0106000020FB0B0000010000000103000000010000000A00000094A982519F780B41EC51B86E0E0F5A4145696F70017A0B41C1CAA1BD220F5A41C9073DDBA07B0B41E17A14A63A0F5A4130DD24C6187C0B41B4C876B6410F5A41B5C8767E917E0B41D9CEF713310F5A411383C00AD87C0B41E17A1406200F5A41845149DD037B0B41A8C64BAF0D0F5A41F41FD2EF607A0B4191ED7C27120F5A41625452E791790B412731089C0A0F5A4194A982519F780B41EC51B86E0E0F5A41
222369	03	609	609010557	6090431350	151	586.350000000000023	0106000020FB0B000001000000010300000001000000090000007D6132D5E47C0B41D9CEF75B9D0D5A41C15B2081CE7E0B41AC1C5A1CB20D5A4147B6F3BD15810B41E7FBA921CB0D5A41A935CDFBDC820B41000000B0C00D5A414EF38E533F810B4123DBF90EB00D5A4190E4F25F037F0B41022B87DE980D5A41BB490C42C37E0B41BE9F1A7F960D5A4190C2F5E8C17C0B41B81E85E39B0D5A417D6132D5E47C0B41D9CEF75B9D0D5A41
222370	03	609	609010557	6090431451	33	235.75	0106000020FB0B0000010000000103000000010000000A0000001F166A8DE7980B41D9CEF743100E5A41D8A370FD3E9A0B41F0A7C6BB1A0E5A41FDA9F1D2CC9A0B4117D9CE4F170E5A41FFD478A9539B0B4114AE4791140E5A412D6519629A9B0B41E7FBA949130E5A41752497FF0B9A0B41E92631D8060E5A4104098ADFD2990B41D9CEF70B090E5A41A9C64B3753990B41DBF97E4A0D0E5A41FE87F49B01990B413F355EBA0F0E5A411F166A8DE7980B41D9CEF743100E5A41
222371	03	609	609010557	6090440343	60	322.639999999999986	0106000020FB0B000001000000010300000001000000120000006844692F10BE0B41273108E4C2125A41F8065F189DBE0B418FC2F5D8C6125A41176A4D3330BF0B4117D9CEDFCB125A41B39DEF6794BF0B41AE47E112CF125A41E0E00BD3D2BF0B419A999921CF125A410C2428FE13C00B41CDCCCC64CE125A414950FC183DC00B412FDD2416CD125A4137AB3E9786C00B41A69BC4A8CC125A4115AE47A1C1C00B41B0726869CC125A41C998BB160EC10B41E3A59B1CCC125A4100B37BB239C10B413108ACACCB125A41744694B66AC10B41FA7E6A84CA125A4159A8358D95C10B411B2FDDC4C8125A4190C2F52822BF0B411283C042B4125A4132992A58D9BE0B4179E926E1B8125A411BC05B2036BE0B414E621020C1125A41BCB88D061CBE0B418195435BC2125A416844692F10BE0B41273108E4C2125A41
222372	03	609	609010557	6090440444	70	357.529999999999973	0106000020FB0B0000010000000103000000010000001800000098FF90BE83BB0B414260E5A0AC125A412BA91310A0BB0B41986E12B3AD125A41DACEF713C3BB0B4152B81E35AE125A41063411F6C4BB0B413333331BAF125A417B36AB7E0EBD0B410AD7A3D0B9125A41BB6B0939D7BD0B4148E17A5CC0125A41DF02090AECBD0B41F6285C47C1125A41CAE53FA43DBE0B411F85EB91BD125A4171CE88128FBE0B4191ED7CEFB8125A41E1BE0E9CCDBE0B41759318ACB5125A41A401BC0502BF0B41E3A59B44B3125A41D1B35935D0BC0B4117D9CE27A1125A41AB6054528FBC0B41D578E92EA2125A4136EF384548BC0B4148E17A7CA3125A41C07D1DF819BC0B411D5A644BA4125A41678863DD02BC0B41713D0A6FA5125A412ACB1087FDBB0B416ABC748BA6125A413DBD521601BC0B41B29DEFA7A7125A4183734614F9BB0B41EE7C3FBDA8125A412C871619D7BB0B414C378989A9125A41DACEF713C3BB0B41FED47811A9125A41913177ED88BB0B41DF4F8D1FAA125A41EA2631485BBB0B411283C032AB125A4198FF90BE83BB0B414260E5A0AC125A41
222373	03	609	609010557	6090526431	23	285.20999999999998	0106000020FB0B000001000000010300000001000000140000003AB4C876FB8F0B419A999941550E5A4187C9548143900B41D34D6218530E5A412085EB519F900B41C3F52814510E5A414CC8073D0F910B41068195A34F0E5A412CF6979D3C910B411904569E4F0E5A4137AB3ED760910B414260E540500E5A41A0ABAD18B5910B41DD2406B1520E5A41D9F0F48AE3910B41068195D3530E5A4133E6AEA589920B41068195EB4D0E5A41EC51B81E10930B41FA7E6AD4480E5A4127E4831E4F930B41A245B6C3460E5A416E567DAE0A930B41DF4F8D07440E5A41EF7C3F755D920B416ABC740B470E5A41B8627F992C910B41FCA9F1E24B0E5A41D0F753E3A6900B4114AE47994E0E5A41244A7B4355900B41B4C8763E500E5A41782D21DF2C900B4108AC1C4A510E5A4155E3A59BFC8F0B4191ED7C1F530E5A412EB29DEFD98F0B417F6ABCEC530E5A413AB4C876FB8F0B419A999941550E5A41
222374	03	609	609010557	6090537242	683	1230.86999999999989	0106000020FB0B000001000000010300000001000000680000004B7B83EF8B8D0B41000000101F105A41221FF42CE28D0B412B8716B12C105A413C014DC41F8E0B41448B6CC736105A415EDC4643428E0B41B81E852B3D105A4137CD3B0E518E0B411B2FDD0440105A41A879C7E9768E0B41295C8F7A41105A418226C286A98E0B4191ED7C5F42105A41F5DBD741DD8E0B41EC51B80643105A419DC420B0188F0B4114AE477943105A412A5C8F023D8F0B412DB29D6743105A418451499D4E8F0B41B81E858B42105A41B8627FD9508F0B4154E3A5CB3F105A415E6DC5BE7B8F0B41AE47E1623D105A4177711BCDB38F0B41BC7493303C105A41063411B6FE8F0B419CC420383B105A41D3DEE04B20900B41448B6CB73A105A41351136FC3F900B41A4703D2A3A105A41D44D62105A900B410AD7A38038105A41ED2FBBA760900B4152B81E4D36105A41260681D586900B41378941C034105A41A2F83166AA900B416666669E33105A416991ED7C26910B419CC4204831105A41A4923A8170910B41EC51B83630105A4125287E0CC3910B41E92631E82E105A41E6F21F1205920B414260E5402E105A418D4AEA844B920B41DBF97E522E105A41EA482E7F91920B41333333732D105A4175B5157BC3920B415839B4E02C105A418204C54F0E930B415839B4882C105A419D33A27467930B412FDD241E2B105A41EC51B81EC0930B41736891CD29105A4139D6C5AD02940B41FED478D128105A417DD0B35981940B418FC2F54026105A4152499DC00B950B4191ED7CA723105A41E358173746950B411D5A64BB22105A4124DBF9BE82950B41CDCCCC1421105A41625452E7ED950B41E17A14FE1F105A4191A0F8B113960B41B81E856B1F105A41E7AE256443960B418B6CE7831F105A415A17B79171960B410AD7A3E01F105A4134C4B12E99960B41FED4784120105A4181B74002CE960B41713D0ACF1F105A41E1BE0E9CF8960B41713D0A571F105A41D300DE8234970B411B2FDD3C1E105A41DC8AFDA54D970B4139B4C8BE1E105A41DDD781337F970B41986E12A31F105A4104780BA4C5970B41A69BC49020105A410F2DB25D05980B41333333C320105A41C620B03244980B414C37894120105A41EDC0392385980B41FED478911F105A410712143F9C980B4154E3A55B1E105A413AB4C876B4980B41F853E3AD1C105A416E567DAECC980B41CDCCCCEC1A105A419587851AFD980B41BC7493B018105A41BCB88D0628990B41FA7E6A7C17105A412FFF213D40990B41295C8F0A16105A4149E17A5420990B413F355EFA13105A41FB7E6AFCCF980B4179E926D910105A41C7DCB5C4AC980B419CC420700F105A41701283C0AB980B416F1283380E105A4163A1D674A8980B416ABC74130D105A41168C4AAA94980B4123DBF9BE0B105A41EF5A42BE77970B419CC420E000105A41441CEB2219940B41D9CEF7F3E20F5A410A8A1FE3D1930B4181954333E40F5A4125287ECC88930B414E6210E0E40F5A41AB8251497B930B418716D95EE70F5A41F875E01C9A930B417D3F35B6E80F5A41782D211FB0930B41F4FDD488EA0F5A41518D972E6F930B41F853E375EC0F5A41B7847C1037930B41DF4F8D37ED0F5A412AED0DBE1D930B41AAF1D21DEC0F5A419D33A27430930B41190456BEE90F5A41F6B9DA8A10930B4121B072C8E70F5A41850D4F2FB9920B41986E1223E80F5A414282E24795920B41F0A7C6F3E80F5A41C1EC9EBC42920B41F6285C7FEB0F5A419ABB96D022920B418716D9DEED0F5A41F341CF6608920B41986E12C3EF0F5A410A1B9E1ED5910B41CFF753D3F10F5A41C8BAB88DB9910B41CDCCCC2CF30F5A41645DDC8686910B413BDF4FA5F40F5A41D7C56DB451910B410C022B6FF60F5A413D4ED19119910B4146B6F375F90F5A41306EA341F5900B41273108CCFC0F5A41D6096822D3900B411F85EB19FF0F5A41806ABC3485900B418B6CE77300105A417C832F4C56900B419A9999F901105A41ED2FBBA73F900B415C8FC26504105A41865AD3BC14900B417B14AEDF06105A41EB0434D1D98F0B41B29DEF4709105A413DBD5216BF8F0B41BC7493380A105A41D222DBF9A98F0B411F85EBC10B105A41B8D1005E8D8F0B4123DBF9060E105A412BA913108A8F0B418D976EE20F105A4145FAEDABA68F0B41EC51B8C611105A41799CA2E3B38F0B41BA490C9A12105A41373CBDD2458F0B41E92631F816105A4177711BCD198F0B41A01A2F8D18105A419CE61D27DC8E0B4117D9CEF719105A41C386A797938E0B41B4C876361B105A41DA3D7918428E0B418195433B1C105A411BC05B20D48D0B41C74B37791D105A414B7B83EF8B8D0B41000000101F105A41
222375	03	609	609010557	6090548053	195	602.519999999999982	0106000020FB0B0000010000000103000000010000000E0000000C2428BE54C50B41931804E6F50A5A41A4923A4187C50B41F2D24D62F90A5A41C07D1DB819C80B41D122DB19F60A5A4101917E3B35C80B41D578E906EF0A5A418148BFFD42C80B412FDD245EE30A5A41837346544BC80B4196438B6CDC0A5A41010000C050C80B4121B07288D10A5A418F06F01654C80B41F853E3DDC70A5A41176A4D3357C80B41643BDF2FC00A5A4111E9B7EF14C80B418D976EB2C00A5A41D52B659907C70B4121B072C0C20A5A417E3F351EF9C50B413D0AD71BC50A5A419ABB96D065C50B41D122DB39C60A5A410C2428BE54C50B41931804E6F50A5A41
222376	03	609	609010557	6090576345	55	320.779999999999973	0106000020FB0B000001000000010300000001000000130000007C832F4CBA8C0B4137894140A50F5A412085EB51E28C0B418FC2F5B8A70F5A414D378941D68D0B414260E5A0B50F5A41D756ECEF418E0B41DD2406B9B30F5A418E976ED2808E0B410E2DB225B70F5A417B36AB7E9B8E0B41D9CEF7EBB70F5A419587851A318F0B41D9CEF763B50F5A410E4FAF145A8F0B414A0C020BB10F5A412FFF213D7F8F0B416666663EAE0F5A419ABB9610978F0B411283C0A2AC0F5A4160984CD5948E0B41448B6C5F9E0F5A41715F070E178E0B41D34D6230A00F5A41B637F8C2A38D0B41A8C64B47A00F5A4133772DE1278D0B4177BE9F8AA20F5A41C15B2041208D0B41F2D24D32A30F5A41C386A7570A8D0B41508D97A6A60F5A411B51DA9BDC8C0B41C976BE07A40F5A4124DBF97EB68C0B4117D9CEA7A40F5A417C832F4CBA8C0B4137894140A50F5A41
222377	03	609	609010557	6090605849	730	1343.75999999999999	0106000020FB0B00000100000001030000000100000029000000016F81C4B6B40A4175931834A50D5A411A73D7D21AB50A41F6285CD7A60D5A418F06F0163EB50A41B6F3FDECA80D5A41365EBA49B3B50A41D34D62C8B20D5A4170F0854955B60A41643BDF77BF0D5A41850D4FEF84B60A41F6285CEFC30D5A4118D9CEB7B1B60A416891ED64C50D5A416991EDBCC5B60A418B6CE71BC60D5A410B6822AC03B70A41EE7C3FF5CC0D5A418BB0E1E963B70A41273108A4D40D5A41C620B03258B90A41D7A370E5D30D5A41FA0FE9B754BA0A4189416045D30D5A410D93A98281BA0A418D976E82D30D5A41E73FA4DFB6BA0A4160E5D01AD40D5A412F90A07838BB0A413F355E82D30D5A41A5DFBE0EA6BB0A41B81E856BD30D5A413E0AD7E322BC0A41D7A370BDD00D5A414803788B15BC0A41C1CAA175CF0D5A419FEFA706CFBB0A417D3F3516C90D5A41845149DD07BB0A411B2FDD1CB80D5A4180FB3A705ABA0A411B2FDDACA70D5A41BF30996AE1B90A41560E2D9A9A0D5A41441CEBE286B90A41105839A4960D5A41312AA95341B90A41FED47889900D5A418295438BC3B80A416F128398840D5A4188A757CA81B80A418FC2F5807F0D5A41D59AE69D42BA0A41D7A37095640D5A41F263CC9D93BA0A410AD7A388600D5A415BD3BCE379BA0A4123DBF9665F0D5A41B17268D107B80A41BC749328550D5A411BC05B6046B70A41F6285C17520D5A41E73FA49F62B60A41105839B4510D5A41B637F8C2FAB50A411D5A64F3600D5A41AF47E1BAC0B50A41DF4F8DDF680D5A411D7C61B296B50A415A643B8F6E0D5A41A5DFBECE57B50A41295C8FE2750D5A4130DD24C61BB50A416891ED8C7E0D5A410D93A9C2F3B40A41AAF1D2B5860D5A410A1B9E1ED8B40A419A999929910D5A4170F08589BBB40A418B6CE7E39E0D5A41016F81C4B6B40A4175931834A50D5A41
222378	03	609	609010557	6090605950	477	949.220000000000027	0106000020FB0B0000010000000103000000010000002A000000BE5296A18FB60A41250681954C0D5A41577DAEB66CB70A417593187C500D5A415EFE43BA3DB90A41FCA9F12A580D5A41DACEF713A3BA0A41B4C876365E0D5A41C15B2081FFBA0A418D976E7A590D5A413580B780B7BA0A416891ED2C580D5A41E0E00B93C1BA0A419CC42020570D5A414B0C026B52BB0A41C3F528F44D0D5A41EE0DBE707ABB0A415EBA490C4C0D5A410BD7A370CBBB0A41D578E9D64B0D5A41913177EDE9BB0A415EBA490C4C0D5A41244A7B83A8BC0A413F355E72410D5A419FEFA786C3BD0A41BC7493E02F0D5A41AEFA5CED34BE0A41F2D24D6A290D5A41A70A4625BEBE0A41F0A7C613210D5A419F5E29CB9CBE0A41CDCCCC5C200D5A413FE8D96C0FBD0A41D578E9DE190D5A414872F94F4FBC0A4183C0CAC1160D5A41DD68002FF8BB0A4162105859160D5A41DE2406C1B0BB0A41F853E385150D5A41859ECD2A94BB0A41B6F3FD54140D5A41D2915CFE5ABB0A41E3A59B5C140D5A41DE24068150BB0A416666660E110D5A41CF19515A3FBB0A41C3F528B40E0D5A4101000000C6B90A4160E5D0DA070D5A4171CE88129CB90A41508D97160A0D5A41518D97AE8FB90A41C74B37690C0D5A41F6B9DACAA1B90A414C3789E90E0D5A41655DDCC68DB90A41C3F5283C110D5A41D2915CFE60B90A4123DBF9CE140D5A41769318C428B90A416ABC7483190D5A4108CE19D110B90A41CBA1456E1A0D5A41E04F8D57E0B80A41643BDF0F1B0D5A414D37890182B80A41378941081B0D5A41FD1873D7F7B70A413108AC54270D5A4194A98251B1B70A4123DBF95E2F0D5A41032B87D655B70A416F128378370D5A411F166A4D3EB70A41A4703DCA390D5A41D4DEE04B38B70A4179E926D93B0D5A41C1EC9EBCF2B60A41C74B3789410D5A413AB4C876BBB60A4133333313470D5A41BE5296A18FB60A41250681954C0D5A41
222379	03	609	609010557	6090671830	667	1673.47000000000003	0106000020FB0B0000010000000103000000010000002D000000FFD4782963730B41986E12AB1F0E5A41F797DD536A760B41E7FBA9E1410E5A4103BC051228790B4179E92671610E5A417AE926314B7A0B41CBA145B66E0E5A41117A362B6D7A0B419EEFA7A66F0E5A4140355EBAE27A0B4110583934740E5A41D678E966337B0B41F0A7C603780E5A41B1726851817C0B41C976BEE7860E5A41A2D634EF647D0B4108AC1C52910E5A41D9817306047E0B415839B4A8980E5A4122B072A8387E0B41FED47831980E5A41E5839E8D0C7F0B41D578E93E940E5A412DD49A260D7E0B41D9CEF7DB880E5A4127C286A7017F0B4137894168830E5A41E0E00BD35E7F0B418B6CE713880E5A413B234A7B917F0B41C3F528448A0E5A415939B408F37F0B41355EBA598E0E5A4156302AE908800B4117D9CE3F8F0E5A419131772D19800B41D9CEF71B900E5A414F62101821810B410AD7A3C08B0E5A41FDA9F1920F820B4154E3A51B880E5A41B32E6EE395820B417B14AEEF850E5A4189635D1CC0820B41CDCCCCF4840E5A41BFC11766B7820B41DD2406D1830E5A41C620B0722B820B411D5A64EB7C0E5A41AE69DEF127810B4160E5D022710E5A417BA52C43C8800B41378941086D0E5A4147B6F3BDB57F0B41C520B0C2600E5A41221FF46C2D7F0B41AE47E1EA5A0E5A41B637F882157F0B41FA7E6A0C5A0E5A41085F98CC3F7E0B411D5A64FB5E0E5A417524973FE77C0B41D34D6268660E5A415E6DC5BED57C0B41D7A37095660E5A41A9C64B77A77C0B410C022BBF640E5A417F8CB9EB4A7C0B4154E3A58B600E5A414A9D8026467B0B4104560EA5540E5A4150AF94E5437A0B41D7A370A5480E5A41ABF1D28D84790B41CFF7531B400E5A4181B740C235790B41643BDF873C0E5A4102DE02C97A780B41C520B002340E5A41782D21DF40770B41CDCCCC8C250E5A418DDB68C05A760B418D976E221B0E5A4103BC05529F750B41B0726871120E5A412CF6979D90750B415EBA49CC120E5A41FFD4782963730B41986E12AB1F0E5A41
222380	03	609	609010557	6090671931	177	584.389999999999986	0106000020FB0B00000100000001030000000100000014000000ADADD85F98820B41E5D0225B6A0F5A41631058F95D830B41D7A3708D7B0F5A41FB7E6A7C6F830B41190456CE810F5A41335530AABE840B414C3789F17B0F5A41D756ECAFE6840B41E7FBA9097D0F5A416310583934850B41E7FBA9097D0F5A41994C154C41860B41643BDF8F770F5A415C423E68F4850B4123DBF98E740F5A410FBE30D9E8850B41333333D3740F5A41E96A2BB6D6850B412FDD242E740F5A41EDC039E3E1850B415C8FC2D5730F5A415C423EA86D850B41B81E854B6F0F5A41A62C435CFA850B41000000906B0F5A4130DD240642870B411283C06A620F5A41A2F831A633870B41C74B37E1610F5A41EC73B59555850B41EE7C3F854F0F5A41A267B36A52850B419EEFA79E4F0F5A41FE87F49B4B840B41C976BE7F510F5A414360E5D03B820B41EC51B826670F5A41ADADD85F98820B41E5D0225B6A0F5A41
222381	03	609	609010557	6090672133	1528	1606.81999999999994	0106000020FB0B0000010000000103000000010000001F000000100BB5668E630B411F85EBF9B20E5A416BDE71CAFA630B411058398CB90E5A416E567DAEA2640B41190456AEC30E5A41645DDCC69C640B41EC51B85EC50E5A412EB29DAFF2630B418716D92EC90E5A4141A4DFBE09630B41C520B0BACE0E5A41A11A2F9D4C650B418B6CE70BDD0E5A417468912DFD660B414260E5D8E70E5A41625452E77A670B41A69BC4D8EA0E5A416844692FB2670B41022B879EEB0E5A41C07D1DF8EC670B414A0C02C3EB0E5A41DEB584BC35680B41EE7C3F3DEB0E5A416A6FF005DE680B41E5D022E3E80E5A41B9AF0327366B0B41C3F52814E00E5A41676666A6D96E0B4162105809D20E5A416A6FF0456B700B41A01A2F25CC0E5A41EDC039E378710B4108AC1C42C80E5A417C14AE075B720B41068195A3C50E5A418C6CE7BBC9720B4117D9CE2FC50E5A41063411F605730B41D34D62D0C40E5A4161764F1E18710B416F128368AD0E5A411E3867C4F16F0B419CC420E09E0E5A4140C6DCF5AE6D0B416891EDDC820E5A411A04560E876C0B41713D0A5F740E5A41FBEDEBC04E6C0B41A69BC468740E5A41E4A59BC4EA6B0B41000000B0750E5A413F7958A8B2690B4179E926497D0E5A41BC270FCB44680B413333330B820E5A4180D93D3911660B4100000080890E5A41570E2D72CE610B41E92631D8970E5A41100BB5668E630B411F85EBF9B20E5A41
222382	03	609	609010557	6090687186	196	662.049999999999955	0106000020FB0B0000010000000103000000010000000A0000007AE9263183C30B4173689105010C5A411E5A64FB70C50B410AD7A3A8000C5A415939B40826C70B41448B6C97FC0B5A41557424D71AC70B41D9CEF753C90B5A41E5839E8D0BC50B419A9999D9CA0B5A41B39DEF6735C50B41068195DBE20B5A419B081B5E42C40B41F6285C37E30B5A41501E16AA44C40B418195434BE50B5A419F5E298B71C30B4166666656E50B5A417AE9263183C30B4173689105010C5A41
222383	03	609	609010557	6090725481	153	935.940000000000055	0106000020FB0B00000100000001030000000100000021000000647FD93D64800B41EC51B8E6520F5A41920F7A7679800B414C378969540F5A41EF7C3F35A1800B41A01A2F9D560F5A4153B81EC50D820B418941608D6E0F5A4117D9CE378F820B4177BE9FB2770F5A41645DDCC6A5820B4179E926D9790F5A412FFF213DBC820B41378941907C0F5A418DDB6800D9820B4160E5D0FA800F5A411E386704ED820B41105839BC830F5A419FEFA78608830B41B0726851850F5A41DDD7813363830B414C378941860F5A412C18951498830B4104560EDD860F5A41BB490CC224850B41F0A7C6637F0F5A41F0C9C3C25D860B41D122DB19790F5A41994C154C41860B41643BDF8F770F5A416310583934850B41E7FBA9097D0F5A41D756ECAFE6840B41E7FBA9097D0F5A41335530AABE840B414C3789F17B0F5A41FB7E6A7C6F830B41190456CE810F5A41631058F95D830B41D7A3708D7B0F5A41ADADD85F98820B41E5D0225B6A0F5A414360E5D03B820B41EC51B826670F5A41FE87F49B4B840B41C976BE7F510F5A41A267B36A52850B419EEFA79E4F0F5A41EC73B59555850B41EE7C3F854F0F5A41F3D24D22CF840B4196438B344A0F5A419ABB96D041840B41AAF1D2C5440F5A418A4160A5EE830B41B0726891410F5A41F5DBD741C7830B41C3F528D43F0F5A416BBC74D3B0830B415EBA4954400F5A415D8FC2B5B3800B41190456D6520F5A41905374644D800B41F853E3E5510F5A41647FD93D64800B41EC51B8E6520F5A41
222384	03	609	609010557	6090725582	28	298.740000000000009	0106000020FB0B0000010000000103000000010000000B0000001C0DE0AD71800B41F853E395500F5A410C462575A2800B41022B8756510F5A41FA0FE9F77F830B417F6ABC44400F5A4147B6F3FDB1830B419A9999093F0F5A41A3B437F889830B41621058793D0F5A413D4ED1515C830B4179E926C13B0F5A4117FBCB6E3C830B41DD2406A93A0F5A41645DDC8637830B41F853E3CD3A0F5A414950FC1805820B41355EBAB9410F5A41806ABC7402800B4191ED7CFF4E0F5A411C0DE0AD71800B41F853E395500F5A41
238980	03	886	609010557	8860016877	456	969.889999999999986	0106000020FB0B0000010000000103000000010000001D000000647FD9FD37EF0B41C520B0721E0B5A41D0F753E386EF0B413108AC5C220B5A418CFD65F7D0EF0B413D0AD7CB260B5A419F5E29CB13F00B41D9CEF7C32B0B5A4197B20C7155F00B4179E926D1310B5A413F7958A872F00B413D0AD70B350B5A417BA52C438BF00B41C1CAA1FD380B5A41577DAE7697F00B4148E17A643C0B5A412675029A9DF00B419318042E3F0B5A415EFE43BA97F00B41F853E39D410B5A41FB7E6A7C88F00B416666668E440B5A4159CA32C473F00B4179E92619470B5A414803788B57F00B410681950B4A0B5A4169B3EAF342F00B412DB29DA74C0B5A41A64E40933AF00B41B07268F94E0B5A41E7AE256432F00B415C8FC21D520B5A41351136FC32F00B41B6F3FD3C550B5A419E11A53D3AF00B418B6CE7B3570B5A418E976ED241F00B413D0AD753590B5A412CF697DD50F00B4179E926415B0B5A41913177ED57F00B41508D97EE5B0B5A41AFD85FB683F60B4152B81E3D460B5A41B7F3FD9489F60B41F6285C07460B5A41D52B659928F30B41F2D24DDA110B5A4153B81E8541F20B4177BE9FD2030B5A410F2DB25D8CEF0B414C3789810C0B5A4196D409A8EFEF0B41B6F3FD34140B5A410E4FAFD4B5EE0B4185EB51A0180B5A41647FD9FD37EF0B41C520B0721E0B5A41
238981	03	886	609010557	8860026779	335	755.75	0106000020FB0B00000100000001030000000100000018000000016F81041F920B418FC2F5F037035A41653BDF0F8E940B415EBA491443035A41B5C8767E9C940B416891ED6443035A41AEFA5C2D49960B416F1283404B035A4186EB51785A970B4177BE9F4A50035A4180FB3AB075970B41F0A7C6734E035A411E5A647BC9970B411F85EB8940035A418D4AEAC415980B41BA490CC237035A41744694764E980B417D3F35AE33035A412ACB1087C1980B41B29DEF6F2C035A41D52B65D9FF980B417593188428035A41E6D0221B06990B4104560E2528035A4168D5E72A69990B4104560E3523035A41F6B9DA8A77990B417D3F357E22035A41C1EC9EBC83960B4177BE9FE214035A41F185C914AA940B41000000580D035A41B172685165940B4121B072C812035A414A2EFF613A940B414A0C023316035A41E4361AC0F6930B41E3A59BD41A035A410C2428BE24930B41B6F3FD4C29035A4162325570BA920B4196438BBC2F035A4141A4DFBE37920B418195437B36035A41D066D5E71C920B41A8C64BE737035A41016F81041F920B418FC2F5F037035A41
238982	03	886	609010557	8860026880	124	455.350000000000023	0106000020FB0B0000010000000103000000010000000D000000BF9F1A2F93A50B41F4FDD46871035A4137CD3B4ECCA60B417F6ABC7C78035A4108F016484EA70B4179E926617B035A4149E17A548DA80B414260E56871035A4115AE47A1FBA90B417D3F350666035A410BD7A3B08FAA0B41E7FBA99961035A41AB6054129FA90B41D9CEF78B5C035A41E12D906036A80B41F6285CDF54035A41A11A2F9D20A70B4177BE9F1A5E035A41365EBA893AA60B419318041666035A412B3A928BB0A50B41068195E36A035A4140355EFA69A50B416F1283586D035A41BF9F1A2F93A50B41F4FDD46871035A41
238983	03	886	609010557	8860026981	321	731.019999999999982	0106000020FB0B00000100000001030000000100000014000000F6B9DA8A739E0B41A69BC408C7035A4193CB7FC87F9E0B41000000F8C7035A4104098A9F299F0B414E621038CC035A41016F8104A5A00B410C022B4FD5035A41555227A058A10B41B4C876AED9035A417AE9263170A10B417F6ABCC4D9035A416EC5FE32C7A30B41CDCCCC7CC4035A41AEFA5CEDD9A50B41E5D022FBB1035A41C7DCB58407A40B41DD2406C9A7035A41F56C567DEBA20B41A8C64BAFA1035A4125B9FC07EBA10B41D122DBF99B035A4111E9B7AF13A10B41BC74933097035A41790B2468F4A00B41D7A3708599035A41EA2631C885A00B418195431BA1035A41ED2FBBA746A00B41986E12D3A5035A4163A1D674679F0B417F6ABC2CB6035A41E19C1125079F0B41D9CEF71BBD035A4136EF3845A29E0B418195430BC4035A41E4361AC0729E0B416891EDC4C6035A41F6B9DA8A739E0B41A69BC408C7035A41
238984	03	886	609010557	8860027385	304	790.950000000000045	0106000020FB0B000001000000010300000001000000180000001C2FDDE4A8AB0B4154E3A51BC5025A419B999999A8AC0B412B8716C1CB025A417F8CB9EBC5AB0B415C8FC265D6025A41085F980CE1AE0B41736891F5E8025A4192ED7C3F26AF0B410E2DB2B5E7025A414A2EFF6151B10B41621058F9CE025A414A9D806680B00B4160E5D0AACA025A417DF2B0D087B10B41448B6C57C1025A41CA54C16888B10B41E7FBA951C1025A41CFAACF5537AC0B4173689145A2025A418F7571DB05AC0B413BDF4FE5A6025A4104780BA4E1AB0B41F853E345AA025A412D651962CDAB0B41713D0A77AC025A41365EBA89C5AB0B414260E5E8AD025A4114F2418FC9AB0B4121B07240AF025A41B47BF270DBAB0B419CC42068B0025A41F263CC9D0FAC0B415C8FC23DB3025A410712143F3FAC0B41022B8716B7025A416EE7FBE94BAC0B41BE9F1AE7B8025A412085EB514BAC0B4133333353BA025A41E8FBA93125AC0B41A69BC4D8BD025A4175B5153BC8AB0B414260E560C3025A4198FF90BE9DAB0B41FED47891C4025A411C2FDDE4A8AB0B4154E3A51BC5025A41
238985	03	886	609010557	8860027587	172	547.940000000000055	0106000020FB0B0000010000000103000000010000000D0000001AE258D7C38C0B41D578E9A649035A41A401BC050B900B41C74B37015B035A413CDF4FCD45900B41F6285C3F4D035A4171CE881274900B41FCA9F1BA49035A4195F6061FD3900B41E17A148E43035A4140575B314B910B413333331B3D035A4187C95481C4910B412506814D37035A41F875E01CB6910B411904567636035A41FFD47829308F0B4160E5D0B22A035A416B4DF38E928E0B4179E9263128035A41DF938785008D0B41000000D843035A41F797DD53B78C0B4139B4C8B648035A411AE258D7C38C0B41D578E9A649035A41
238986	03	886	609010557	8860038402	212	845.940000000000055	0106000020FB0B00000100000001030000000100000012000000C1EC9EBCFACA0B41C74B3759AC095A4123FDF6B557D30B41355EBA29BF095A4173F90F2992D30B419EEFA7BEB5095A41F953E3E592D10B412FDD241EB2095A41458B6CA790D10B4191ED7C5FB1095A4188A757CABCD10B416DE7FBA9AB095A41DD4603B894D00B418FC2F540AD095A41782D211F88D00B41560E2D0AA7095A41BA8D06F07ACE0B4191ED7C4FA2095A413C014DC4F6CE0B414A0C028B94095A4177711B8D5FCD0B41F0A7C67391095A419C559FAB2FCD0B4148E17A0C98095A41DB1B7CA103CD0B412FDD245E99095A41C07D1DB8F7CB0B415C8FC2A597095A41CD5D4BC8C6CB0B41D122DB7197095A419ABB961067CB0B413333337396095A415D204131E3CA0B413D0AD74395095A41C1EC9EBCFACA0B41C74B3759AC095A41
238987	03	886	609010557	8860044361	70	357.04000000000002	0106000020FB0B0000010000000103000000010000001800000055742497CECE0B412506816DE1035A418F75719BD6CE0B4104560EDDE1035A414CC8077D0AD00B4139B4C81EE9035A41577DAE767ED00B4175931854EB035A41DF02094A9AD00B411F85EBB1EB035A4140575B31F9D00B41BC749328EE035A419B779C6226D10B41FCA9F13AEF035A415B643B9F4FD10B41A01A2FDDEF035A418148BF3D72D10B41B29DEFFFEF035A416029CB10B0D10B413F355E32F0035A413C70CE88FBD10B41986E1203F0035A418204C58F35D20B41666666C6EF035A41A4923A8168D20B4173689155EF035A41DEB5847C9CD20B414E6210E8EE035A41B0946508E2D20B41C74B3731EE035A4198900FFA22D30B41105839D4EC035A413208AC5C4DD30B41666666E6EB035A41B615FB0B74D30B416F128380EA035A41927EFB7A80D20B41B4C876CEE4035A412675025ABED00B4100000048DA035A4105E78C2883D00B41508D975ED9035A415040132147D00B41D9CEF793D9035A41472575C2C5CE0B41295C8FF2E0035A4155742497CECE0B412506816DE1035A41
238988	03	886	609010557	8860044462	6	122.290000000000006	0106000020FB0B00000100000001030000000100000008000000E5839E8D81D10B4179E926A9F1035A41F3D24DA237D20B4114AE4719F6035A418295438B40D20B411B2FDD44F6035A411F166A8D15D30B41A69BC418EF035A411C2FDDE48BD20B413F355E2AF0035A41842F4C662DD20B41355EBA11F1035A41B8627FD97CD10B41F0A7C68BF1035A41E5839E8D81D10B4179E926A9F1035A41
238989	03	886	609010557	8860082959	3158	3187.5300000000002	0106000020FB0B000001000000010300000001000000600000000BD7A3B08B070C411D5A6463790B5A41806ABCB4C4080C41E92631C09A0B5A41E9D9ACBACE080C41D122DBD19B0B5A415405A3D2590A0C41E3A59BE4970B5A41F1A7C60BAD0C0C4123DBF95ECE0B5A412C1895D4CD0C0C410681951BD20B5A41D2915CFEDF0C0C411F85EB99D20B5A410D022BC7B00D0C41BE9F1A47E50B5A41994C154C2A0E0C4139B4C87EEE0B5A41AFB6627F880F0C413BDF4FD5060C5A415BF5B91ABE110C41E17A14162F0C5A413A454772F6110C4148E17A5C2E0C5A415405A3D2E6120C4154E3A55B2A0C5A413CDF4F8D35130C4162105891280C5A4106A392BA43130C413BDF4FA5270C5A41B1E1E9553D130C41CBA1456E260C5A4192ED7C3FE1120C41448B6CE7220C5A41F341CF26E8120C41FA7E6A54210C5A418204C58F01130C41F853E3BD1F0C5A419418049636130C413BDF4FBD1D0C5A41F953E3E553120C41508D9766130C5A412675025A6E130C41AAF1D2050E0C5A41845149DD8D130C41931804460E0C5A41016F8104F9130C4191ED7C47130C5A41B094650832140C411B2FDD7C110C5A4172AC8B9BDF130C41A01A2F3D0E0C5A418A41602507140C41FCA9F13A0D0C5A41AD8BDB287C140C416F1283D00B0C5A4156302AE9CA140C4146B6F34D0B0C5A41DDD781330D150C415EBA493C0B0C5A41E27A146E44150C41000000C0080C5A415E6DC57E99150C41DF4F8D6F050C5A41CE3B4ED1FB150C41355EBA91010C5A41D36F5F872D160C41FCA9F1FAFE0B5A417BC729FA3C160C41B6F3FD3CFD0B5A415A86385675160C41508D977EFC0B5A41109C33A28D160C412506816DFE0B5A41744694767E170C4154E3A5B3FC0B5A41C620B0B229180C41F0A7C663FB0B5A41D8A3703D64180C4191ED7CE7F80B5A41AEFA5CEDBF180C4146B6F38DF40B5A41501E166A19190C4117D9CE4FF30B5A410AF9A0E75D190C4106819523F30B5A41D834EF7895190C4152B81E4DF30B5A4125287E0CCD190C4121B072E0F20B5A4180FB3A70391A0C41068195A3F00B5A41FAA067F33B1A0C4175931834EF0B5A41365EBA89121A0C41CDCCCC94ED0B5A41A11A2FDDED190C4196438B4CEB0B5A41F56C563DE9190C41EE7C3FADEA0B5A414FD1915CEA190C41F853E31DEA0B5A41E3581777F3190C41E9263188E90B5A41AB825149041A0C41AC1C5AC4E80B5A4189855AD3651A0C41A245B6ABE50B5A41E4A59B047D1A0C41F2D24D9AE40B5A4153B81EC5811A0C412B871679E30B5A41C84B37897E1A0C41D578E95EE20B5A41ED2FBBA7EE180C416DE7FBF9CD0B5A41085F98CC8F170C4106819513BC0B5A413E9B559F81170C419CC420F8BA0B5A410022FD367C170C41EC51B8FEB90B5A41504013E17C170C410C022B47B90B5A4152DA1BBC8A170C41A69BC470B80B5A41221FF42C99170C4121B072D8B70B5A4134C4B12E621A0C41FED478B1A90B5A41EC73B595C71A0C4193180486A70B5A41BA8D06F0C2190C41FA7E6AAC9A0B5A411AE25897FE170C41273108FC840B5A41F64A59C6D6160C410681956B770B5A41C76D34002D160C41BA490C2A6F0B5A414EF38E9364150C41CDCCCC6C650B5A4163A1D67447140C4177BE9FD2570B5A41F41FD2EFE7130C41C520B042530B5A4117D9CEB76D130C414A0C02BB4C0B5A41FB7E6A7CDD120C41AAF1D2E5460B5A41A03C2C549A120C410C022B27450B5A41BBDA8AFD1C120C41F4FDD4C0430B5A41A62C431CE5110C418D976EEA430B5A4150401321B8110C415EBA49EC440B5A41221FF46CB3110C41DF4F8D87460B5A4197218EF57B110C418941609D470B5A413B92CB3FAA0E0C416891ED34500B5A411C2FDD6455120C411D5A64337E0B5A41504013E13E120C416ABC74AB7E0B5A417B36AB7E910E0C4185EB5180500B5A41C1CAA105E20C0C41819543A3550B5A41A423B9BCEB0C0C41560E2D1A5B0B5A41AF47E13AE10C0C41AE47E1BA5E0B5A4125B9FC07D30C0C418FC2F5585F0B5A41C8293A12980C0C41FA7E6A4C600B5A4187C95481870C0C412B8716815F0B5A4153B81E456F0C0C41C1CAA13D5D0B5A41D144D8300C0C0C4121B07230580B5A4127530523470B0C41C3F528AC5B0B5A410A1B9E1EEC060C41DF4F8D7F690B5A410BD7A3B08B070C411D5A6463790B5A41
238990	03	886	609010557	8860091043	1183	1453.83999999999992	0106000020FB0B000001000000010300000002000000450000003AB4C8B668D50B416DE7FB110D0B5A418DB96B897FD50B41E3A59B74120B5A41B09465C899D50B410C022BD7170B5A41D6E76AEBABD50B41CDCCCC4C1C0B5A41B5C8767EECD50B41D9CEF7B32A0B5A41E20B93E935D60B41DBF97E8A3A0B5A4198FF907E61D60B41BE9F1A07430B5A41774F1E5668D60B416F128300470B5A414FD1915CC1D60B419EEFA74E470B5A4125B9FC0788D80B41D122DB61480B5A4111E9B7EF0ADA0B416891ED14490B5A41DBACFADCB5DA0B41FCA9F1E2480B5A415496214E4FDC0B4183C0CA89460B5A4171CE881248DD0B4191ED7C0F450B5A41EFEBC0390BDE0B41A4703DE2430B5A419CE61D279CDE0B4121B07290420B5A41ABF1D20DDCDE0B411F85EB99410B5A41C84B37C93ADF0B41A245B6E33F0B5A41EB0434D183DF0B416DE7FB513E0B5A410C462575B9DF0B41E92631003D0B5A41C6FEB23BF0DF0B41A69BC4803B0B5A4198FF907E0CE00B4108AC1C8A3A0B5A41F64A59C662E10B41BA490C12290B5A4161E5D06285E20B4191ED7C5F190B5A415327A0498AE20B4196438B1C180B5A41BC270FCB57E20B41022B87C6160B5A4191A0F8B1EAE10B416F128328140B5A41ADADD85F41E10B4104560E1D100B5A41F3B0502B81E00B41A8C64B370B0B5A41351136FC36E00B41DD240681090B5A410C2428FED4DF0B4162105859080B5A41032B875695DF0B41643BDFCF070B5A41C07D1DF855DF0B41022B8786070B5A4197218EF5F3DE0B4154E3A54B070B5A4133772D21A8DE0B4121B072F0060B5A4155E3A5DB6FDE0B417B14AE67060B5A41DCF97EAA3EDE0B416891EDAC050B5A411C2FDD6415DE0B41250681A5040B5A41ECE2361AC9DD0B41A01A2F4D020B5A415BD3BCE36ADD0B417F6ABCDCFE0A5A4115AE4721B5DC0B4152B81EA5F70A5A41579FABAD78DC0B41A69BC4E8F40A5A414B0C026B54DC0B41FED478A9F20A5A41464772F935DC0B41D7A370A5EF0A5A416BDE71CA13DC0B41C3F5280CEB0A5A41E5141DC9FADB0B4154E3A5FBE60A5A418E976E129FDB0B41D122DB39D90A5A412753052374DB0B41FA7E6A24D30A5A41943A01CD57DB0B41E3A59B64D30A5A418E976E1226DB0B4146B6F39DD40A5A412085EB11C7D80B41CBA14556E10A5A416BDE710ABAD70B4148E17AF4E60A5A41EC73B555ACD70B4191ED7C3FE70A5A4189F4DB978CD70B413BDF4FEDE70A5A41BD96904F89D70B41A01A2FBDE80A5A41B91E852B89D70B4185EB5160EA0A5A41E04F8D57B1D70B4191ED7C4FEE0A5A41FF43FAEDC5D70B4177BE9F1AF10A5A412ACB1087D5D70B41A245B6DBF20A5A4162545227D5D70B41986E1283F80A5A41C8BAB84DD1D70B41643BDF0FFA0A5A412DD49A66C2D70B41273108CCFA0A5A4145FAEDEB9CD70B41B29DEF4FFC0A5A414E158C0AF0D60B4154E3A513FF0A5A41927EFB7A69D60B415A643B1F020B5A413E0AD763DCD50B41AC1C5A4C050B5A415939B4C882D50B417D3F359E070B5A410E4FAF1459D50B4137894108090B5A413AB4C8B668D50B416DE7FB110D0B5A4109000000FDA9F19252E10B41D122DBA1130B5A41F7285C8F5AE10B41A69BC478130B5A41F341CF6665E10B413BDF4F7D130B5A41FA0FE9F76DE10B415839B4B8130B5A41FA0FE9F76DE10B41448B6CEF130B5A41996E124364E10B41F6285C2F140B5A410FBE30D958E10B41F6285C2F140B5A417012830052E10B41273108F4130B5A41FDA9F19252E10B41D122DBA1130B5A41
238991	03	886	609010557	8860091346	746	1096.55999999999995	0106000020FB0B00000100000001030000000100000005000000032B87D6AFCD0B410E2DB2E5850B5A412A5C8F0286D60B41AE47E1FA890B5A418B8EE47245D60B41E5D022E3480B5A41DE2406C150CD0B41FED47899430B5A41032B87D6AFCD0B410E2DB2E5850B5A41
238992	03	886	609010557	8860091447	638	1025.59999999999991	0106000020FB0B00000100000001030000000100000030000000D2915CFE79D60B41B29DEF474D0B5A4103BC05127CD60B418716D9AE520B5A4146D8F03487D60B413333339B5F0B5A41E04F8DD78DD60B41FA7E6A9C680B5A41E4A59B0499D60B41355EBA39760B5A412DD49A66A4D60B41CDCCCC347F0B5A41365EBA09ADD60B4183C0CA11870B5A4128A08970ACD60B41F6285C9F890B5A41A11A2FDD3DD70B41AE47E1BA890B5A4114F241CF9AD70B41105839D4890B5A411E5A643BE8D70B414C3789018A0B5A416BBC745330D80B41CBA145FE890B5A414260E510CED80B416ABC74638A0B5A41F875E0DC09DA0B4117D9CEE78A0B5A41F6B9DA8A6FDA0B419CC420E88A0B5A41C9073D9B33DB0B41F853E3358B0B5A415496210E6BDB0B414A0C024B8B0B5A4140355E7A38DC0B413108ACAC8B0B5A4170F08549B6DC0B419A9999F18B0B5A41DD68006F39DD0B414E6210088C0B5A418148BF3DD2DD0B41355EBA718C0B5A413E0AD7A32FDE0B41C520B0828C0B5A41F953E3652FDE0B41B29DEF37860B5A4176029AC829DE0B419A9999C17D0B5A4108CE19912ADE0B41EC51B83E7A0B5A413355306A2CDE0B41A69BC4D0750B5A41F5FDD4B829DE0B410E2DB25D700B5A4198FF90BE25DE0B412B871631630B5A4115AE472120DE0B419EEFA75E550B5A41B2506B5A21DE0B4196438BFC4B0B5A414794F6861FDE0B41986E122B4A0B5A41DDD7813318DE0B417B14AE9F480B5A41D066D5E709DE0B412DB29D3F470B5A41C84B378909DE0B41D9CEF78B460B5A418DDB68C0E8DD0B411F85EBE1450B5A41C8293A52BFDD0B41D578E91E460B5A4196D4096883DD0B41D122DB49460B5A4105560E6D6FDD0B410E2DB2AD460B5A412FFF213D48DC0B41EC51B866480B5A41CD7F483FF7DA0B41C74B37414A0B5A418148BFFD6FDA0B41C1CAA1D54A0B5A413B234A3B3EDA0B41448B6CEF4A0B5A4152DA1BFCF4D90B41000000D84A0B5A4114F241CF10D90B416DE7FB814A0B5A41F1A7C68B54D70B41AC1C5A74490B5A41C84B37C9B8D60B41F0A7C613490B5A4140355EBA73D60B41DBF97EF2480B5A41D2915CFE79D60B41B29DEF474D0B5A41
238993	03	886	609010557	8860091750	548	920.259999999999991	0106000020FB0B0000010000000103000000010000001F000000FFD478A9B6E80B411D5A64B33A0B5A4160984CD512E90B410AD7A3B8410B5A41C4F5285CAEE90B41DD2406594E0B5A41D59AE69D38EA0B418716D9B6590B5A4161E5D0621DEB0B4123DBF9966C0B5A41D834EF786FEB0B419CC420A06F0B5A416991ED3CB4EE0B41AE47E192620B5A412E211FF4B6EF0B41666666A65E0B5A41117A362B9AEF0B41EE7C3F9D5A0B5A411EC9E5FF8CEF0B41621058F1580B5A4159CA32047FEF0B414A0C0223550B5A413889412083EF0B41AC1C5A9C500B5A41935CFE03A8EF0B41736891E54A0B5A4147B6F33DD6EF0B416891ED24460B5A415F4BC847EDEF0B41B29DEFBF420B5A415EDC4683F5EF0B4177BE9FA23E0B5A41A5703D0AF3EF0B41DD2406993A0B5A418DDB6800DCEF0B4108AC1C62370B5A41304CA60AB7EF0B41295C8F4A330B5A41E5839E4D82EF0B41AAF1D2052E0B5A41433EE89936EF0B4185EB5150280B5A410EE02D10EAEE0B41E7FBA9C9230B5A41EA482E3FEEED0B41A4703D52190B5A41A0ABADD895ED0B412B871671160B5A41E96A2BB68BEB0B4191ED7C8F210B5A419B9999D9A6EA0B4108AC1C22260B5A416007CE5957EA0B4179E92631280B5A413F7958A800EA0B41F4FDD4A02A0B5A4101917E3BB9E90B41DD2406492D0B5A4140355E7A8DE80B415A643BB7350B5A41FFD478A9B6E80B411D5A64B33A0B5A41
238994	03	886	609010557	8860091851	1046	1372.15000000000009	0106000020FB0B000001000000010300000002000000200000007BC729BA68EB0B41C1CAA1D5730B5A41CAE53FA49DEB0B41713D0ABF870B5A41244A7B4312EC0B41713D0AAFAF0B5A41708104C580EC0B415A643B77D70B5A4181B7400221ED0B41C3F5283CD70B5A41DA3D791890ED0B41E17A140ED70B5A41943A018DDCEE0B419EEFA776D60B5A411EC9E53FCCEF0B413108AC34D60B5A4155E3A55BAAF10B41BC749368D50B5A412FFF217D05F50B41F4FDD418D40B5A414803784B14F50B419CC420A0D00B5A413C70CE8816F50B41D7A37045C90B5A41B025E4030BF50B41FED47891C50B5A410F2DB25D01F50B4166666696C30B5A41B103E7CCECF40B41D34D6280C00B5A41063411F65BF40B41FA7E6AECB20B5A41C6FEB2FBF8F30B411D5A643BAA0B5A41EC51B81E92F30B41D122DB41A10B5A41F341CF2611F30B41E9263160950B5A41388941A0CCF20B41931804DE8D0B5A41E3C798FB65F20B41AC1C5AFC810B5A4165CC5D4B4DF20B41AC1C5A847F0B5A4106A392BA38F20B41713D0ADF7D0B5A415FBA49CCD6F10B410681958B770B5A4155E3A55BAAF10B41986E123B750B5A41D59AE69D4FF10B41E7FBA931710B5A418AD2DE20CBF00B4104560E9D6B0B5A410D022B47D9EF0B41736891B5610B5A4168D5E72AA6EF0B4166666626620B5A4194A9829192ED0B416ABC740B6A0B5A41306EA30176EB0B418B6CE7AB720B5A417BC729BA68EB0B41C1CAA1D5730B5A41080000000D022BC705EE0B417B14AE97870B5A41994C154C11EE0B4121B07220860B5A41AFD85FB659EE0B41DF4F8DFF850B5A41C76D34C070EE0B41560E2DD2860B5A41BAFC87746DEE0B41EC51B8E6870B5A41EA26310833EE0B41B4C87636890B5A41797AA52C0DEE0B41B29DEFBF880B5A410D022BC705EE0B417B14AE97870B5A41
238995	03	886	609010557	8860092154	851	1275.1400000000001	0106000020FB0B0000010000000103000000010000001A000000BF9F1AEFE0CE0B41E92631F8DF0B5A4125B9FC4753CF0B41295C8FAADF0B5A4161E5D0227FCF0B4137894138DD0B5A4197B20C71A2CF0B414A0C02ABDC0B5A41C2A8A40EE9CF0B41AAF1D21DDC0B5A41010000C090D00B41DBF97EFADB0B5A4150AF94A5AFD00B4179E92641DC0B5A41FA31E62EE0D00B4137894138DD0B5A418E28ED4D41D10B41DBF97E02DF0B5A41D2915CFE22D20B4179E92631DF0B5A41DACEF71389D20B4117D9CEFFDE0B5A41920F7A7653D60B41F2D24DBADD0B5A41723D0A9778D60B41713D0A9FDD0B5A410FBE309987D60B4127310894DD0B5A41C2A8A4CE68D60B410C022B27CF0B5A419131772D0CD50B416DE7FB99CE0B5A41433EE85903D50B4100000030CE0B5A41F953E3E502D50B41378941D0C70B5A41D44D621025D50B41BE9F1A4FC70B5A4177711B8DC0D50B413108ACACC60B5A417AE92631A2D60B411D5A64A3BD0B5A4147B6F33DA8D60B41333333EBBC0B5A418E976E5269D60B4179E926018D0B5A416A006F416DD60B412FDD24AE8C0B5A41115839B4D9CD0B41DBF97ED2880B5A41BF9F1AEFE0CE0B41E92631F8DF0B5A41
238996	03	886	609010557	8860130449	346	973.600000000000023	0106000020FB0B0000010000000103000000010000000F00000067666626B06F0B4152B81EF5C1045A41FFD478E9B36F0B419EEFA70EC4045A41304CA6CAB86F0B41B29DEFDFC4045A41A857CA32D16F0B4146B6F35DC5045A4104780BA448700B41819543F3C4045A41C386A757DF720B41819543A3C2045A41E20B93E91B780B4114AE4789BD045A41C1CAA185197C0B41F0A7C6E3B9045A41943A01CD7A7B0B41A01A2F7599045A4127530523C9790B41B29DEF879E045A41E5141DC9A8760B417B14AE17A7045A41C8293A521C720B4137894138B3045A413433337334710B4117D9CEEFB5045A417AE926F1B06F0B41713D0ADFB9045A4167666626B06F0B4152B81EF5C1045A41
238997	03	886	609010557	8860130550	106	418.100000000000023	0106000020FB0B0000010000000103000000010000000A0000004872F94FBA8E0B41D578E90627035A41DC8AFDA50F920B417B14AE4736035A41746891ED39920B41D578E9F633035A417F8CB9EBA2920B410E2DB28D2E035A41C1EC9E7CD4920B4123DBF91E2B035A419FCDAA8F46930B41F6285C7F23035A41D7C56D7443930B41C74B377123035A41A857CA32D48F0B41295C8FFA13035A41C4F5281C758F0B41B07268711A035A414872F94FBA8E0B41D578E90627035A41
238998	03	886	609010557	8860163690	737	2244.98999999999978	0106000020FB0B0000010000000103000000010000009D0000008DB96B89D9A10B4185EB51E0CF025A41CC10C77AD3A30B41643BDF6FEF025A41845149DDEBA30B418D976ED2EE025A41653BDF4F3AA40B41E3A59B7CEC025A417BA52C8390A40B41643BDF0FEA025A41AD1C5A2406A50B411B2FDD54E6025A415EFE43FA4CA50B41DF4F8D1FE4025A415EDC4643CEA50B4179E92621E0025A410CB5A6391DA60B41FCA9F19ADD025A41DC8AFDA5A4A60B41AC1C5A9CD9025A41C00E9CF300A70B413F355E5AD7025A4174D712F23CA70B41000000E0D5025A4151FC1873A9A70B41AAF1D27DD3025A411461C3930CA80B41295C8FAAD1025A4196438B6C72A80B41F6285C1FD0025A419CE61DE7B1A80B41F0A7C6ABCF025A414B0C026BF0A80B41B81E8563CE025A41D9F0F4CA1FA90B419CC42020CD025A411C9E5E2949A90B41E17A142ECC025A4196438B2C7DA90B419EEFA716CB025A41024D844D5BA90B4108AC1CC2C9025A4172AC8B9B0DA90B41EE7C3F85C5025A414C598678E5A80B416ABC74D3C4025A410C462575B9A80B41295C8FAAC4025A414794F686DAA70B4189416055C7025A41701283808CA70B412B871611C3025A4146D8F03459A70B415EBA49BCC0025A41BF9F1A2FE8A60B41355EBA91BA025A412ACB10C794A60B41BA490C62B6025A41A2D6342F05A70B41819543DBB4025A412BA91310FDA70B41986E128BB1025A41FAA067333AA80B415EBA49ACB6025A410CB5A63943A80B410AD7A380B7025A4135A2B4376EA80B41A69BC430B8025A41A4923A81FCA80B41A8C64B97B6025A4123FDF6F5D2A90B414260E528B4025A41BD7493D865AA0B415839B480BF025A4147B6F33D5EA90B413F355E82C3025A41BCB88DC647A90B41EC51B886C4025A41039A08DB3FA90B41508D974EC5025A41D6E76AEB48A90B41508D970EC6025A419B779C2254A90B412DB29D9FC7025A41ECE2361A7EA90B41B81E8583C9025A410CB5A639A3A90B410C022BA7CA025A419C559FEBC4A90B4181954333CA025A412DD49A269CAA0B41C520B09AC6025A41161DC9A5E2AA0B417F6ABC4CC5025A413F7958A823AB0B4146B6F3B5C4025A41BBDA8A3D7EAB0B41B6F3FD1CC3025A41335530AAACAB0B411F85EB79C1025A41DCF97E6ACFAB0B418FC2F598BE025A412085EB1101AC0B416666662EBB025A4161764F1E0CAC0B416DE7FB81B9025A41CF19515AF5AB0B41E5D02273B7025A41AD1C5A24D0AB0B41819543FBB4025A4140355EFA83AB0B41A245B66BB0025A4187C9544179AB0B41C74B3721AF025A41085F98CC7EAB0B41E17A1496AD025A41752497FF93AB0B41C74B37F1AA025A4139F8C264BDAB0B41D122DB11A7025A419E11A53DDDAB0B416F1283D0A4025A41653BDF0FEEAB0B416DE7FBC9A2025A415F4BC847D2AB0B41AAF1D20DA2025A41C00E9CF357AB0B417F6ABC2C9F025A41AE69DEF1D5AA0B41022B87169C025A41A79BC4E0E8AA0B413F355E629B025A41A6BDC157D5AB0B41508D97D6A0025A41A423B9BCFBAB0B415EBA49CCA1025A41BFC117A60AAC0B414E6210D0A0025A413FE8D96C5AAC0B41022B87BE9A025A41D1B359357CAC0B4183C0CAE195025A413E9B559F8EAC0B41BC7493F892025A41A2D634EF92AC0B41C3F5280C91025A4183734614A2AC0B41DF4F8DAF8A025A41994C154CABAC0B41B6F3FD8C87025A41F03845879EAC0B418FC2F5A085025A41935CFE031CAC0B4123DBF93E84025A415BF5B9DA00AC0B41C976BE4783025A415EDC4643BCAB0B41EC51B86E82025A41117A36EB81AB0B411283C0C281025A411B51DA9B5EAB0B41448B6CDF81025A417E3F351E34AB0B41AAF1D24D84025A4135A2B437F7AA0B4123DBF98E88025A41F8E461A1B2AA0B4148E17A8C8D025A41F8065F1897AA0B417B14AEA78E025A41FCCBEE096EAA0B4139B4C8EE8D025A41DE2406C14BAA0B4160E5D0428D025A41994C154C22AA0B411283C0928C025A417BC729FABDA90B417F6ABC348A025A410CB5A639E8A80B41F853E36585025A413D4ED151BEA80B415EBA490485025A41C386A7977CA80B41BC7493E085025A41275305E35FA80B417B14AEF785025A41312AA9933CA80B41F0A7C66B84025A4183E2C7D80BA80B412506815D84025A41845149DDC9A70B416891ED6484025A4122B072A88EA70B419CC420B885025A414113614382A70B41C74B37E986025A415A17B751DBA70B41E7FBA90189025A4104098ADFCEA90B41022B87A694025A41799CA2A3C8AA0B41190456B69A025A41A8E848AEB4AA0B41AE47E17A9B025A41A62C431C94A80B41022B87AE8E025A4184C0CAE1EAA70B4196438BB48A025A416D09F920A5A70B416ABC740389025A41032B87D671A70B41DF4F8D1F88025A41C6FEB2FB66A70B419318044E88025A411A73D71239A70B41FA7E6ACC88025A41CF19515AEEA60B41333333F389025A417BC729BA92A60B41DBF97EDA8B025A4169226CB847A60B41A8C64BBF8D025A419A2A1895FFA50B41295C8F1A8C025A41E6D0229BADA50B41273108748C025A4160984C957EA50B41FA7E6A8C8C025A417012830035A50B41F6285C978C025A41351136FCDFA40B41DBF97E6A8D025A4153B81E4589A40B41A245B6F38D025A411383C00A3FA40B418941600D8F025A41A9C64BF713A40B41378941888F025A411D7C6172FDA30B41F2D24DE28F025A41EF7C3F35DDA30B4114AE476190025A41A79BC420C4A30B41643BDF8793025A415327A089AAA30B4152B81EC595025A41D0F753231BA40B415C8FC29598025A4104780BA446A40B41DF4F8D8F99025A41FA0FE9F77FA40B41F0A7C6F39A025A4139F8C264FEA40B41B6F3FDDC9D025A4141A4DF7E59A50B41D122DB09A0025A415EFE43FA83A50B41F6285CF7A0025A4131BB278F87A50B410E2DB25DA1025A4153B81E457BA50B4196438B84A2025A418204C54F67A50B418716D996A3025A410CB5A6F917A50B4100000028A7025A41B637F8C2F8A40B410E2DB2CDA8025A41D2915CFE80A40B41E3A59BB4AE025A417AE926F13DA40B41643BDF47B1025A416029CB10D7A30B4160E5D082B6025A41D44D62504AA30B41FA7E6A8CBD025A4111E9B7AF5AA30B4117D9CE77BF025A4170128300A9A30B411F85EBE1C3025A415BD3BCA305A40B41FCA9F1CAC8025A41CC10C7BA75A40B415EBA491CCF025A41996E12837EA40B412DB29DCFCF025A413A4547B261A40B4117D9CE87D0025A41C239234AFCA30B41F2D24D6AD2025A4102DE02C99EA30B41295C8F3AD3025A41799CA2E382A30B411F85EB49D3025A418B1F63AE2BA30B41295C8FEACE025A41994C150CEDA20B41F853E3FDCA025A41CDEEC903B0A20B4185EB5190C7025A413AB4C876A1A20B4104560E05C7025A414FD1911C8FA20B4166666696C7025A41A2F8316648A20B41E3A59B2CCA025A41645DDCC624A20B418B6CE7D3CB025A41F1164850F9A10B41B0726849CE025A41D222DBF9E1A10B413D0AD7A3CF025A418DB96B89D9A10B4185EB51E0CF025A41
238999	03	886	609010557	8860164603	16	177.849999999999994	0106000020FB0B0000010000000103000000010000000E0000001D7C61B270B00B4177BE9F12C6045A41A4923A41C6B00B412FDD24CEC7045A4105560E2D72B10B41CDCCCCC4CA045A4133E6AEE510B20B41AAF1D265CD045A41B559F5B93FB20B419EEFA776CC045A411EC9E5FF7DB20B410AD7A3A0C9045A41C4D32BE571B20B4123DBF956C9045A4152499D801AB20B4114AE47F1C6045A4128A08970D9B10B414E621090C5045A41DA3D791852B10B413BDF4FD5C2045A413208ACDC13B10B414A0C0293C1045A41085F98CCA6B00B4148E17ABCBF045A412E431C2B8AB00B412B871699C2045A411D7C61B270B00B4177BE9F12C6045A41
239000	03	886	609010557	8860164704	208	585.970000000000027	0106000020FB0B000001000000010300000001000000170000005496214E0AB10B411283C082B2045A41DD68006FFFB10B41DD2406C1B9045A4156302A2983B20B41508D97AEBB045A411C9E5EE9F6B20B41C1CAA1E5BD045A413D4ED15187B30B41EC51B8D6C2045A410C2428FEC9B30B41A8C64B4FC1045A414260E51095B40B41FED478D1B7045A4195F6061FBFB40B41F853E3BDB5045A4159A835CDCFB40B414A0C0213B4045A41BAFC8774B8B40B411283C032B2045A413C70CE885DB50B4183C0CAB1AA045A41D44D625027B60B41B81E8523A3045A4194180416FBB60B41B6F3FD2C9A045A4146477239F0B30B41C74B37F191045A41BD96904F87B30B41B4C8765E91045A410FBE309935B30B412B87162991045A415A17B791F9B20B41022B872692045A417E3F351E1FB20B4152B81EE59B045A41653BDF0FBBB10B410E2DB245A0045A41A79BC4608CB10B416ABC74FBA2045A4128A089B05DB10B413D0AD793A6045A41D8A370FD3BB10B416891ED5CAA045A415496214E0AB10B411283C082B2045A41
239001	03	886	609010557	8860169653	253	655.720000000000027	0106000020FB0B0000010000000103000000010000000D000000A857CA32D48F0B41295C8FFA13035A41D7C56D7443930B41C74B377123035A419FCDAA8F46930B41F6285C7F23035A416B4DF38E33940B417368914513035A412675029A30940B41643BDF4F12035A4145696F702C940B411904560612035A41312AA99375950B41448B6CD7FB025A4149BF7DDDCB950B41508D97A6F5025A41D300DE826F920B41E9263158E6025A415D2041F159920B4146B6F3F5E5025A41B2BFEC1EC1900B41355EBAA902035A416A6FF00571900B41A01A2F1509035A41A857CA32D48F0B41295C8FFA13035A41
239002	03	886	609010557	8860180565	411	898.120000000000005	0106000020FB0B000001000000010300000001000000120000009F5E298B15C80B416F128370CE025A413355306A4CCC0B41759318CCE9025A4156302A2942CF0B4148E17AC4FC025A414803788BA4CF0B419EEFA7CEF8025A4173F90FE9EDCF0B417D3F3536F5025A4172AC8B9B1CD00B41931804CEF3025A41CCA145F651D00B415839B4E8F3025A414794F6C67AD10B41E7FBA969FB025A410B68226C54D10B41D34D6240F1025A41D300DEC23ED10B41CDCCCC6CEA025A410E4FAF142ED10B417D3F355EE0025A417C832F4C38D10B419A9999C1DB025A41CECCCC0CA3CB0B41B81E85D3B7025A41721B0DA0FFCA0B413D0AD7DBB9025A4194A9829182CA0B41759318E4BB025A41EB0434D1DBC90B416DE7FB79C0025A419587855AE3C80B41378941E0C7025A419F5E298B15C80B416F128370CE025A41
239003	03	886	609010557	8860197945	283	854.049999999999955	0106000020FB0B00000100000001030000000100000029000000625452E71DBD0B41BE9F1ACF88035A41996E128330BE0B41E3A59BFC8E035A4101917E3BF5BF0B418941608D99035A41ED2FBBA7FCBF0B41EC51B84699035A41723D0A97C5C00B419CC420189E035A4115D044186CC10B410E2DB2E5A1035A41845149DD65C10B41CDCCCC2CA2035A414260E5D0B2C10B417D3F35FEA3035A416A006F41E3C10B4133333323A5035A418A416025EAC10B4185EB51E0A4035A41388941606DC20B416891EDECA7035A41C1EC9EBCF1C20B412DB29D1FAB035A41F1A7C64B67C30B410AD7A378AA035A411EC9E5FFB8C30B417593189CA9035A419FCDAA4F3AC40B41B29DEF7FA7035A41F2F44AD9A0C40B4121B07200A6035A41B8627F9967C30B41BC7493589F035A41752497FF61C30B41EE7C3FAD9E035A41F3B050EBCFC30B41AAF1D28D9C035A41ABF1D24D40C40B411D5A64D397035A41D066D5A7DEC40B416F1283789B035A419C559FEB4FC50B4152B81E9D9D035A41D4BCE3D46AC60B41C3F5288C90035A419B999959A2C50B414E6210388C035A413208AC1C2DC50B413D0AD7BB89035A41ACCFD5D6F5C40B416891EDBC88035A413C70CEC8C7C40B41BE9F1A3F88035A41570E2D7297C40B41F6285CE787035A41ADADD89F61C30B411F85EB4187035A41AF47E1BADDC20B41713D0ACF86035A418816D90EB3C10B41AE47E1CA85035A41A5703D4A88C10B41EE7C3F4585035A411A73D7D266C10B41FED4788984035A412FFF217DAFC00B4139B4C84E80035A41D3DEE00B0CC00B411904569E7C035A417BA52C4337BF0B4185EB51D877035A416C9A771CD9BE0B41713D0AFF75035A4130DD244697BE0B418941602D78035A415A17B75107BE0B4177BE9F727E035A41063411B627BD0B41AC1C5ABC87035A41625452E71DBD0B41BE9F1ACF88035A41
239004	03	886	609010557	8860202793	740	1412.27999999999997	0106000020FB0B0000010000000103000000010000002C000000388941A0139D0B41C1CAA1C590035A41EC51B85E739D0B4148E17ACC90035A418F06F0D6FD9F0B4196438B6490035A41E661A1D609A00B41CBA1457E90035A418E976E5219A00B418FC2F5D890035A4195F6061F56A00B41CDCCCCD490035A412CF697DD1FA10B41A01A2F4D90035A41797AA52CE6A20B419A9999B180035A416C9A771C2AA40B41A4703DA275035A414B7B832F34A20B41CBA145866B035A416A006F41B1A20B41B29DEF4764035A41D36F5F47F2A20B41E3A59B6463035A41FC5C6D8546A30B415A643BAF62035A41996E128397A30B41643BDFDF61035A413580B7C0E5A30B41A4703D4261035A416B4DF3CE58A40B41A01A2F3D63035A4114F241CF62A40B419CC4203865035A41039A08DBBCA40B4185EB51F066035A414B7B832FA7A40B418195433B69035A416C2BF65745A50B41333333FB6B035A41B5C8767E7FA50B41CBA145F669035A4156302A29E1A50B41EC51B8A666035A418E28ED0D57A60B41A69BC48862035A41B6A6798700A70B417F6ABCA45C035A412B3A924B97A70B414260E5A057035A41016F8104D2A70B41713D0A8F55035A417BA52C43E2A70B412DB29DEF54035A4101000040F0A70B414E62105054035A4183734614FEA70B418D976EB253035A412E431CEB20A80B419EEFA7A651035A41A935CD3B68A80B41B6F3FD1C4D035A41100BB526EBA80B413F355EC243035A411E3867C4F6A80B410C022BA742035A418A4160A5D7A80B41D34D626041035A41526B9AF7FBA70B41759318543C035A4109AC1C5ABFA60B41DD24062935035A41C31726D34BA50B417F6ABC7C2D035A4124DBF9BE08A40B418941608526035A41EAB7AF0343A30B41BE9F1A4F22035A41ABF1D28DD89F0B41AC1C5A9C5D035A411BC05B60939E0B415A643B2F73035A41221FF42C5A9D0B415A643BD78A035A41B40C71ACEF9C0B41C1CAA1AD90035A41388941A0139D0B41C1CAA1C590035A41
239005	03	886	609010557	8860204211	451	917.990000000000009	0106000020FB0B000001000000010300000001000000130000001EC9E53F09CD0B41E7FBA901D7035A41FFD478E97DCE0B418D976EEADF035A414F621018C6CE0B41819543B3DF035A419DC420706BD00B41EE7C3F0DD7035A41DDD781F396D20B41F853E3E5E3035A41B9AF0327B6D30B41BA490C8AEA035A412F90A07810D40B417593188CE7035A41BF9F1AEF86D60B417D3F354ED1035A41E661A11673D40B41A4703DFAC6035A41AB6054521BD40B410681951BC3035A4156C1A864A1D30B41A69BC438BB035A41093D9B5538D30B4121B07240B8035A41E04F8D9791D20B418941607DB5035A41D300DE82F1D10B4152B81E25B3035A418A41602536D10B41FED478C9B0035A413CDF4F4D5CD00B4123DBF976AD035A4150AF94E583CF0B414C378951AB035A41A64E4093B4CB0B415A643B77CF035A411EC9E53F09CD0B41E7FBA901D7035A41
239006	03	886	609010557	8860204312	825	1392.19000000000005	0106000020FB0B0000010000000103000000010000003C00000052499D805CDE0B4139B4C8D64B0B5A41EFEBC0F95ADE0B419EEFA7EE4C0B5A417BA52C8366DE0B41EE7C3FDD660B5A41F5DBD70169DE0B41295C8F42710B5A4106A3927A6CDE0B411B2FDDEC7C0B5A414F62101875DE0B4117D9CEBF880B5A418295434B77DE0B413BDF4FFD8B0B5A418A4160A5CFDE0B41355EBAB98B0B5A4105560EEDE9E20B41D578E99E860B5A4109AC1C9A73E30B414A0C02F3850B5A41B1E1E955BFE40B41D578E9A6840B5A41D0F753E3DBE50B4127310844830B5A41EE0DBE306CE60B41E17A1466820B5A4113143FC6BAE60B4146B6F385810B5A4180FB3AB056E70B413108AC247F0B5A41829543CB69E80B4154E3A5B37A0B5A41A5DFBE0ECEE90B415839B460750B5A41EB0434D112EB0B418941603D700B5A41DE2406C1EBEA0B412FDD24F66C0B5A41DC8AFDA50FEA0B4117D9CE0F5B0B5A416A6FF0052CE90B41AC1C5A0C480B5A416BBC74938CE80B41E7FBA9A93A0B5A41C620B03288E80B418D976E32390B5A415F4BC84731E80B41DBF97EAA390B5A416C9A775CDFE70B4148E17A6C410B5A41297E8C39BEE70B41CDCCCCEC430B5A4114F2418F8EE70B415A643BD7450B5A4101917E3BCDE60B4121B072504C0B5A418E28ED0D7EE60B41A4703DCA4E0B5A415939B40842E60B418D976E32500B5A41A11A2F9DF2E50B4106819563510B5A418B1F636E94E40B41F2D24D22570B5A41C542ADE95FE40B4121B07258580B5A41B09465C861E40B41BC749348590B5A41AD8BDB285CE40B41B29DEF275C0B5A41AB82518956E40B41E17A145E5D0B5A41769318C443E40B417D3F35FE5D0B5A41994C150C27E40B41E5D022635E0B5A41684469EF95E30B41B4C8762E600B5A41B39DEFE759E30B41736891DD600B5A412ACB10C756E30B41D9CEF73B5C0B5A418738D60558E30B41250681AD580B5A41F64A59C651E30B41666666D6570B5A415EDC464340E30B411058398C560B5A41799CA2E33BE30B41A01A2F2D560B5A4141A4DFBE15E30B416891ED74540B5A41D52B6599EAE20B415839B480520B5A417DF2B0D0AFE20B41FA7E6A3C500B5A41F038454762E20B410C022B2F4E0B5A410C2428BE1EE20B41D578E9764C0B5A4135A2B437C0E10B41643BDFAF4A0B5A41797AA52C97E10B4191ED7C5F4A0B5A41FD18739773E10B41B81E852B4A0B5A41464772F93CE10B419A9999094A0B5A41B2BFEC1EFAE00B41273108CC490B5A41FE87F49B55E00B41CBA1457E490B5A415405A312D8DF0B41B0726829490B5A413D4ED151E2DE0B4166666606490B5A41C239230A59DE0B41AAF1D20D490B5A4152499D805CDE0B4139B4C8D64B0B5A41
239007	03	886	609010557	8860204413	565	1091.95000000000005	0106000020FB0B000001000000010300000001000000280000006619E2D853CD0B414C3789E9410B5A41D834EF38DFCE0B41273108CC420B5A4131BB27CFD4D00B414E6210E0430B5A41F1A7C64B50D20B413F355EB2440B5A41DC8AFDA51AD40B418D976EAA450B5A41EB95B20CECD50B411D5A648B460B5A41E3C798FB2AD60B415C8FC2CD460B5A41966588A332D60B41CBA145BE440B5A41168C4A6A2AD60B411D5A6443410B5A41F3D24DA2DED50B416891EDEC300B5A4151FC183385D50B418716D94E1C0B5A415D8FC2353ED50B412DB29D6F0B0B5A41C00E9CF331D50B41F6285C9F080B5A418738D64532D50B4148E17AF4060B5A418738D60535D50B412B871679050B5A41C31726D355D50B415A643B87030B5A41E3C7983B57D50B418716D986000B5A4188A757CA58D50B4123DBF9EEFE0A5A41DF02094A49D50B410AD7A3A0FD0A5A41C4D32BE56ED40B411D5A64E3F80A5A41557424977DD30B4196438B94F30A5A412831086C31D30B41C976BEDFF10A5A41A423B9BCD3D20B41C520B002F10A5A41FF43FAADABD20B4191ED7C17F10A5A41797AA52C82D20B416666664EF30A5A41C07D1DF850D20B41448B6C87F60A5A417BC729BA3AD20B417F6ABC6CF80A5A419ABB96D06DD10B41C74B3771050B5A419FEFA7C694D00B41448B6C17130B5A415327A08923D00B41F4FDD4401A0B5A41DCF97EAA10D00B41C74B37591B0B5A41E0E00B9308D00B41022B87461C0B5A41A2D6342F03D00B4185EB51E01D0B5A411DEBE236C9CF0B4160E5D0022E0B5A41A423B9BCBBCF0B41D7A370CD310B5A41D1D5566C9FCF0B4175931844320B5A4126068155A3CE0B41000000A8330B5A41E1BE0E5C56CD0B41643BDF87350B5A41AF47E13A1DCD0B413F355EF2350B5A416619E2D853CD0B414C3789E9410B5A41
239008	03	886	609010557	8860221789	7	315.490000000000009	0106000020FB0B0000010000000103000000010000000A000000472575C2C5CE0B41295C8FF2E0035A415040132147D00B41D9CEF793D9035A4105E78C2883D00B41508D975ED9035A412675025ABED00B4100000048DA035A41927EFB7A80D20B41B4C876CEE4035A41DDD781F396D20B41F853E3E5E3035A419DC420706BD00B41EE7C3F0DD7035A414F621018C6CE0B41819543B3DF035A41FFD478E97DCE0B418D976EEADF035A41472575C2C5CE0B41295C8FF2E0035A41
239009	03	886	609010557	8860223813	543	1276.43000000000006	0106000020FB0B0000010000000103000000010000001D000000F263CC5D28CD0B413F355EAA32045A41E6D0229B2ACD0B419A99996933045A41D834EF3827CD0B410681959333045A410E71AC8B02CF0B41EE7C3FA541045A41797AA52C12CF0B418941609542045A417C832F0C0ACF0B412DB29D2F44045A41E20B93A900CF0B411283C04245045A41D8A3703D8FCF0B415A643B3F49045A418C6CE73BF7CF0B41AE47E1D24B045A4161764F5E42D00B41B4C8767E49045A4183E2C71807D10B41BE9F1A9742045A416007CE99BAD30B4146B6F31524045A41F263CC5DADD40B418195438319045A41B5C8767E9CD60B418D976EFA01045A41F3B0502BE1D60B418716D9BEFE035A4166AA60143ED70B417D3F35FEFA035A411E3867C47CD80B4162105871ED035A4183734654F7D80B4160E5D01AE9035A411C0DE0EDADD90B4108AC1C7AE3035A41994C150C92DA0B41FCA9F172DE035A413E0AD763BAD90B416DE7FBA9DB035A41BF9F1A6F7BD80B41CDCCCC64DC035A4105E78C28A5D70B4137894110DD035A41312AA953FFD60B41BC7493C8DF035A418226C2C6A5D40B41B29DEFEFF1035A41117A36EBEBD10B418B6CE7DB08045A41A5DFBECEB8D00B41F6285CD712045A41260681D51CCD0B41448B6C2F32045A41F263CC5D28CD0B413F355EAA32045A41
\.


--
-- Data for Name: portti_bundle; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.portti_bundle (id, name, config, state, startup) FROM stdin;
61	language-selector	{}	{}	\N
62	peltodata	\N	\N	\N
54	maprotator	\N	\N	\N
3	divmanazer	{}	{}	\N
5	statehandler	{}	{}	\N
7	search	{}	{}	\N
8	layerselector2	{}	{}	\N
9	layerselection2	{}	{}	\N
10	personaldata	{}	{}	\N
12	coordinatedisplay	{}	{}	\N
13	maplegend	{}	{}	\N
14	userguide	{\n    "tags" : "userguide",\n    "flyoutClazz" : "Oskari.mapframework.bundle.userguide.SimpleFlyout"\n}	{}	\N
4	toolbar	{}	{}	\N
53	system-message	\N	\N	\N
6	infobox	{\n  "adaptable": true\n}	{}	\N
63	camera-controls-3d	\N	\N	\N
65	dimension-change	{}	\N	\N
64	time-control-3d	\N	\N	\N
66	layerlist	\N	\N	\N
67	admin-layereditor	\N	\N	\N
16	guidedtour	{}	{}	\N
17	backendstatus	{}	{}	\N
18	printout	{}	{}	\N
19	postprocessor	{}	{}	\N
28	publishedstatehandler	{}	{}	\N
26	featuredata2	{}	{}	\N
24	promote	{}	{}	\N
27	admin-layerrights	{}	{}	\N
29	publishedmyplaces2	{}	{}	\N
30	myplacesimport	{\n    "name": "MyPlacesImport",\n    "sandbox": "sandbox",\n    "flyoutClazz": "Oskari.mapframework.bundle.myplacesimport.Flyout"\n}	{}	\N
31	admin-users	{\n    "restUrl": "action_route=Users"\n}	{}	\N
44	metrics	\N	\N	\N
32	routesearch	{\n    "flyoutClazz": "Oskari.mapframework.bundle.routesearch.Flyout"\n}	{}	\N
33	rpc	{}	{}	\N
34	findbycoordinates	{}	{}	\N
36	admin-layerselector	{}	{}	\N
37	admin	{}	{}	\N
39	metadataflyout	{}	{}	\N
41	coordinatetool	{}	{}	\N
42	routingUI	{}	{}	\N
43	routingService	{}	{}	\N
45	admin-wfs-search-channel	\N	\N	\N
46	search-from-channels	\N	\N	\N
47	publisher2	\N	\N	\N
49	timeseries	\N	\N	\N
50	selected-featuredata	{}	{}	\N
51	content-editor	\N	\N	\N
52	feedbackService	\N	\N	\N
48	drawtools	\N	\N	\N
20	statsgrid	{}	{}	\N
38	analyse	{}	{}	\N
55	appsetup	\N	\N	\N
56	admin-publish-transfer	\N	\N	\N
57	download-basket	{}	{}	\N
58	hierarchical-layerlist	\N	\N	\N
59	admin-hierarchical-layerlist	\N	\N	\N
60	myplaces3	\N	\N	\N
40	metadatacatalogue	{}	{}	\N
35	heatmap	{}	{}	\N
2	mapfull	{}	{}	\N
\.


--
-- Data for Name: portti_keyword_association; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.portti_keyword_association (keyid1, keyid2, type) FROM stdin;
\.


--
-- Data for Name: portti_keywords; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.portti_keywords (id, keyword, uri, lang, editable) FROM stdin;
\.


--
-- Data for Name: portti_layer_keywords; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.portti_layer_keywords (keyid, layerid) FROM stdin;
\.


--
-- Data for Name: portti_view; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.portti_view (uuid, id, name, is_default, type, description, page, application, application_dev_prefix, only_uuid, creator, domain, lang, is_public, metadata, old_id, created, used, usagecount) FROM stdin;
3cd8e907-864d-45a4-a0a0-34c4c7fd2f1f	2	Publisher template	f	PUBLISH		published	embedded	/applications	t	-1		fi	f	{}	-1	2019-02-19 12:19:25.847561	2019-02-19 12:19:25.847561	0
461192eb-40d0-499b-96d3-40bc25f7220a	1	Default view	t	DEFAULT		index	peltodata	/applications	t	-1		fi	t	{}	-1	2019-02-19 12:19:25.822387	2021-05-18 10:02:35.74283	3175
\.


--
-- Data for Name: portti_view_bundle_seq; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.portti_view_bundle_seq (view_id, bundle_id, seqno, config, state, startup, bundleinstance) FROM stdin;
1	3	2	{}	{}	\N	divmanazer
1	48	3	\N	\N	\N	drawtools
1	4	4	{}	{}	\N	toolbar
1	6	5	{\n  "adaptable": true\n}	{}	\N	infobox
1	5	6	{}	{}	\N	statehandler
1	7	7	{}	{}	\N	search
1	40	8	{}	{}	\N	metadatacatalogue
1	41	11	{}	{}	\N	coordinatetool
1	39	12	{}	{}	\N	metadataflyout
1	10	13	{}	{}	\N	personaldata
1	47	14	\N	\N	\N	publisher2
1	13	15	{}	{}	\N	maplegend
1	14	16	{\n    "tags" : "userguide",\n    "flyoutClazz" : "Oskari.mapframework.bundle.userguide.SimpleFlyout"\n}	{}	\N	userguide
1	60	17	\N	\N	\N	myplaces3
1	18	18	{}	{}	\N	printout
1	38	19	{}	{}	\N	analyse
1	30	20	{\n    "name": "MyPlacesImport",\n    "sandbox": "sandbox",\n    "flyoutClazz": "Oskari.mapframework.bundle.myplacesimport.Flyout"\n}	{}	\N	myplacesimport
1	26	21	{"selectionTools":true}	{}	\N	featuredata2
1	35	22	{}	{}	\N	heatmap
2	6	2	{\n  "adaptable": true\n}	{}	\N	infobox
2	4	3	{"toolbarId":"PublisherToolbar","viewtools":{"link":false},"basictools":{"measureline":false,"select":false,"measurearea":false,"zoombox":false},"history":{"history_forward":false,"history_back":false,"reset":false},"hasContentContainer":true,"defaultToolbarContainer":".publishedToolbarContent"}	{}	\N	toolbar
2	28	4	{}	{}	\N	publishedstatehandler
2	48	5	\N	\N	\N	drawtools
2	43	6	{}	{}	\N	routingService
2	33	7	{}	{}	\N	rpc
1	49	23	{}	{}	\N	timeseries
1	49	55	{}	{}	\N	timeseries
2	2	1	{"plugins":[{"id":"Oskari.mapframework.bundle.mapmodule.plugin.LayersPlugin"},{"id":"Oskari.mapframework.mapmodule.WmsLayerPlugin"},{"id":"Oskari.wfsvector.WfsVectorLayerPlugin"},{"id":"Oskari.mapframework.wmts.mapmodule.plugin.WmtsLayerPlugin"},{"id":"Oskari.mapframework.bundle.mapmodule.plugin.RealtimePlugin"},{"id":"Oskari.mapframework.bundle.mapmodule.plugin.LogoPlugin"},{"id":"Oskari.mapframework.bundle.myplacesimport.plugin.UserLayersLayerPlugin"},{"id":"Oskari.mapframework.bundle.mapanalysis.plugin.AnalysisLayerPlugin"},{"id":"Oskari.arcgis.bundle.maparcgis.plugin.ArcGisLayerPlugin"},{"id":"Oskari.mapframework.mapmodule.MarkersPlugin","config":{"markerButton":false}},{"id":"Oskari.mapframework.mapmodule.VectorLayerPlugin"}],"mapOptions":{"srsName":"EPSG:3067","resolutions":[2048,1024,512,256,128,64,32,16,8,4,2,1,0.5,0.25],"maxExtent":{"top":8388608,"left":-548576,"bottom":6291456,"right":1548576}},"layers":[]}	{"east":395640.5,"selectedLayers":[],"srs":"EPSG:3067","north":6715992.5,"zoom":3}	\N	mapfull
1	66	9	{}	{}	\N	layerlist
1	2	1	{"plugins":[{"id":"Oskari.mapframework.bundle.mapmodule.plugin.LayersPlugin"},{"id":"Oskari.mapframework.mapmodule.WmsLayerPlugin"},{"id":"Oskari.mapframework.mapmodule.MarkersPlugin"},{"id":"Oskari.mapframework.mapmodule.ControlsPlugin"},{"id":"Oskari.mapframework.mapmodule.GetInfoPlugin","config":{"ignoredLayerTypes":["WFS","MYPLACES"]}},{"id":"Oskari.mapframework.wmts.mapmodule.plugin.WmtsLayerPlugin"},{"id":"Oskari.wfsvector.WfsVectorLayerPlugin"},{"id":"Oskari.mapframework.bundle.mapmodule.plugin.ScaleBarPlugin"},{"id":"Oskari.mapframework.bundle.mapmodule.plugin.Portti2Zoombar"},{"id":"Oskari.mapframework.bundle.mapmodule.plugin.PanButtons"},{"id":"Oskari.mapframework.bundle.mapmodule.plugin.FullScreenPlugin"},{"id":"Oskari.mapframework.bundle.myplacesimport.plugin.UserLayersLayerPlugin"},{"id":"Oskari.mapframework.bundle.mapmyplaces.plugin.MyPlacesLayerPlugin"},{"id":"Oskari.mapframework.bundle.mapanalysis.plugin.AnalysisLayerPlugin"},{"id":"Oskari.mapframework.mapmodule.VectorLayerPlugin"}],"mapOptions":{"srsName":"EPSG:3067","resolutions":[2048,1024,512,256,128,64,32,16,8,4,2,1,0.5,0.25],"maxExtent":{"top":8388608,"left":-548576,"bottom":6291456,"right":1548576}},"layers":[]}	{"east":228603.63452149878,"selectedLayers":[{"id":"155"}],"north":6829300.268010451,"zoom":7}	\N	mapfull
\.


--
-- Data for Name: ratings; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.ratings (id, userid, rating, category, categoryitem, comment, userrole, created) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: user_layer; Type: TABLE DATA; Schema: public; Owner: oskari
--

COPY public.user_layer (id, uuid, layer_name, layer_desc, layer_source, publisher_name, created, updated, fields, wkt, options) FROM stdin;
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.categories_id_seq', 2, true);


--
-- Name: my_places_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.my_places_id_seq', 1, true);


--
-- Name: oskari_capabilities_cache_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_capabilities_cache_id_seq', 28, true);


--
-- Name: oskari_jaas_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_jaas_users_id_seq', 20, true);


--
-- Name: oskari_layergroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_layergroup_id_seq', 19, true);


--
-- Name: oskari_maplayer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_maplayer_id_seq', 158, true);


--
-- Name: oskari_maplayer_metadata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_maplayer_metadata_id_seq', 1, false);


--
-- Name: oskari_maplayer_projections_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_maplayer_projections_id_seq', 1, false);


--
-- Name: oskari_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_permission_id_seq', 3049, true);


--
-- Name: oskari_resource_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_resource_id_seq', 238, true);


--
-- Name: oskari_role_oskari_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_role_oskari_user_id_seq', 136, true);


--
-- Name: oskari_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_roles_id_seq', 14, true);


--
-- Name: oskari_statistical_datasource_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_statistical_datasource_id_seq', 1, false);


--
-- Name: oskari_user_indicator_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_user_indicator_data_id_seq', 1, false);


--
-- Name: oskari_user_indicator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_user_indicator_id_seq', 1, false);


--
-- Name: oskari_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_users_id_seq', 20, true);


--
-- Name: oskari_users_pending_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_users_pending_id_seq', 2, true);


--
-- Name: oskari_wfs_search_channels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.oskari_wfs_search_channels_id_seq', 1, false);


--
-- Name: peltodata_field_execution_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.peltodata_field_execution_id_seq', 8, true);


--
-- Name: peltodata_field_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.peltodata_field_file_id_seq', 4, true);


--
-- Name: peltodata_field_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.peltodata_field_id_seq', 4, true);


--
-- Name: peltodata_peltolohkot_gid_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.peltodata_peltolohkot_gid_seq', 3, false);


--
-- Name: portti_bundle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.portti_bundle_id_seq', 67, true);


--
-- Name: portti_inspiretheme_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.portti_inspiretheme_id_seq', 49, true);


--
-- Name: portti_keywords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.portti_keywords_id_seq', 1, false);


--
-- Name: portti_view_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.portti_view_id_seq', 2, true);


--
-- Name: ratings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.ratings_id_seq', 1, false);


--
-- Name: ratings_userid_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.ratings_userid_seq', 1, false);


--
-- Name: user_layer_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.user_layer_data_id_seq', 1, false);


--
-- Name: user_layer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: oskari
--

SELECT pg_catalog.setval('public.user_layer_id_seq', 1, false);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: gt_pk_metadata_table gt_pk_metadata_table_table_schema_table_name_pk_column_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.gt_pk_metadata_table
    ADD CONSTRAINT gt_pk_metadata_table_table_schema_table_name_pk_column_key UNIQUE (table_schema, table_name, pk_column);


--
-- Name: oskari_capabilities_cache oskari_capabilities_cache__unique_service; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_capabilities_cache
    ADD CONSTRAINT oskari_capabilities_cache__unique_service UNIQUE (layertype, version, url);


--
-- Name: oskari_capabilities_cache oskari_capabilities_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_capabilities_cache
    ADD CONSTRAINT oskari_capabilities_cache_pkey PRIMARY KEY (id);


--
-- Name: oskari_jaas_users oskari_jaas_users_login_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_jaas_users
    ADD CONSTRAINT oskari_jaas_users_login_key UNIQUE (login);


--
-- Name: oskari_jaas_users oskari_jaas_users_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_jaas_users
    ADD CONSTRAINT oskari_jaas_users_pkey PRIMARY KEY (id);


--
-- Name: oskari_dataprovider oskari_layergroup_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_dataprovider
    ADD CONSTRAINT oskari_layergroup_pkey PRIMARY KEY (id);


--
-- Name: oskari_maplayer_externalid oskari_maplayer_externalid_externalid_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_externalid
    ADD CONSTRAINT oskari_maplayer_externalid_externalid_key UNIQUE (externalid);


--
-- Name: oskari_maplayer_externalid oskari_maplayer_externalid_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_externalid
    ADD CONSTRAINT oskari_maplayer_externalid_pkey PRIMARY KEY (maplayerid);


--
-- Name: oskari_maplayer_metadata oskari_maplayer_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_metadata
    ADD CONSTRAINT oskari_maplayer_metadata_pkey PRIMARY KEY (id);


--
-- Name: oskari_maplayer oskari_maplayer_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer
    ADD CONSTRAINT oskari_maplayer_pkey PRIMARY KEY (id);


--
-- Name: oskari_maplayer_projections oskari_maplayer_projections_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_projections
    ADD CONSTRAINT oskari_maplayer_projections_pkey PRIMARY KEY (id);


--
-- Name: oskari_permission oskari_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_permission
    ADD CONSTRAINT oskari_permission_pkey PRIMARY KEY (id);


--
-- Name: oskari_resource oskari_resource_pk; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_resource
    ADD CONSTRAINT oskari_resource_pk PRIMARY KEY (id);


--
-- Name: oskari_role_oskari_user oskari_role_oskari_user_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_role_oskari_user
    ADD CONSTRAINT oskari_role_oskari_user_pkey PRIMARY KEY (id);


--
-- Name: oskari_roles oskari_roles_name_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_roles
    ADD CONSTRAINT oskari_roles_name_key UNIQUE (name);


--
-- Name: oskari_roles oskari_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_roles
    ADD CONSTRAINT oskari_roles_pkey PRIMARY KEY (id);


--
-- Name: oskari_statistical_datasource oskari_statistical_datasource_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_statistical_datasource
    ADD CONSTRAINT oskari_statistical_datasource_pkey PRIMARY KEY (id);


--
-- Name: oskari_statistical_layer oskari_statistical_layer_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_statistical_layer
    ADD CONSTRAINT oskari_statistical_layer_pkey PRIMARY KEY (datasource_id, layer_id);


--
-- Name: oskari_status_myapp oskari_status_myapp_pk; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_status_myapp
    ADD CONSTRAINT oskari_status_myapp_pk PRIMARY KEY (version);


--
-- Name: oskari_status_myplaces oskari_status_myplaces_pk; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_status_myplaces
    ADD CONSTRAINT oskari_status_myplaces_pk PRIMARY KEY (installed_rank);


--
-- Name: oskari_status_peltodata oskari_status_peltodata_pk; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_status_peltodata
    ADD CONSTRAINT oskari_status_peltodata_pk PRIMARY KEY (installed_rank);


--
-- Name: oskari_status oskari_status_pk; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_status
    ADD CONSTRAINT oskari_status_pk PRIMARY KEY (installed_rank);


--
-- Name: oskari_status_userlayer oskari_status_userlayer_pk; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_status_userlayer
    ADD CONSTRAINT oskari_status_userlayer_pk PRIMARY KEY (installed_rank);


--
-- Name: oskari_user_indicator_data oskari_user_indicator_data_indicator_year; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator_data
    ADD CONSTRAINT oskari_user_indicator_data_indicator_year UNIQUE (indicator_id, regionset_id, year);


--
-- Name: oskari_user_indicator_data oskari_user_indicator_data_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator_data
    ADD CONSTRAINT oskari_user_indicator_data_pkey PRIMARY KEY (id);


--
-- Name: oskari_user_indicator oskari_user_indicator_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator
    ADD CONSTRAINT oskari_user_indicator_pkey PRIMARY KEY (id);


--
-- Name: oskari_users_pending oskari_users_pending_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_users_pending
    ADD CONSTRAINT oskari_users_pending_pkey PRIMARY KEY (id);


--
-- Name: oskari_users oskari_users_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_users
    ADD CONSTRAINT oskari_users_pkey PRIMARY KEY (id);


--
-- Name: oskari_users oskari_users_user_name_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_users
    ADD CONSTRAINT oskari_users_user_name_key UNIQUE (user_name);


--
-- Name: oskari_users oskari_users_uuid_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_users
    ADD CONSTRAINT oskari_users_uuid_key UNIQUE (uuid);


--
-- Name: peltodata_field_file peltodata_field_file_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_file
    ADD CONSTRAINT peltodata_field_file_pkey PRIMARY KEY (id);


--
-- Name: peltodata_field_layer peltodata_field_layer_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_layer
    ADD CONSTRAINT peltodata_field_layer_pkey PRIMARY KEY (field_id, layer_id);


--
-- Name: peltodata_field peltodata_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field
    ADD CONSTRAINT peltodata_fields_pkey PRIMARY KEY (id);


--
-- Name: peltodata_peltolohkot peltodata_peltolohkot_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_peltolohkot
    ADD CONSTRAINT peltodata_peltolohkot_pkey PRIMARY KEY (gid);


--
-- Name: portti_bundle portti_bundle_name_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_bundle
    ADD CONSTRAINT portti_bundle_name_key UNIQUE (name);


--
-- Name: portti_bundle portti_bundle_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_bundle
    ADD CONSTRAINT portti_bundle_pkey PRIMARY KEY (id);


--
-- Name: oskari_maplayer_group portti_inspiretheme_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_group
    ADD CONSTRAINT portti_inspiretheme_pkey PRIMARY KEY (id);


--
-- Name: portti_keywords portti_keywords_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_keywords
    ADD CONSTRAINT portti_keywords_pkey PRIMARY KEY (id);


--
-- Name: portti_view portti_view_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_view
    ADD CONSTRAINT portti_view_pkey PRIMARY KEY (id);


--
-- Name: portti_view portti_view_uuid_key; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_view
    ADD CONSTRAINT portti_view_uuid_key UNIQUE (uuid);


--
-- Name: oskari_wfs_search_channels portti_wfs_search_channels_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_wfs_search_channels
    ADD CONSTRAINT portti_wfs_search_channels_pkey PRIMARY KEY (id);


--
-- Name: ratings ratings_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_pkey PRIMARY KEY (id);


--
-- Name: peltodata_field_execution table_name_pk; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_execution
    ADD CONSTRAINT table_name_pk PRIMARY KEY (id);


--
-- Name: oskari_resource type_mapping; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_resource
    ADD CONSTRAINT type_mapping UNIQUE (resource_type, resource_mapping);


--
-- Name: portti_keyword_association unique_all_columns; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_keyword_association
    ADD CONSTRAINT unique_all_columns UNIQUE (keyid1, keyid2, type);


--
-- Name: user_layer user_layer_pkey; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.user_layer
    ADD CONSTRAINT user_layer_pkey PRIMARY KEY (id);


--
-- Name: portti_view_bundle_seq view_seq; Type: CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_view_bundle_seq
    ADD CONSTRAINT view_seq UNIQUE (view_id, seqno);


--
-- Name: oskari_maplayer_projections_maplayerid_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_maplayer_projections_maplayerid_idx ON public.oskari_maplayer_projections USING btree (maplayerid);


--
-- Name: oskari_maplayer_projections_name_index; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_maplayer_projections_name_index ON public.oskari_maplayer_projections USING btree (name);


--
-- Name: oskari_maplayer_q1; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_maplayer_q1 ON public.oskari_maplayer USING btree (parentid);


--
-- Name: oskari_maplayer_q2; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_maplayer_q2 ON public.oskari_maplayer USING btree (dataprovider_id);


--
-- Name: oskari_permission_resid_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_permission_resid_idx ON public.oskari_permission USING btree (oskari_resource_id);


--
-- Name: oskari_resource_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_resource_idx ON public.oskari_resource USING btree (resource_type, resource_mapping);


--
-- Name: oskari_status_myapp_ir_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_status_myapp_ir_idx ON public.oskari_status_myapp USING btree (installed_rank);


--
-- Name: oskari_status_myapp_s_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_status_myapp_s_idx ON public.oskari_status_myapp USING btree (success);


--
-- Name: oskari_status_myapp_vr_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_status_myapp_vr_idx ON public.oskari_status_myapp USING btree (version_rank);


--
-- Name: oskari_status_myplaces_s_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_status_myplaces_s_idx ON public.oskari_status_myplaces USING btree (success);


--
-- Name: oskari_status_peltodata_s_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_status_peltodata_s_idx ON public.oskari_status_peltodata USING btree (success);


--
-- Name: oskari_status_s_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_status_s_idx ON public.oskari_status USING btree (success);


--
-- Name: oskari_status_userlayer_s_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX oskari_status_userlayer_s_idx ON public.oskari_status_userlayer USING btree (success);


--
-- Name: peltodata_peltolohkot_ely_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX peltodata_peltolohkot_ely_idx ON public.peltodata_peltolohkot USING btree (ely);


--
-- Name: peltodata_peltolohkot_geom_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX peltodata_peltolohkot_geom_idx ON public.peltodata_peltolohkot USING gist (geom);


--
-- Name: peltodata_peltolohkot_lohko_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX peltodata_peltolohkot_lohko_idx ON public.peltodata_peltolohkot USING btree (lohko);


--
-- Name: peltodata_peltolohkot_tiltu_idx; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX peltodata_peltolohkot_tiltu_idx ON public.peltodata_peltolohkot USING btree (tiltu);


--
-- Name: user_layer_uuid_index; Type: INDEX; Schema: public; Owner: oskari
--

CREATE INDEX user_layer_uuid_index ON public.user_layer USING btree (uuid);


--
-- Name: oskari_backendstatus oskari_backendstatus_maplayer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_backendstatus
    ADD CONSTRAINT oskari_backendstatus_maplayer_id_fkey FOREIGN KEY (maplayer_id) REFERENCES public.oskari_maplayer(id) ON DELETE CASCADE;


--
-- Name: portti_layer_keywords oskari_layer_keywords_layerid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_layer_keywords
    ADD CONSTRAINT oskari_layer_keywords_layerid_fkey FOREIGN KEY (layerid) REFERENCES public.oskari_maplayer(id) ON DELETE CASCADE;


--
-- Name: oskari_maplayer_externalid oskari_maplayer_externalid_maplayerid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_externalid
    ADD CONSTRAINT oskari_maplayer_externalid_maplayerid_fkey FOREIGN KEY (maplayerid) REFERENCES public.oskari_maplayer(id) ON DELETE CASCADE;


--
-- Name: oskari_maplayer oskari_maplayer_groupid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer
    ADD CONSTRAINT oskari_maplayer_groupid_fkey FOREIGN KEY (dataprovider_id) REFERENCES public.oskari_dataprovider(id) ON DELETE CASCADE;


--
-- Name: oskari_maplayer_group_link oskari_maplayer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_group_link
    ADD CONSTRAINT oskari_maplayer_id_fkey FOREIGN KEY (maplayerid) REFERENCES public.oskari_maplayer(id) ON DELETE CASCADE;


--
-- Name: oskari_maplayer_projections oskari_maplayer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_projections
    ADD CONSTRAINT oskari_maplayer_id_fkey FOREIGN KEY (maplayerid) REFERENCES public.oskari_maplayer(id) ON DELETE CASCADE;


--
-- Name: oskari_permission oskari_resource_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_permission
    ADD CONSTRAINT oskari_resource_id_fkey FOREIGN KEY (oskari_resource_id) REFERENCES public.oskari_resource(id) ON DELETE CASCADE;


--
-- Name: oskari_role_external_mapping oskari_role_external_mapping_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_role_external_mapping
    ADD CONSTRAINT oskari_role_external_mapping_fkey FOREIGN KEY (roleid) REFERENCES public.oskari_roles(id) ON DELETE CASCADE;


--
-- Name: oskari_role_oskari_user oskari_role_oskari_user_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_role_oskari_user
    ADD CONSTRAINT oskari_role_oskari_user_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.oskari_roles(id) ON DELETE CASCADE;


--
-- Name: oskari_role_oskari_user oskari_role_oskari_user_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_role_oskari_user
    ADD CONSTRAINT oskari_role_oskari_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.oskari_users(id) ON DELETE CASCADE;


--
-- Name: oskari_statistical_layer oskari_statistical_layer_datasource_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_statistical_layer
    ADD CONSTRAINT oskari_statistical_layer_datasource_id_fkey FOREIGN KEY (datasource_id) REFERENCES public.oskari_statistical_datasource(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oskari_statistical_layer oskari_statistical_layer_layer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_statistical_layer
    ADD CONSTRAINT oskari_statistical_layer_layer_id_fkey FOREIGN KEY (layer_id) REFERENCES public.oskari_maplayer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oskari_user_indicator_data oskari_user_indicator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator_data
    ADD CONSTRAINT oskari_user_indicator_id_fkey FOREIGN KEY (indicator_id) REFERENCES public.oskari_user_indicator(id) ON DELETE CASCADE;


--
-- Name: oskari_user_indicator_data oskari_user_indicator_regionset_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator_data
    ADD CONSTRAINT oskari_user_indicator_regionset_fkey FOREIGN KEY (regionset_id) REFERENCES public.oskari_maplayer(id);


--
-- Name: oskari_user_indicator oskari_user_indicator_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_user_indicator
    ADD CONSTRAINT oskari_user_indicator_user_fk FOREIGN KEY (user_id) REFERENCES public.oskari_users(id) ON DELETE CASCADE;


--
-- Name: peltodata_field_execution peltodata_field_execution_peltodata_field_file_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_execution
    ADD CONSTRAINT peltodata_field_execution_peltodata_field_file_id_fk FOREIGN KEY (field_file_id) REFERENCES public.peltodata_field_file(id);


--
-- Name: peltodata_field_file peltodata_field_file_peltodata_field_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_file
    ADD CONSTRAINT peltodata_field_file_peltodata_field_id_fk FOREIGN KEY (field_id) REFERENCES public.peltodata_field(id);


--
-- Name: peltodata_field_layer peltodata_field_layer_field_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_layer
    ADD CONSTRAINT peltodata_field_layer_field_id_fkey FOREIGN KEY (field_id) REFERENCES public.peltodata_field(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peltodata_field_layer peltodata_field_layer_layer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_layer
    ADD CONSTRAINT peltodata_field_layer_layer_id_fkey FOREIGN KEY (layer_id) REFERENCES public.oskari_maplayer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peltodata_field peltodata_field_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field
    ADD CONSTRAINT peltodata_field_user_fk FOREIGN KEY (user_id) REFERENCES public.oskari_users(id) ON DELETE CASCADE;


--
-- Name: oskari_maplayer_group_link portti_inspiretheme_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.oskari_maplayer_group_link
    ADD CONSTRAINT portti_inspiretheme_id_fkey FOREIGN KEY (groupid) REFERENCES public.oskari_maplayer_group(id) ON DELETE CASCADE;


--
-- Name: portti_keyword_association portti_keyword_association_keyid1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_keyword_association
    ADD CONSTRAINT portti_keyword_association_keyid1_fkey FOREIGN KEY (keyid1) REFERENCES public.portti_keywords(id) ON DELETE CASCADE;


--
-- Name: portti_keyword_association portti_keyword_association_keyid2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_keyword_association
    ADD CONSTRAINT portti_keyword_association_keyid2_fkey FOREIGN KEY (keyid2) REFERENCES public.portti_keywords(id) ON DELETE CASCADE;


--
-- Name: portti_layer_keywords portti_layer_keywords_keyid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_layer_keywords
    ADD CONSTRAINT portti_layer_keywords_keyid_fkey FOREIGN KEY (keyid) REFERENCES public.portti_keywords(id) ON DELETE CASCADE;


--
-- Name: portti_view_bundle_seq portti_view_bundle_seq_bundle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_view_bundle_seq
    ADD CONSTRAINT portti_view_bundle_seq_bundle_id_fkey FOREIGN KEY (bundle_id) REFERENCES public.portti_bundle(id);


--
-- Name: portti_view_bundle_seq portti_view_bundle_seq_view_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.portti_view_bundle_seq
    ADD CONSTRAINT portti_view_bundle_seq_view_id_fkey FOREIGN KEY (view_id) REFERENCES public.portti_view(id) ON DELETE CASCADE;


--
-- Name: peltodata_field_execution table_name_peltodata_field_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: oskari
--

ALTER TABLE ONLY public.peltodata_field_execution
    ADD CONSTRAINT table_name_peltodata_field_id_fk FOREIGN KEY (field_id) REFERENCES public.peltodata_field(id);


--
-- Name: DATABASE oskaridb; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON DATABASE oskaridb TO oskari;


--
-- PostgreSQL database dump complete
--

